// WL_ACT2.C

#include <stdio.h>
#include <math.h>
#include <iostream>
#include <algorithm>
#include <ctime>
#include <set>
#include <fstream>
#include "wl_def.h"
#pragma hdrstop

/*
=============================================================================

                               LOCAL CONSTANTS

=============================================================================
*/

#define PROJECTILESIZE  0xc000l

#define BJRUNSPEED      2048
#define BJJUMPSPEED     680

#define FINDNEWTARGET_TIMEOUT 100


/*
=============================================================================

                              GLOBAL VARIABLES

=============================================================================
*/

boolean TryMove (objtype *ob);

/*
=============================================================================

                              LOCAL VARIABLES

=============================================================================
*/


dirtype dirtable[9] = {northwest,north,northeast,west,nodir,east,
    southwest,south,southeast};

short starthitpoints[4][NUMENEMIES] =
//
// BABY MODE
//
{
    {
        25,   // guards
        50,   // officer
        100,  // SS
        1,    // dogs
        850,  // Hans
        850,  // Schabbs
        200,  // fake hitler
        800,  // mecha hitler
        45,   // mutants
        25,   // ghosts
        25,   // ghosts
        25,   // ghosts
        25,   // ghosts

        850,  // Gretel
        850,  // Gift
        850,  // Fat
        5,    // en_spectre,
        1450, // en_angel,
        850,  // en_trans,
        1050, // en_uber,
        950,  // en_will,
        1250, // en_death
        100,  // en_bjmutant
        25,   // en_bjmutantdog
        20,   // en_wbarrel
        50,   // en_tbarrel
        30,   // en_ebarrel
        5,    // en_vines
        20,   // en_toxbarrel
    },
    //
    // DON'T HURT ME MODE
    //
    {
        25,   // guards
        50,   // officer
        100,  // SS
        1,    // dogs
        950,  // Hans
        950,  // Schabbs
        300,  // fake hitler
        950,  // mecha hitler
        55,   // mutants
        25,   // ghosts
        25,   // ghosts
        25,   // ghosts
        25,   // ghosts

        950,  // Gretel
        950,  // Gift
        950,  // Fat
        10,   // en_spectre,
        1550, // en_angel,
        950,  // en_trans,
        1150, // en_uber,
        1050, // en_will,
        1350, // en_death
        125,  // en_bjmutant
        50,   // en_bjmutantdog
        20,   // en_wbarrel
        50,   // en_tbarrel
        30,   // en_ebarrel
        5,    // en_vines
        20,   // en_toxbarrel
    },
    //
    // BRING 'EM ON MODE
    //
    {
        25,   // guards
        50,   // officer
        100,  // SS
        1,    // dogs

        1050, // Hans
        1550, // Schabbs
        400,  // fake hitler
        1050, // mecha hitler

        55,   // mutants
        25,   // ghosts
        25,   // ghosts
        25,   // ghosts
        25,   // ghosts

        1050, // Gretel
        1050, // Gift
        1050, // Fat
        15,   // en_spectre,
        1650, // en_angel,
        1050, // en_trans,
        1250, // en_uber,
        1150, // en_will,
        1450, // en_death
        150,  // en_bjmutant
        75,   // en_bjmutantdog
        20,   // en_wbarrel
        50,   // en_tbarrel
        30,   // en_ebarrel
        5,    // en_vines
        20,   // en_toxbarrel
    },
    //
    // DEATH INCARNATE MODE
    //
    {
        25,   // guards
        50,   // officer
        100,  // SS
        1,    // dogs

        1200, // Hans
        2400, // Schabbs
        500,  // fake hitler
        1200, // mecha hitler

        65,   // mutants
        25,   // ghosts
        25,   // ghosts
        25,   // ghosts
        25,   // ghosts

        1200, // Gretel
        1200, // Gift
        1200, // Fat
        25,   // en_spectre,
        2000, // en_angel,
        1200, // en_trans,
        1400, // en_uber,
        1300, // en_will,
        1600, // en_death
        200,  // en_bjmutant
        75,   // en_bjmutantdog
        20,   // en_wbarrel
        50,   // en_tbarrel
        30,   // en_ebarrel
        5,    // en_vines
        20,   // en_toxbarrel
    }
};

void    A_StartDeathCam (objtype *ob);


void    T_Path (objtype *ob);
void    T_Shoot (objtype *ob);
void    T_Bite (objtype *ob);
void    T_DogChase (objtype *ob);
void    T_Chase (objtype *ob);
void    T_Projectile (objtype *ob);
void    T_Stand (objtype *ob);

void A_DeathScream (objtype *ob);

extern  statetype s_rocket;
extern  statetype s_smoke1;
extern  statetype s_smoke2;
extern  statetype s_smoke3;
extern  statetype s_smoke4;
extern  statetype s_boom2;
extern  statetype s_boom3;

void A_Smoke (objtype *ob);

statetype s_rocket              = {true,SPR_ROCKET_1,3,(statefunc)T_Projectile,(statefunc)A_Smoke,&s_rocket};
statetype s_smoke1              = {false,SPR_SMOKE_1,3,NULL,NULL,&s_smoke2};
statetype s_smoke2              = {false,SPR_SMOKE_2,3,NULL,NULL,&s_smoke3};
statetype s_smoke3              = {false,SPR_SMOKE_3,3,NULL,NULL,&s_smoke4};
statetype s_smoke4              = {false,SPR_SMOKE_4,3,NULL,NULL,NULL};

statetype s_boom1               = {false,SPR_BOOM_1,6,NULL,NULL,&s_boom2};
statetype s_boom2               = {false,SPR_BOOM_2,6,NULL,NULL,&s_boom3};
statetype s_boom3               = {false,SPR_BOOM_3,6,NULL,NULL,NULL};

#ifdef SPEAR

extern  statetype s_hrocket;
extern  statetype s_hsmoke1;
extern  statetype s_hsmoke2;
extern  statetype s_hsmoke3;
extern  statetype s_hsmoke4;
extern  statetype s_hboom2;
extern  statetype s_hboom3;

void A_Smoke (objtype *ob);

statetype s_hrocket             = {true,SPR_HROCKET_1,3,(statefunc)T_Projectile,(statefunc)A_Smoke,&s_hrocket};
statetype s_hsmoke1             = {false,SPR_HSMOKE_1,3,NULL,NULL,&s_hsmoke2};
statetype s_hsmoke2             = {false,SPR_HSMOKE_2,3,NULL,NULL,&s_hsmoke3};
statetype s_hsmoke3             = {false,SPR_HSMOKE_3,3,NULL,NULL,&s_hsmoke4};
statetype s_hsmoke4             = {false,SPR_HSMOKE_4,3,NULL,NULL,NULL};

statetype s_hboom1              = {false,SPR_HBOOM_1,6,NULL,NULL,&s_hboom2};
statetype s_hboom2              = {false,SPR_HBOOM_2,6,NULL,NULL,&s_hboom3};
statetype s_hboom3              = {false,SPR_HBOOM_3,6,NULL,NULL,NULL};

#endif

extern  statetype s_flame1;
extern  statetype s_flame2;
extern  statetype s_flame3;
extern  statetype s_flame4;

extern  statetype s_flameboom1;
extern  statetype s_flameboom2;
extern  statetype s_flameboom3;

statetype s_flame1              = {false,SPR_FLAME_1,3,(statefunc)T_Projectile,NULL,&s_flame2};
statetype s_flame2              = {false,SPR_FLAME_2,3,(statefunc)T_Projectile,NULL,&s_flame3};
statetype s_flame3              = {false,SPR_FLAME_3,3,(statefunc)T_Projectile,NULL,&s_flame4};
statetype s_flame4              = {false,SPR_FLAME_4,3,(statefunc)T_Projectile,NULL,&s_flame1};

statetype s_flameboom1          = {false,SPR_FLAMEBOOM_1,6,NULL,NULL,&s_flameboom2};
statetype s_flameboom2          = {false,SPR_FLAMEBOOM_2,6,NULL,NULL,&s_flameboom3};
statetype s_flameboom3          = {false,SPR_FLAMEBOOM_3,6,NULL,NULL,NULL};

void    T_Schabb (objtype *ob);
void    T_SchabbThrow (objtype *ob);
void    T_Fake (objtype *ob);
void    T_FakeFire (objtype *ob);
void    T_Ghosts (objtype *ob);

void A_Slurpie (objtype *ob);
void A_HitlerMorph (objtype *ob);
void A_MechaSound (objtype *ob);

/*
=================
=
= A_Smoke
=
=================
*/

void A_Smoke (objtype *ob)
{
    GetNewActor ();
#ifdef SPEAR
    if (ob->obclass == hrocketobj)
        newobj->state = &s_hsmoke1;
    else
#endif
        newobj->state = &s_smoke1;
    newobj->ticcount = 6;

    newobj->tilex = ob->tilex;
    newobj->tiley = ob->tiley;
    newobj->x = ob->x;
    newobj->y = ob->y;
    newobj->obclass = inertobj;
    newobj->active = ac_yes;

    newobj->flags = FL_NEVERMARK;
}


/*
===================
=
= ProjectileTryMove
=
= returns true if move ok
===================
*/

#define PROJSIZE        0x2000

boolean ProjectileTryMove (objtype *ob, objtype* &hitobj)
{
    int      xl,yl,xh,yh,x,y;
    objtype *check;
    bool     flameproj;

    xl = (ob->x-PROJSIZE) >> TILESHIFT;
    yl = (ob->y-PROJSIZE) >> TILESHIFT;

    xh = (ob->x+PROJSIZE) >> TILESHIFT;
    yh = (ob->y+PROJSIZE) >> TILESHIFT;

    flameproj = (ob->obclass == flameobj);

    //
    // check for solid walls
    //
    for (y=yl;y<=yh;y++)
    {
        for (x=xl;x<=xh;x++)
        {
            check = actorat[x][y];
            if (check && ISPOINTER(check) && (check->flags & FL_PROJHITS) != 0)
            {
                if (!flameproj)
                {
                    if ((check->flags & (FL_SHOOTABLE|FL_DAMAGEABLE)) == FL_SHOOTABLE)
                    {
                        hitobj = check;
                        return false;
                    }
                }
                else
                {
                    if ((check->flags & (FL_SHOOTABLE|FL_DAMAGEABLE)) != 0)
                    {
                        hitobj = check;
                        return false;
                    }
                }
            }
            else if (check && !ISPOINTER(check))
                return false;
        }
    }

    return true;
}



/*
=================
=
= T_Projectile
=
=================
*/

void T_Projectile (objtype *ob)
{
    int32_t deltax,deltay;
    int     damage;
    int32_t speed;
    int c;
    int hitplayer;
    bool killed;
    objtype *shooter, *hitobj;

    speed = (int32_t)ob->speed*tics;

    deltax = FixedMul(speed,costable[ob->angle]);
    deltay = -FixedMul(speed,sintable[ob->angle]);

    if (deltax>0x10000l)
        deltax = 0x10000l;
    if (deltay>0x10000l)
        deltay = 0x10000l;

    ob->x += deltax;
    ob->y += deltay;

    damage = 0;
    switch (ob->obclass)
    {
    case needleobj:
        damage = (US_RndT() >>3) + 20;
        break;
    case rocketobj:
    case hrocketobj:
    case sparkobj:
        damage = (US_RndT() >>3) + 30;
        break;
    case fireobj:
    case flameobj:
        damage = (US_RndT() >>3);
        break;
    }

    hitobj = NULL;
    if (!ProjectileTryMove (ob, hitobj))
    {
        if (ob->obclass == flameobj)
        {
            PlaySoundLocActor(FLAMEHITSND,ob);
            ob->state = &s_flameboom1;
        }
#ifndef APOGEE_1_0          // actually the whole method is never reached in shareware 1.0
        else if (ob->obclass == rocketobj)
        {
            PlaySoundLocActor(MISSILEHITSND,ob);
            ob->state = &s_boom1;
        }
#ifdef SPEAR
        else if (ob->obclass == hrocketobj)
        {
            PlaySoundLocActor(MISSILEHITSND,ob);
            ob->state = &s_hboom1;
        }
#endif
        else
#endif
            ob->state = NULL;               // mark for removal

        if (hitobj)
        {
            DamageActor(hitobj, damage);
        }
        return;
    }

    hitplayer = -1;
    for (LWMP_REPEAT(c))
    {
        // do not hit shooter if set
        if(ob->shooterobjid == (c+1))
            continue;
        deltax = LABS(ob->x - player_mp->x);
        deltay = LABS(ob->y - player_mp->y);
        if (deltax < PROJECTILESIZE && deltay < PROJECTILESIZE)
        {
            hitplayer = LWMP_GetInstance();
            LWMP_ENDREPEAT(c);
            break;
        }
    }

    if (hitplayer >= 0)
    {       // hit the player
        killed = false;
        for (LWMP_REPEAT_ONCE(c, hitplayer))
        {
            killed = TakeDamage (damage,ob);
            if (killed && ob->shooterobjid != 0)
            {
                shooter = &objlist[ob->shooterobjid - 1];
                shooter->fragcount++;
            }
        }
        if (killed && ob->shooterobjid != 0)
        {
            for (LWMP_REPEAT_ONCE(c, ob->shooterobjid - 1))
            {
                sb(DrawLives());
            }
        }
        ob->state = NULL;               // mark for removal
        return;
    }

    ob->tilex = (short)(ob->x >> TILESHIFT);
    ob->tiley = (short)(ob->y >> TILESHIFT);
}


/*
=============================================================================

GUARD

=============================================================================
*/

//
// guards
//

extern  statetype s_grdstand;

extern  statetype s_grdpath1;
extern  statetype s_grdpath1s;
extern  statetype s_grdpath2;
extern  statetype s_grdpath3;
extern  statetype s_grdpath3s;
extern  statetype s_grdpath4;

extern  statetype s_grdpain;
extern  statetype s_grdpain1;

extern  statetype s_grdgiveup;

extern  statetype s_grdshoot1;
extern  statetype s_grdshoot2;
extern  statetype s_grdshoot3;
extern  statetype s_grdshoot4;

extern  statetype s_grdchase1;
extern  statetype s_grdchase1s;
extern  statetype s_grdchase2;
extern  statetype s_grdchase3;
extern  statetype s_grdchase3s;
extern  statetype s_grdchase4;

extern  statetype s_grddie1;
extern  statetype s_grddie1d;
extern  statetype s_grddie2;
extern  statetype s_grddie3;
extern  statetype s_grddie4;

statetype s_grdstand            = {true,SPR_GRD_S_1,0,(statefunc)T_Stand,NULL,&s_grdstand};

statetype s_grdpath1            = {true,SPR_GRD_W1_1,20,(statefunc)T_Path,NULL,&s_grdpath1s};
statetype s_grdpath1s           = {true,SPR_GRD_W1_1,5,NULL,NULL,&s_grdpath2};
statetype s_grdpath2            = {true,SPR_GRD_W2_1,15,(statefunc)T_Path,NULL,&s_grdpath3};
statetype s_grdpath3            = {true,SPR_GRD_W3_1,20,(statefunc)T_Path,NULL,&s_grdpath3s};
statetype s_grdpath3s           = {true,SPR_GRD_W3_1,5,NULL,NULL,&s_grdpath4};
statetype s_grdpath4            = {true,SPR_GRD_W4_1,15,(statefunc)T_Path,NULL,&s_grdpath1};

statetype s_grdpain             = {2,SPR_GRD_PAIN_1,10,NULL,NULL,&s_grdchase1};
statetype s_grdpain1            = {2,SPR_GRD_PAIN_2,10,NULL,NULL,&s_grdchase1};

statetype s_grdshoot1           = {false,SPR_GRD_SHOOT1,20,NULL,NULL,&s_grdshoot2};
statetype s_grdshoot2           = {false,SPR_GRD_SHOOT2,20,NULL,(statefunc)T_Shoot,&s_grdshoot3};
statetype s_grdshoot3           = {false,SPR_GRD_SHOOT3,20,NULL,NULL,&s_grdchase1};

statetype s_grdchase1           = {true,SPR_GRD_W1_1,10,(statefunc)T_Chase,NULL,&s_grdchase1s};
statetype s_grdchase1s          = {true,SPR_GRD_W1_1,3,NULL,NULL,&s_grdchase2};
statetype s_grdchase2           = {true,SPR_GRD_W2_1,8,(statefunc)T_Chase,NULL,&s_grdchase3};
statetype s_grdchase3           = {true,SPR_GRD_W3_1,10,(statefunc)T_Chase,NULL,&s_grdchase3s};
statetype s_grdchase3s          = {true,SPR_GRD_W3_1,3,NULL,NULL,&s_grdchase4};
statetype s_grdchase4           = {true,SPR_GRD_W4_1,8,(statefunc)T_Chase,NULL,&s_grdchase1};

statetype s_grddie1             = {false,SPR_GRD_DIE_1,15,(statefunc)Enemy::die,(statefunc)A_DeathScream,&s_grddie2};
statetype s_grddie2             = {false,SPR_GRD_DIE_2,15,(statefunc)Enemy::die,NULL,&s_grddie3};
statetype s_grddie3             = {false,SPR_GRD_DIE_3,15,(statefunc)Enemy::die,NULL,&s_grddie4};
statetype s_grddie4             = {false,SPR_GRD_DEAD,0,(statefunc)Enemy::die,NULL,&s_grddie4};


#ifndef SPEAR
//
// ghosts
//
extern  statetype s_blinkychase1;
extern  statetype s_blinkychase2;
extern  statetype s_inkychase1;
extern  statetype s_inkychase2;
extern  statetype s_pinkychase1;
extern  statetype s_pinkychase2;
extern  statetype s_clydechase1;
extern  statetype s_clydechase2;

statetype s_blinkychase1        = {false,SPR_BLINKY_W1,10,(statefunc)T_Ghosts,NULL,&s_blinkychase2};
statetype s_blinkychase2        = {false,SPR_BLINKY_W2,10,(statefunc)T_Ghosts,NULL,&s_blinkychase1};

statetype s_inkychase1          = {false,SPR_INKY_W1,10,(statefunc)T_Ghosts,NULL,&s_inkychase2};
statetype s_inkychase2          = {false,SPR_INKY_W2,10,(statefunc)T_Ghosts,NULL,&s_inkychase1};

statetype s_pinkychase1         = {false,SPR_PINKY_W1,10,(statefunc)T_Ghosts,NULL,&s_pinkychase2};
statetype s_pinkychase2         = {false,SPR_PINKY_W2,10,(statefunc)T_Ghosts,NULL,&s_pinkychase1};

statetype s_clydechase1         = {false,SPR_CLYDE_W1,10,(statefunc)T_Ghosts,NULL,&s_clydechase2};
statetype s_clydechase2         = {false,SPR_CLYDE_W2,10,(statefunc)T_Ghosts,NULL,&s_clydechase1};
#endif

//
// dogs
//

extern  statetype s_dogpath1;
extern  statetype s_dogpath1s;
extern  statetype s_dogpath2;
extern  statetype s_dogpath3;
extern  statetype s_dogpath3s;
extern  statetype s_dogpath4;

extern  statetype s_dogjump1;
extern  statetype s_dogjump2;
extern  statetype s_dogjump3;
extern  statetype s_dogjump4;
extern  statetype s_dogjump5;

extern  statetype s_dogchase1;
extern  statetype s_dogchase1s;
extern  statetype s_dogchase2;
extern  statetype s_dogchase3;
extern  statetype s_dogchase3s;
extern  statetype s_dogchase4;

extern  statetype s_dogdie1;
extern  statetype s_dogdie1d;
extern  statetype s_dogdie2;
extern  statetype s_dogdie3;
extern  statetype s_dogdead;

statetype s_dogpath1            = {true,SPR_DOG_W1_1,20,(statefunc)T_Path,NULL,&s_dogpath1s};
statetype s_dogpath1s           = {true,SPR_DOG_W1_1,5,NULL,NULL,&s_dogpath2};
statetype s_dogpath2            = {true,SPR_DOG_W2_1,15,(statefunc)T_Path,NULL,&s_dogpath3};
statetype s_dogpath3            = {true,SPR_DOG_W3_1,20,(statefunc)T_Path,NULL,&s_dogpath3s};
statetype s_dogpath3s           = {true,SPR_DOG_W3_1,5,NULL,NULL,&s_dogpath4};
statetype s_dogpath4            = {true,SPR_DOG_W4_1,15,(statefunc)T_Path,NULL,&s_dogpath1};

statetype s_dogjump1            = {false,SPR_DOG_JUMP1,10,NULL,NULL,&s_dogjump2};
statetype s_dogjump2            = {false,SPR_DOG_JUMP2,10,NULL,(statefunc)T_Bite,&s_dogjump3};
statetype s_dogjump3            = {false,SPR_DOG_JUMP3,10,NULL,NULL,&s_dogjump4};
statetype s_dogjump4            = {false,SPR_DOG_JUMP1,10,NULL,NULL,&s_dogjump5};
statetype s_dogjump5            = {false,SPR_DOG_W1_1,10,NULL,NULL,&s_dogchase1};

statetype s_dogchase1           = {true,SPR_DOG_W1_1,10,(statefunc)T_DogChase,NULL,&s_dogchase1s};
statetype s_dogchase1s          = {true,SPR_DOG_W1_1,3,NULL,NULL,&s_dogchase2};
statetype s_dogchase2           = {true,SPR_DOG_W2_1,8,(statefunc)T_DogChase,NULL,&s_dogchase3};
statetype s_dogchase3           = {true,SPR_DOG_W3_1,10,(statefunc)T_DogChase,NULL,&s_dogchase3s};
statetype s_dogchase3s          = {true,SPR_DOG_W3_1,3,NULL,NULL,&s_dogchase4};
statetype s_dogchase4           = {true,SPR_DOG_W4_1,8,(statefunc)T_DogChase,NULL,&s_dogchase1};

statetype s_dogdie1             = {false,SPR_DOG_DIE_1,15,(statefunc)Enemy::die,(statefunc)A_DeathScream,&s_dogdie2};
statetype s_dogdie2             = {false,SPR_DOG_DIE_2,15,(statefunc)Enemy::die,NULL,&s_dogdie3};
statetype s_dogdie3             = {false,SPR_DOG_DIE_3,15,(statefunc)Enemy::die,NULL,&s_dogdead};
statetype s_dogdead             = {false,SPR_DOG_DEAD,15,(statefunc)Enemy::die,NULL,&s_dogdead};

statetype s_dogpain             = {2,SPR_DOG_DIE_1,10,NULL,(statefunc)KillActor,&s_dogdie1};

//
// officers
//

extern  statetype s_ofcstand;

extern  statetype s_ofcpath1;
extern  statetype s_ofcpath1s;
extern  statetype s_ofcpath2;
extern  statetype s_ofcpath3;
extern  statetype s_ofcpath3s;
extern  statetype s_ofcpath4;

extern  statetype s_ofcpain;
extern  statetype s_ofcpain1;

extern  statetype s_ofcgiveup;

extern  statetype s_ofcshoot1;
extern  statetype s_ofcshoot2;
extern  statetype s_ofcshoot3;
extern  statetype s_ofcshoot4;

extern  statetype s_ofcchase1;
extern  statetype s_ofcchase1s;
extern  statetype s_ofcchase2;
extern  statetype s_ofcchase3;
extern  statetype s_ofcchase3s;
extern  statetype s_ofcchase4;

extern  statetype s_ofcdie1;
extern  statetype s_ofcdie2;
extern  statetype s_ofcdie3;
extern  statetype s_ofcdie4;
extern  statetype s_ofcdie5;

statetype s_ofcstand            = {true,SPR_OFC_S_1,0,(statefunc)T_Stand,NULL,&s_ofcstand};

statetype s_ofcpath1            = {true,SPR_OFC_W1_1,20,(statefunc)T_Path,NULL,&s_ofcpath1s};
statetype s_ofcpath1s           = {true,SPR_OFC_W1_1,5,NULL,NULL,&s_ofcpath2};
statetype s_ofcpath2            = {true,SPR_OFC_W2_1,15,(statefunc)T_Path,NULL,&s_ofcpath3};
statetype s_ofcpath3            = {true,SPR_OFC_W3_1,20,(statefunc)T_Path,NULL,&s_ofcpath3s};
statetype s_ofcpath3s           = {true,SPR_OFC_W3_1,5,NULL,NULL,&s_ofcpath4};
statetype s_ofcpath4            = {true,SPR_OFC_W4_1,15,(statefunc)T_Path,NULL,&s_ofcpath1};

statetype s_ofcpain             = {2,SPR_OFC_PAIN_1,10,NULL,NULL,&s_ofcchase1};
statetype s_ofcpain1            = {2,SPR_OFC_PAIN_2,10,NULL,NULL,&s_ofcchase1};

statetype s_ofcshoot1           = {false,SPR_OFC_SHOOT1,6,NULL,NULL,&s_ofcshoot2};
statetype s_ofcshoot2           = {false,SPR_OFC_SHOOT2,20,NULL,(statefunc)T_Shoot,&s_ofcshoot3};
statetype s_ofcshoot3           = {false,SPR_OFC_SHOOT3,10,NULL,NULL,&s_ofcchase1};

statetype s_ofcchase1           = {true,SPR_OFC_W1_1,10,(statefunc)T_Chase,NULL,&s_ofcchase1s};
statetype s_ofcchase1s          = {true,SPR_OFC_W1_1,3,NULL,NULL,&s_ofcchase2};
statetype s_ofcchase2           = {true,SPR_OFC_W2_1,8,(statefunc)T_Chase,NULL,&s_ofcchase3};
statetype s_ofcchase3           = {true,SPR_OFC_W3_1,10,(statefunc)T_Chase,NULL,&s_ofcchase3s};
statetype s_ofcchase3s          = {true,SPR_OFC_W3_1,3,NULL,NULL,&s_ofcchase4};
statetype s_ofcchase4           = {true,SPR_OFC_W4_1,8,(statefunc)T_Chase,NULL,&s_ofcchase1};

statetype s_ofcdie1             = {false,SPR_OFC_DIE_1,11,(statefunc)Enemy::die,(statefunc)A_DeathScream,&s_ofcdie2};
statetype s_ofcdie2             = {false,SPR_OFC_DIE_2,11,(statefunc)Enemy::die,NULL,&s_ofcdie3};
statetype s_ofcdie3             = {false,SPR_OFC_DIE_3,11,(statefunc)Enemy::die,NULL,&s_ofcdie4};
statetype s_ofcdie4             = {false,SPR_OFC_DIE_4,11,(statefunc)Enemy::die,NULL,&s_ofcdie5};
statetype s_ofcdie5             = {false,SPR_OFC_DEAD,0,(statefunc)Enemy::die,NULL,&s_ofcdie5};


//
// mutant
//

extern  statetype s_mutstand;

extern  statetype s_mutpath1;
extern  statetype s_mutpath1s;
extern  statetype s_mutpath2;
extern  statetype s_mutpath3;
extern  statetype s_mutpath3s;
extern  statetype s_mutpath4;

extern  statetype s_mutpain;
extern  statetype s_mutpain1;

extern  statetype s_mutgiveup;

extern  statetype s_mutshoot1;
extern  statetype s_mutshoot2;
extern  statetype s_mutshoot3;
extern  statetype s_mutshoot4;

extern  statetype s_mutchase1;
extern  statetype s_mutchase1s;
extern  statetype s_mutchase2;
extern  statetype s_mutchase3;
extern  statetype s_mutchase3s;
extern  statetype s_mutchase4;

extern  statetype s_mutdie1;
extern  statetype s_mutdie2;
extern  statetype s_mutdie3;
extern  statetype s_mutdie4;
extern  statetype s_mutdie5;

statetype s_mutstand            = {true,SPR_MUT_S_1,0,(statefunc)T_Stand,NULL,&s_mutstand};

statetype s_mutpath1            = {true,SPR_MUT_W1_1,20,(statefunc)T_Path,NULL,&s_mutpath1s};
statetype s_mutpath1s           = {true,SPR_MUT_W1_1,5,NULL,NULL,&s_mutpath2};
statetype s_mutpath2            = {true,SPR_MUT_W2_1,15,(statefunc)T_Path,NULL,&s_mutpath3};
statetype s_mutpath3            = {true,SPR_MUT_W3_1,20,(statefunc)T_Path,NULL,&s_mutpath3s};
statetype s_mutpath3s           = {true,SPR_MUT_W3_1,5,NULL,NULL,&s_mutpath4};
statetype s_mutpath4            = {true,SPR_MUT_W4_1,15,(statefunc)T_Path,NULL,&s_mutpath1};

statetype s_mutpain             = {2,SPR_MUT_PAIN_1,10,NULL,NULL,&s_mutchase1};
statetype s_mutpain1            = {2,SPR_MUT_PAIN_2,10,NULL,NULL,&s_mutchase1};

statetype s_mutshoot1           = {false,SPR_MUT_SHOOT1,6,NULL,(statefunc)T_Shoot,&s_mutshoot2};
statetype s_mutshoot2           = {false,SPR_MUT_SHOOT2,20,NULL,NULL,&s_mutshoot3};
statetype s_mutshoot3           = {false,SPR_MUT_SHOOT3,10,NULL,(statefunc)T_Shoot,&s_mutshoot4};
statetype s_mutshoot4           = {false,SPR_MUT_SHOOT4,20,NULL,NULL,&s_mutchase1};

statetype s_mutchase1           = {true,SPR_MUT_W1_1,10,(statefunc)T_Chase,NULL,&s_mutchase1s};
statetype s_mutchase1s          = {true,SPR_MUT_W1_1,3,NULL,NULL,&s_mutchase2};
statetype s_mutchase2           = {true,SPR_MUT_W2_1,8,(statefunc)T_Chase,NULL,&s_mutchase3};
statetype s_mutchase3           = {true,SPR_MUT_W3_1,10,(statefunc)T_Chase,NULL,&s_mutchase3s};
statetype s_mutchase3s          = {true,SPR_MUT_W3_1,3,NULL,NULL,&s_mutchase4};
statetype s_mutchase4           = {true,SPR_MUT_W4_1,8,(statefunc)T_Chase,NULL,&s_mutchase1};

statetype s_mutdie1             = {false,SPR_MUT_DIE_1,7,(statefunc)Enemy::die,(statefunc)A_DeathScream,&s_mutdie2};
statetype s_mutdie2             = {false,SPR_MUT_DIE_2,7,(statefunc)Enemy::die,NULL,&s_mutdie3};
statetype s_mutdie3             = {false,SPR_MUT_DIE_3,7,(statefunc)Enemy::die,NULL,&s_mutdie4};
statetype s_mutdie4             = {false,SPR_MUT_DIE_4,7,(statefunc)Enemy::die,NULL,&s_mutdie5};
statetype s_mutdie5             = {false,SPR_MUT_DEAD,0,(statefunc)Enemy::die,NULL,&s_mutdie5};


//
// SS
//

extern  statetype s_ssstand;

extern  statetype s_sspath1;
extern  statetype s_sspath1s;
extern  statetype s_sspath2;
extern  statetype s_sspath3;
extern  statetype s_sspath3s;
extern  statetype s_sspath4;

extern  statetype s_sspain;
extern  statetype s_sspain1;

extern  statetype s_ssshoot1;
extern  statetype s_ssshoot2;
extern  statetype s_ssshoot3;
extern  statetype s_ssshoot4;
extern  statetype s_ssshoot5;
extern  statetype s_ssshoot6;
extern  statetype s_ssshoot7;
extern  statetype s_ssshoot8;
extern  statetype s_ssshoot9;

extern  statetype s_sschase1;
extern  statetype s_sschase1s;
extern  statetype s_sschase2;
extern  statetype s_sschase3;
extern  statetype s_sschase3s;
extern  statetype s_sschase4;

extern  statetype s_ssdie1;
extern  statetype s_ssdie2;
extern  statetype s_ssdie3;
extern  statetype s_ssdie4;

statetype s_ssstand             = {true,SPR_SS_S_1,0,(statefunc)T_Stand,NULL,&s_ssstand};

statetype s_sspath1             = {true,SPR_SS_W1_1,20,(statefunc)T_Path,NULL,&s_sspath1s};
statetype s_sspath1s            = {true,SPR_SS_W1_1,5,NULL,NULL,&s_sspath2};
statetype s_sspath2             = {true,SPR_SS_W2_1,15,(statefunc)T_Path,NULL,&s_sspath3};
statetype s_sspath3             = {true,SPR_SS_W3_1,20,(statefunc)T_Path,NULL,&s_sspath3s};
statetype s_sspath3s            = {true,SPR_SS_W3_1,5,NULL,NULL,&s_sspath4};
statetype s_sspath4             = {true,SPR_SS_W4_1,15,(statefunc)T_Path,NULL,&s_sspath1};

statetype s_sspain              = {2,SPR_SS_PAIN_1,10,NULL,NULL,&s_sschase1};
statetype s_sspain1             = {2,SPR_SS_PAIN_2,10,NULL,NULL,&s_sschase1};

statetype s_ssshoot1            = {false,SPR_SS_SHOOT1,20,NULL,NULL,&s_ssshoot2};
statetype s_ssshoot2            = {false,SPR_SS_SHOOT2,20,NULL,(statefunc)T_Shoot,&s_ssshoot3};
statetype s_ssshoot3            = {false,SPR_SS_SHOOT3,10,NULL,NULL,&s_ssshoot4};
statetype s_ssshoot4            = {false,SPR_SS_SHOOT2,10,NULL,(statefunc)T_Shoot,&s_ssshoot5};
statetype s_ssshoot5            = {false,SPR_SS_SHOOT3,10,NULL,NULL,&s_ssshoot6};
statetype s_ssshoot6            = {false,SPR_SS_SHOOT2,10,NULL,(statefunc)T_Shoot,&s_ssshoot7};
statetype s_ssshoot7            = {false,SPR_SS_SHOOT3,10,NULL,NULL,&s_ssshoot8};
statetype s_ssshoot8            = {false,SPR_SS_SHOOT2,10,NULL,(statefunc)T_Shoot,&s_ssshoot9};
statetype s_ssshoot9            = {false,SPR_SS_SHOOT3,10,NULL,NULL,&s_sschase1};

statetype s_sschase1            = {true,SPR_SS_W1_1,10,(statefunc)T_Chase,NULL,&s_sschase1s};
statetype s_sschase1s           = {true,SPR_SS_W1_1,3,NULL,NULL,&s_sschase2};
statetype s_sschase2            = {true,SPR_SS_W2_1,8,(statefunc)T_Chase,NULL,&s_sschase3};
statetype s_sschase3            = {true,SPR_SS_W3_1,10,(statefunc)T_Chase,NULL,&s_sschase3s};
statetype s_sschase3s           = {true,SPR_SS_W3_1,3,NULL,NULL,&s_sschase4};
statetype s_sschase4            = {true,SPR_SS_W4_1,8,(statefunc)T_Chase,NULL,&s_sschase1};

statetype s_ssdie1              = {false,SPR_SS_DIE_1,15,(statefunc)Enemy::die,(statefunc)A_DeathScream,&s_ssdie2};
statetype s_ssdie2              = {false,SPR_SS_DIE_2,15,(statefunc)Enemy::die,NULL,&s_ssdie3};
statetype s_ssdie3              = {false,SPR_SS_DIE_3,15,(statefunc)Enemy::die,NULL,&s_ssdie4};
statetype s_ssdie4              = {false,SPR_SS_DEAD,0,(statefunc)Enemy::die,NULL,&s_ssdie4};


#ifndef SPEAR
//
// hans
//
extern  statetype s_bossstand;

extern  statetype s_bosschase1;
extern  statetype s_bosschase1s;
extern  statetype s_bosschase2;
extern  statetype s_bosschase3;
extern  statetype s_bosschase3s;
extern  statetype s_bosschase4;

extern  statetype s_bossdie1;
extern  statetype s_bossdie2;
extern  statetype s_bossdie3;
extern  statetype s_bossdie4;

extern  statetype s_bossshoot1;
extern  statetype s_bossshoot2;
extern  statetype s_bossshoot3;
extern  statetype s_bossshoot4;
extern  statetype s_bossshoot5;
extern  statetype s_bossshoot6;
extern  statetype s_bossshoot7;
extern  statetype s_bossshoot8;


statetype s_bossstand           = {false,SPR_BOSS_W1,0,(statefunc)T_Stand,NULL,&s_bossstand};

statetype s_bosschase1          = {false,SPR_BOSS_W1,10,(statefunc)T_Chase,NULL,&s_bosschase1s};
statetype s_bosschase1s         = {false,SPR_BOSS_W1,3,NULL,NULL,&s_bosschase2};
statetype s_bosschase2          = {false,SPR_BOSS_W2,8,(statefunc)T_Chase,NULL,&s_bosschase3};
statetype s_bosschase3          = {false,SPR_BOSS_W3,10,(statefunc)T_Chase,NULL,&s_bosschase3s};
statetype s_bosschase3s         = {false,SPR_BOSS_W3,3,NULL,NULL,&s_bosschase4};
statetype s_bosschase4          = {false,SPR_BOSS_W4,8,(statefunc)T_Chase,NULL,&s_bosschase1};

statetype s_bossdie1            = {false,SPR_BOSS_DIE1,15,NULL,(statefunc)A_DeathScream,&s_bossdie2};
statetype s_bossdie2            = {false,SPR_BOSS_DIE2,15,NULL,NULL,&s_bossdie3};
statetype s_bossdie3            = {false,SPR_BOSS_DIE3,15,NULL,NULL,&s_bossdie4};
statetype s_bossdie4            = {false,SPR_BOSS_DEAD,0,NULL,NULL,&s_bossdie4};

statetype s_bossshoot1          = {false,SPR_BOSS_SHOOT1,30,NULL,NULL,&s_bossshoot2};
statetype s_bossshoot2          = {false,SPR_BOSS_SHOOT2,10,NULL,(statefunc)T_Shoot,&s_bossshoot3};
statetype s_bossshoot3          = {false,SPR_BOSS_SHOOT3,10,NULL,(statefunc)T_Shoot,&s_bossshoot4};
statetype s_bossshoot4          = {false,SPR_BOSS_SHOOT2,10,NULL,(statefunc)T_Shoot,&s_bossshoot5};
statetype s_bossshoot5          = {false,SPR_BOSS_SHOOT3,10,NULL,(statefunc)T_Shoot,&s_bossshoot6};
statetype s_bossshoot6          = {false,SPR_BOSS_SHOOT2,10,NULL,(statefunc)T_Shoot,&s_bossshoot7};
statetype s_bossshoot7          = {false,SPR_BOSS_SHOOT3,10,NULL,(statefunc)T_Shoot,&s_bossshoot8};
statetype s_bossshoot8          = {false,SPR_BOSS_SHOOT1,10,NULL,NULL,&s_bosschase1};


//
// gretel
//
extern  statetype s_gretelstand;

extern  statetype s_gretelchase1;
extern  statetype s_gretelchase1s;
extern  statetype s_gretelchase2;
extern  statetype s_gretelchase3;
extern  statetype s_gretelchase3s;
extern  statetype s_gretelchase4;

extern  statetype s_greteldie1;
extern  statetype s_greteldie2;
extern  statetype s_greteldie3;
extern  statetype s_greteldie4;

extern  statetype s_gretelshoot1;
extern  statetype s_gretelshoot2;
extern  statetype s_gretelshoot3;
extern  statetype s_gretelshoot4;
extern  statetype s_gretelshoot5;
extern  statetype s_gretelshoot6;
extern  statetype s_gretelshoot7;
extern  statetype s_gretelshoot8;


statetype s_gretelstand         = {false,SPR_GRETEL_W1,0,(statefunc)T_Stand,NULL,&s_gretelstand};

statetype s_gretelchase1        = {false,SPR_GRETEL_W1,10,(statefunc)T_Chase,NULL,&s_gretelchase1s};
statetype s_gretelchase1s       = {false,SPR_GRETEL_W1,3,NULL,NULL,&s_gretelchase2};
statetype s_gretelchase2        = {false,SPR_GRETEL_W2,8,(statefunc)T_Chase,NULL,&s_gretelchase3};
statetype s_gretelchase3        = {false,SPR_GRETEL_W3,10,(statefunc)T_Chase,NULL,&s_gretelchase3s};
statetype s_gretelchase3s       = {false,SPR_GRETEL_W3,3,NULL,NULL,&s_gretelchase4};
statetype s_gretelchase4        = {false,SPR_GRETEL_W4,8,(statefunc)T_Chase,NULL,&s_gretelchase1};

statetype s_greteldie1          = {false,SPR_GRETEL_DIE1,15,NULL,(statefunc)A_DeathScream,&s_greteldie2};
statetype s_greteldie2          = {false,SPR_GRETEL_DIE2,15,NULL,NULL,&s_greteldie3};
statetype s_greteldie3          = {false,SPR_GRETEL_DIE3,15,NULL,NULL,&s_greteldie4};
statetype s_greteldie4          = {false,SPR_GRETEL_DEAD,0,NULL,NULL,&s_greteldie4};

statetype s_gretelshoot1        = {false,SPR_GRETEL_SHOOT1,30,NULL,NULL,&s_gretelshoot2};
statetype s_gretelshoot2        = {false,SPR_GRETEL_SHOOT2,10,NULL,(statefunc)T_Shoot,&s_gretelshoot3};
statetype s_gretelshoot3        = {false,SPR_GRETEL_SHOOT3,10,NULL,(statefunc)T_Shoot,&s_gretelshoot4};
statetype s_gretelshoot4        = {false,SPR_GRETEL_SHOOT2,10,NULL,(statefunc)T_Shoot,&s_gretelshoot5};
statetype s_gretelshoot5        = {false,SPR_GRETEL_SHOOT3,10,NULL,(statefunc)T_Shoot,&s_gretelshoot6};
statetype s_gretelshoot6        = {false,SPR_GRETEL_SHOOT2,10,NULL,(statefunc)T_Shoot,&s_gretelshoot7};
statetype s_gretelshoot7        = {false,SPR_GRETEL_SHOOT3,10,NULL,(statefunc)T_Shoot,&s_gretelshoot8};
statetype s_gretelshoot8        = {false,SPR_GRETEL_SHOOT1,10,NULL,NULL,&s_gretelchase1};
#endif


/*
===============
=
= SpawnStand
=
===============
*/

void SpawnStand (enemy_t which, int tilex, int tiley, int dir)
{
    word *map;
    word tile;

    switch (which)
    {
        case en_guard:
            if (BjMutantMode::enabled())
            {
                SpawnNewObj (tilex,tiley,&s_bjmutantstand);
                newobj->bjMutantWeapon = wp_pistol;
                newobj->bjMutantChaseFactor = 4;
            }
            else
            {
                SpawnNewObj (tilex,tiley,&s_grdstand);
            }
            newobj->speed = SPDPATROL;
            if (!loadedgame)
                gamestate.killtotal++;
            break;

        case en_officer:
            if (BjMutantMode::enabled())
            {
                SpawnNewObj (tilex,tiley,&s_bjmutantstand);
                newobj->bjMutantWeapon = wp_pistol;
                newobj->bjMutantChaseFactor = 5;
            }
            else
            {
                SpawnNewObj (tilex,tiley,&s_ofcstand);
            }
            newobj->speed = SPDPATROL;
            if (!loadedgame)
                gamestate.killtotal++;
            break;

        case en_mutant:
            if (BjMutantMode::enabled())
            {
                SpawnNewObj (tilex,tiley,&s_bjmutantstand);
                newobj->bjMutantWeapon = wp_pistol;
                newobj->bjMutantChaseFactor = 4;
            }
            else
            {
                SpawnNewObj (tilex,tiley,&s_mutstand);
            }
            newobj->speed = SPDPATROL;
            if (!loadedgame)
                gamestate.killtotal++;
            break;

        case en_ss:
            if (BjMutantMode::enabled())
            {
                SpawnNewObj (tilex,tiley,&s_bjmutantstand);
                newobj->bjMutantWeapon = wp_machinegun;
                newobj->bjMutantChaseFactor = 4;
            }
            else
            {
                SpawnNewObj (tilex,tiley,&s_ssstand);
            }
            newobj->speed = SPDPATROL;
            if (!loadedgame)
                gamestate.killtotal++;
            break;
    }

    BjMutantMode::replaceWeap(newobj->bjMutantWeapon);

    map = mapsegs[0]+(tiley<<mapshift)+tilex;
    tile = *map;
    if (tile == AMBUSHTILE)
    {
        tilemap[tilex][tiley] = 0;

        if (*(map+1) >= AREATILE)
            tile = *(map+1);
        if (*(map-mapwidth) >= AREATILE)
            tile = *(map-mapwidth);
        if (*(map+mapwidth) >= AREATILE)
            tile = *(map+mapwidth);
        if ( *(map-1) >= AREATILE)
            tile = *(map-1);

        *map = tile;
        newobj->areanumber = tile-AREATILE;

        newobj->flags |= FL_AMBUSH;
    }

    if (BjMutantMode::enabled())
    {
        newobj->obclass = bjmutantobj;
        newobj->player_index = -1;
    }
    else
    {
        newobj->obclass = (classtype)(guardobj + which);
    }
    newobj->hitpoints = starthitpoints[gamestate.difficulty][which];
    BjMutantMode::scaleHp(newobj->hitpoints);
    ZombieMode::scaleHp(newobj->hitpoints);
    ZombieMode::scaleSpeed(newobj->speed);
    ZombieMode::setShade(newobj->shade);
    newobj->dir = (dirtype)(dir * 2);
    newobj->flags |= FL_SHOOTABLE;
    newobj->starthitpoints = newobj->hitpoints;
    newobj->active = ac_yes;

    SpawnSpecialObjects::enemyAt(tilex, tiley, newobj->hitpoints,
        newobj->areanumber);

    CoopDifficulty::spawnStand(which, tilex, tiley, dir);
}


namespace CoopDifficulty
{
    static int pendingSpawnCount = 0;
    static std::set<lwlib::Point2i> placedSpots;

    void reset(void)
    {
        pendingSpawnCount = 0;
        placedSpots.clear();
    }

    void spawnStand (enemy_t which, int tilex, int tiley, int dir)
    {
        word *map;
        word tile;

        map = mapsegs[0]+(tiley<<mapshift)+tilex;

        tile = *map;

        objtype *ob = newobj;

        const int n = LWMP_NUM_PLAYERS - 1;

        typedef std::vector<lwlib::Point2i> SpotVec;

        SpotVec spots;
        getFreeSpots(tilex, tiley, spots);

        int &spawnCount = pendingSpawnCount;
        spawnCount += n;

        for (int i = 0; i < std::min(spawnCount, (int)spots.size()); i++)
        {
            const lwlib::Point2i tilepos = spots[i];
            if (placedSpots.find(tilepos) != placedSpots.end())
            {
                continue;
            }

            placedSpots.insert(placedSpots.begin(), tilepos);

            if (!loadedgame)
                gamestate.killtotal++;

            spawnCount--;
            GetNewActor();
            objtype *prev = newobj->prev;
            *newobj = *ob;
            newobj->next = NULL;
            newobj->prev = prev;

            int x, y;
            vec2_get(tilepos, x, y);

            newobj->tilex = x;
            newobj->tiley = y;
            newobj->x = ((int32_t)x << TILESHIFT) + (TILEGLOBAL / 2);
            newobj->y = ((int32_t)y << TILESHIFT) + (TILEGLOBAL / 2);
        }
    }

    void getFreeSpots(int tilex, int tiley, std::vector<lwlib::Point2i> &spots)
    {
        const int r = 1;

        const lwlib::Point2i low = lwlib::vec2i(tilex - r, tiley - r);
        const lwlib::Point2i high = low + lwlib::vec2i(r * 2 + 1, r * 2 + 1);

        const lwlib::Box2i mapBox = lwlib::box2i(lwlib::vec2i_zero(), 
            lwlib::vec2i(MAPSIZE, MAPSIZE));

        const word areaTile = MAPSPOT(tilex, tiley, 0);

        lwlib::Point2i tilepos = low;
        do
        {
            int x, y;
            vec2_get(tilepos, x, y);

            if (box_test(mapBox, tilepos) && tilemap[x][y] == 0 &&
                MAPSPOT(x,y,1) == 0 &&
                placedSpots.find(tilepos) == placedSpots.end() &&
                MAPSPOT(x,y,0) == areaTile)
            {
                spots.push_back(tilepos);
            }

            tilepos = vec_box_inc(low, high, tilepos);
        } while (tilepos != low);
    }
}


/*
===============
=
= SpawnDeadGuard
=
===============
*/

void SpawnDeadGuard (int tilex, int tiley)
{
    if (BjMutantMode::enabled())
    {
        SpawnNewObj (tilex,tiley,&s_bjmutantdie4);
        newobj->shade = COLORREMAP_SHADE_MAX + 1;
    }
    else
    {
        SpawnNewObj (tilex,tiley,&s_grddie4);
    }
    DEMOIF_SDL
    {
        newobj->flags |= FL_NONMARK;    // walk through moving enemy fix
    }
    newobj->obclass = inertobj;
}



#ifndef SPEAR
/*
===============
=
= SpawnBoss
=
===============
*/

void SpawnBoss (int tilex, int tiley)
{
    SpawnNewObj (tilex,tiley,&s_bossstand);
    newobj->speed = SPDPATROL;

    newobj->obclass = bossobj;
    newobj->hitpoints = starthitpoints[gamestate.difficulty][en_boss] *
        LWMP_BOSSHPFACTOR;
    newobj->dir = nodir;
    newobj->flags |= FL_SHOOTABLE|FL_AMBUSH;
    if (!loadedgame)
        gamestate.killtotal++;
}

/*
===============
=
= SpawnGretel
=
===============
*/

void SpawnGretel (int tilex, int tiley)
{
    SpawnNewObj (tilex,tiley,&s_gretelstand);
    newobj->speed = SPDPATROL;

    newobj->obclass = gretelobj;
    newobj->hitpoints = starthitpoints[gamestate.difficulty][en_gretel] *
        LWMP_BOSSHPFACTOR;
    newobj->dir = nodir;
    newobj->flags |= FL_SHOOTABLE|FL_AMBUSH;
    if (!loadedgame)
        gamestate.killtotal++;
}
#endif

/*
===============
=
= SpawnPatrol
=
===============
*/

void SpawnPatrol (enemy_t which, int tilex, int tiley, int dir)
{
    switch (which)
    {
        case en_guard:
            if (BjMutantMode::enabled())
            {
                SpawnNewObj (tilex,tiley,&s_bjmutantpath1);
                newobj->bjMutantWeapon = wp_pistol;
                newobj->bjMutantChaseFactor = 3;
            }
            else
            {
                SpawnNewObj (tilex,tiley,&s_grdpath1);
            }
            newobj->speed = SPDPATROL;
            if (!loadedgame)
                gamestate.killtotal++;
            break;

        case en_officer:
            if (BjMutantMode::enabled())
            {
                SpawnNewObj (tilex,tiley,&s_bjmutantpath1);
                newobj->bjMutantWeapon = wp_pistol;
                newobj->bjMutantChaseFactor = 5;
            }
            else
            {
                SpawnNewObj (tilex,tiley,&s_ofcpath1);
            }
            newobj->speed = SPDPATROL;
            if (!loadedgame)
                gamestate.killtotal++;
            break;

        case en_ss:
            if (BjMutantMode::enabled())
            {
                SpawnNewObj (tilex,tiley,&s_bjmutantpath1);
                newobj->bjMutantWeapon = wp_machinegun;
                newobj->bjMutantChaseFactor = 4;
            }
            else
            {
                SpawnNewObj (tilex,tiley,&s_sspath1);
            }
            newobj->speed = SPDPATROL;
            if (!loadedgame)
                gamestate.killtotal++;
            break;

        case en_mutant:
            if (BjMutantMode::enabled())
            {
                SpawnNewObj (tilex,tiley,&s_bjmutantpath1);
                newobj->bjMutantWeapon = wp_pistol;
                newobj->bjMutantChaseFactor = 3;
            }
            else
            {
                SpawnNewObj (tilex,tiley,&s_mutpath1);
            }
            newobj->speed = SPDPATROL;
            if (!loadedgame)
                gamestate.killtotal++;
            break;

        case en_dog:
            if (BjMutantMode::enabled())
            {
                SpawnNewObj (tilex,tiley,&s_bjmutantpath1);
                newobj->bjMutantWeapon = wp_knife;
                newobj->bjMutantChaseFactor = 2;
            }
            else
            {
                SpawnNewObj (tilex,tiley,&s_dogpath1);
            }
            newobj->speed = SPDDOG;
            if (!loadedgame)
                gamestate.killtotal++;
            break;
    }

    BjMutantMode::replaceWeap(newobj->bjMutantWeapon);

    if (BjMutantMode::enabled())
    {
        newobj->obclass = bjmutantobj;
        if (which == en_dog)
        {
            newobj->obclass = bjmutantdogobj;
        }
        newobj->player_index = -1;
    }
    else
    {
        newobj->obclass = (classtype)(guardobj+which);
    }
    newobj->dir = (dirtype)(dir*2);
    newobj->hitpoints = starthitpoints[gamestate.difficulty][which];
    BjMutantMode::scaleHp(newobj->hitpoints);
    ZombieMode::scaleHp(newobj->hitpoints);
    ZombieMode::scaleSpeed(newobj->speed);
    ZombieMode::setShade(newobj->shade);
    newobj->distance = TILEGLOBAL;
    newobj->flags |= FL_SHOOTABLE;
    newobj->active = ac_yes;
    newobj->starthitpoints = newobj->hitpoints;

    actorat[newobj->tilex][newobj->tiley] = NULL;           // don't use original spot

    switch (dir)
    {
        case 0:
            newobj->tilex++;
            break;
        case 1:
            newobj->tiley--;
            break;
        case 2:
            newobj->tilex--;
            break;
        case 3:
            newobj->tiley++;
            break;
    }

    actorat[newobj->tilex][newobj->tiley] = newobj;
}



/*
==================
=
= A_DeathScream
=
==================
*/

void A_DeathScream (objtype *ob)
{
    if (ob->gibbed &&
            (ob->gibMethod == GIB_METHOD_ROCKET || ob->gibMethod == GIB_METHOD_FLAME))
    {
        soundnames gibsound =
            (ob->gibMethod == GIB_METHOD_ROCKET ? GIBEXPLODESND : GIBFLAMESND);
        PlaySoundLocActor(gibsound, ob);
        if (ob->obclass != dogobj)
        {
            return;
        }
    }

#ifndef UPLOAD
#ifndef SPEAR
    if (mapon==9 && !US_RndT())
#else
    if ((mapon==18 || mapon==19) && !US_RndT())
#endif
    {
        switch(ob->obclass)
        {
            case mutantobj:
            case guardobj:
            case officerobj:
            case ssobj:
            case dogobj:
                PlaySoundLocActor(DEATHSCREAM6SND,ob);
                return;
        }
    }
#endif

    switch (ob->obclass)
    {
        case playerobj:
            PlaySoundLocActor(BJDIESND,ob);
            break;

        case mutantobj:
        case bjmutantobj:
        case bjmutantdogobj:
            PlaySoundLocActor(AHHHGSND,ob);
            break;

        case guardobj:
        {
            if (Enemy::dieSound(ob) != HITWALLSND)
            {
                PlaySoundLocActor(Enemy::dieSound(ob),ob);
                break;
            }

            int sounds[9]={ DEATHSCREAM1SND,
                DEATHSCREAM2SND,
                DEATHSCREAM3SND,
#ifndef APOGEE_1_0
                DEATHSCREAM4SND,
                DEATHSCREAM5SND,
                DEATHSCREAM7SND,
                DEATHSCREAM8SND,
                DEATHSCREAM9SND
#endif
            };

#ifndef UPLOAD
            PlaySoundLocActor(sounds[US_RndT()%8],ob);
#else
            PlaySoundLocActor(sounds[US_RndT()%2],ob);
#endif
            break;
        }
        case officerobj:
            PlaySoundLocActor(Enemy::dieSound(ob),ob);
            break;
        case ssobj:
            PlaySoundLocActor(Enemy::dieSound(ob),ob); // JAB
            break;
        case dogobj:
            PlaySoundLocActor(Enemy::dieSound(ob),ob);      // JAB
            break;
#ifndef SPEAR
        case bossobj:
            SD_PlaySound(MUTTISND);                         // JAB
            break;
        case schabbobj:
            SD_PlaySound(MEINGOTTSND);
            break;
        case fakeobj:
            SD_PlaySound(HITLERHASND);
            break;
        case mechahitlerobj:
            SD_PlaySound(SCHEISTSND);
            break;
        case realhitlerobj:
            SD_PlaySound(EVASND);
            break;
#ifndef APOGEE_1_0
        case gretelobj:
            SD_PlaySound(MEINSND);
            break;
        case giftobj:
            SD_PlaySound(DONNERSND);
            break;
        case fatobj:
            SD_PlaySound(ROSESND);
            break;
#endif
#else
        case spectreobj:
            SD_PlaySound(GHOSTFADESND);
            break;
        case angelobj:
            SD_PlaySound(ANGELDEATHSND);
            break;
        case transobj:
            SD_PlaySound(TRANSDEATHSND);
            break;
        case uberobj:
            SD_PlaySound(UBERDEATHSND);
            break;
        case willobj:
            SD_PlaySound(WILHELMDEATHSND);
            break;
        case deathobj:
            SD_PlaySound(KNIGHTDEATHSND);
            break;
#endif
        case wbarrelobj:
            PlaySoundLocActor(WBARRELDEATHSND,ob);
            break;
        case tbarrelobj:
            PlaySoundLocActor(TBARRELDEATHSND,ob);
            break;
        case ebarrelobj:
            PlaySoundLocActor(EBARRELDEATHSND,ob);
            break;
        case vinesobj:
            PlaySoundLocActor(VINESDEATHSND,ob);
            break;
        case toxbarrelobj:
            PlaySoundLocActor(TOXBARRELDEATHSND,ob);
            break;
        case tankobj:
            PlaySoundLocActor(TANKDEATHSND,ob);
            break;
    }
}


/*
=============================================================================

                                SPEAR ACTORS

=============================================================================
*/

#ifdef SPEAR

void T_Launch (objtype *ob);
void T_Will (objtype *ob);

extern  statetype s_angelshoot1;
extern  statetype s_deathshoot1;
extern  statetype s_spark1;

//
// trans
//
extern  statetype s_transstand;

extern  statetype s_transchase1;
extern  statetype s_transchase1s;
extern  statetype s_transchase2;
extern  statetype s_transchase3;
extern  statetype s_transchase3s;
extern  statetype s_transchase4;

extern  statetype s_transdie0;
extern  statetype s_transdie01;
extern  statetype s_transdie1;
extern  statetype s_transdie2;
extern  statetype s_transdie3;
extern  statetype s_transdie4;

extern  statetype s_transshoot1;
extern  statetype s_transshoot2;
extern  statetype s_transshoot3;
extern  statetype s_transshoot4;
extern  statetype s_transshoot5;
extern  statetype s_transshoot6;
extern  statetype s_transshoot7;
extern  statetype s_transshoot8;


statetype s_transstand          = {false,SPR_TRANS_W1,0,(statefunc)T_Stand,NULL,&s_transstand};

statetype s_transchase1         = {false,SPR_TRANS_W1,10,(statefunc)T_Chase,NULL,&s_transchase1s};
statetype s_transchase1s        = {false,SPR_TRANS_W1,3,NULL,NULL,&s_transchase2};
statetype s_transchase2         = {false,SPR_TRANS_W2,8,(statefunc)T_Chase,NULL,&s_transchase3};
statetype s_transchase3         = {false,SPR_TRANS_W3,10,(statefunc)T_Chase,NULL,&s_transchase3s};
statetype s_transchase3s        = {false,SPR_TRANS_W3,3,NULL,NULL,&s_transchase4};
statetype s_transchase4         = {false,SPR_TRANS_W4,8,(statefunc)T_Chase,NULL,&s_transchase1};

statetype s_transdie0           = {false,SPR_TRANS_W1,1,NULL,(statefunc)A_DeathScream,&s_transdie01};
statetype s_transdie01          = {false,SPR_TRANS_W1,1,NULL,NULL,&s_transdie1};
statetype s_transdie1           = {false,SPR_TRANS_DIE1,15,NULL,NULL,&s_transdie2};
statetype s_transdie2           = {false,SPR_TRANS_DIE2,15,NULL,NULL,&s_transdie3};
statetype s_transdie3           = {false,SPR_TRANS_DIE3,15,NULL,NULL,&s_transdie4};
statetype s_transdie4           = {false,SPR_TRANS_DEAD,0,NULL,NULL,&s_transdie4};

statetype s_transshoot1         = {false,SPR_TRANS_SHOOT1,30,NULL,NULL,&s_transshoot2};
statetype s_transshoot2         = {false,SPR_TRANS_SHOOT2,10,NULL,(statefunc)T_Shoot,&s_transshoot3};
statetype s_transshoot3         = {false,SPR_TRANS_SHOOT3,10,NULL,(statefunc)T_Shoot,&s_transshoot4};
statetype s_transshoot4         = {false,SPR_TRANS_SHOOT2,10,NULL,(statefunc)T_Shoot,&s_transshoot5};
statetype s_transshoot5         = {false,SPR_TRANS_SHOOT3,10,NULL,(statefunc)T_Shoot,&s_transshoot6};
statetype s_transshoot6         = {false,SPR_TRANS_SHOOT2,10,NULL,(statefunc)T_Shoot,&s_transshoot7};
statetype s_transshoot7         = {false,SPR_TRANS_SHOOT3,10,NULL,(statefunc)T_Shoot,&s_transshoot8};
statetype s_transshoot8         = {false,SPR_TRANS_SHOOT1,10,NULL,NULL,&s_transchase1};


/*
===============
=
= SpawnTrans
=
===============
*/

void SpawnTrans (int tilex, int tiley)
{
    //        word *map;
    //        word tile;

    if (SoundBlasterPresent && DigiMode != sds_Off)
        s_transdie01.tictime = 105;

    SpawnNewObj (tilex,tiley,&s_transstand);
    newobj->obclass = transobj;
    newobj->hitpoints = starthitpoints[gamestate.difficulty][en_trans] *
        LWMP_BOSSHPFACTOR;
    newobj->flags |= FL_SHOOTABLE|FL_AMBUSH;
    if (!loadedgame)
        gamestate.killtotal++;
}


//
// uber
//
void T_UShoot (objtype *ob);

extern  statetype s_uberstand;

extern  statetype s_uberchase1;
extern  statetype s_uberchase1s;
extern  statetype s_uberchase2;
extern  statetype s_uberchase3;
extern  statetype s_uberchase3s;
extern  statetype s_uberchase4;

extern  statetype s_uberdie0;
extern  statetype s_uberdie01;
extern  statetype s_uberdie1;
extern  statetype s_uberdie2;
extern  statetype s_uberdie3;
extern  statetype s_uberdie4;
extern  statetype s_uberdie5;

extern  statetype s_ubershoot1;
extern  statetype s_ubershoot2;
extern  statetype s_ubershoot3;
extern  statetype s_ubershoot4;
extern  statetype s_ubershoot5;
extern  statetype s_ubershoot6;
extern  statetype s_ubershoot7;


statetype s_uberstand           = {false,SPR_UBER_W1,0,(statefunc)T_Stand,NULL,&s_uberstand};

statetype s_uberchase1          = {false,SPR_UBER_W1,10,(statefunc)T_Chase,NULL,&s_uberchase1s};
statetype s_uberchase1s         = {false,SPR_UBER_W1,3,NULL,NULL,&s_uberchase2};
statetype s_uberchase2          = {false,SPR_UBER_W2,8,(statefunc)T_Chase,NULL,&s_uberchase3};
statetype s_uberchase3          = {false,SPR_UBER_W3,10,(statefunc)T_Chase,NULL,&s_uberchase3s};
statetype s_uberchase3s         = {false,SPR_UBER_W3,3,NULL,NULL,&s_uberchase4};
statetype s_uberchase4          = {false,SPR_UBER_W4,8,(statefunc)T_Chase,NULL,&s_uberchase1};

statetype s_uberdie0            = {false,SPR_UBER_W1,1,NULL,(statefunc)A_DeathScream,&s_uberdie01};
statetype s_uberdie01           = {false,SPR_UBER_W1,1,NULL,NULL,&s_uberdie1};
statetype s_uberdie1            = {false,SPR_UBER_DIE1,15,NULL,NULL,&s_uberdie2};
statetype s_uberdie2            = {false,SPR_UBER_DIE2,15,NULL,NULL,&s_uberdie3};
statetype s_uberdie3            = {false,SPR_UBER_DIE3,15,NULL,NULL,&s_uberdie4};
statetype s_uberdie4            = {false,SPR_UBER_DIE4,15,NULL,NULL,&s_uberdie5};
statetype s_uberdie5            = {false,SPR_UBER_DEAD,0,NULL,NULL,&s_uberdie5};

statetype s_ubershoot1          = {false,SPR_UBER_SHOOT1,30,NULL,NULL,&s_ubershoot2};
statetype s_ubershoot2          = {false,SPR_UBER_SHOOT2,12,NULL,(statefunc)T_UShoot,&s_ubershoot3};
statetype s_ubershoot3          = {false,SPR_UBER_SHOOT3,12,NULL,(statefunc)T_UShoot,&s_ubershoot4};
statetype s_ubershoot4          = {false,SPR_UBER_SHOOT4,12,NULL,(statefunc)T_UShoot,&s_ubershoot5};
statetype s_ubershoot5          = {false,SPR_UBER_SHOOT3,12,NULL,(statefunc)T_UShoot,&s_ubershoot6};
statetype s_ubershoot6          = {false,SPR_UBER_SHOOT2,12,NULL,(statefunc)T_UShoot,&s_ubershoot7};
statetype s_ubershoot7          = {false,SPR_UBER_SHOOT1,12,NULL,NULL,&s_uberchase1};


/*
===============
=
= SpawnUber
=
===============
*/

void SpawnUber (int tilex, int tiley)
{
    if (SoundBlasterPresent && DigiMode != sds_Off)
        s_uberdie01.tictime = 70;

    SpawnNewObj (tilex,tiley,&s_uberstand);
    newobj->obclass = uberobj;
    newobj->hitpoints = starthitpoints[gamestate.difficulty][en_uber] *
        LWMP_BOSSHPFACTOR;
    newobj->flags |= FL_SHOOTABLE|FL_AMBUSH;
    if (!loadedgame)
        gamestate.killtotal++;
}


/*
===============
=
= T_UShoot
=
===============
*/

void T_UShoot (objtype *ob)
{
    int     dx,dy,dist;
    int c;

    T_Shoot (ob);

    for (LWMP_REPEAT(c))
    {
        dx = abs(ob->tilex - player_mp->tilex);
        dy = abs(ob->tiley - player_mp->tiley);
        dist = dx>dy ? dx : dy;
        if (dist <= 1)
            TakeDamage (10,ob);
    }
}


//
// will
//
extern  statetype s_willstand;

extern  statetype s_willchase1;
extern  statetype s_willchase1s;
extern  statetype s_willchase2;
extern  statetype s_willchase3;
extern  statetype s_willchase3s;
extern  statetype s_willchase4;

extern  statetype s_willdie1;
extern  statetype s_willdie2;
extern  statetype s_willdie3;
extern  statetype s_willdie4;
extern  statetype s_willdie5;
extern  statetype s_willdie6;

extern  statetype s_willshoot1;
extern  statetype s_willshoot2;
extern  statetype s_willshoot3;
extern  statetype s_willshoot4;
extern  statetype s_willshoot5;
extern  statetype s_willshoot6;


statetype s_willstand           = {false,SPR_WILL_W1,0,(statefunc)T_Stand,NULL,&s_willstand};

statetype s_willchase1          = {false,SPR_WILL_W1,10,(statefunc)T_Will,NULL,&s_willchase1s};
statetype s_willchase1s         = {false,SPR_WILL_W1,3,NULL,NULL,&s_willchase2};
statetype s_willchase2          = {false,SPR_WILL_W2,8,(statefunc)T_Will,NULL,&s_willchase3};
statetype s_willchase3          = {false,SPR_WILL_W3,10,(statefunc)T_Will,NULL,&s_willchase3s};
statetype s_willchase3s         = {false,SPR_WILL_W3,3,NULL,NULL,&s_willchase4};
statetype s_willchase4          = {false,SPR_WILL_W4,8,(statefunc)T_Will,NULL,&s_willchase1};

statetype s_willdeathcam        = {false,SPR_WILL_W1,1,NULL,NULL,&s_willdie1};

statetype s_willdie1            = {false,SPR_WILL_W1,1,NULL,(statefunc)A_DeathScream,&s_willdie2};
statetype s_willdie2            = {false,SPR_WILL_W1,10,NULL,NULL,&s_willdie3};
statetype s_willdie3            = {false,SPR_WILL_DIE1,10,NULL,NULL,&s_willdie4};
statetype s_willdie4            = {false,SPR_WILL_DIE2,10,NULL,NULL,&s_willdie5};
statetype s_willdie5            = {false,SPR_WILL_DIE3,10,NULL,NULL,&s_willdie6};
statetype s_willdie6            = {false,SPR_WILL_DEAD,20,NULL,NULL,&s_willdie6};

statetype s_willshoot1          = {false,SPR_WILL_SHOOT1,30,NULL,NULL,&s_willshoot2};
statetype s_willshoot2          = {false,SPR_WILL_SHOOT2,10,NULL,(statefunc)T_Launch,&s_willshoot3};
statetype s_willshoot3          = {false,SPR_WILL_SHOOT3,10,NULL,(statefunc)T_Shoot,&s_willshoot4};
statetype s_willshoot4          = {false,SPR_WILL_SHOOT4,10,NULL,(statefunc)T_Shoot,&s_willshoot5};
statetype s_willshoot5          = {false,SPR_WILL_SHOOT3,10,NULL,(statefunc)T_Shoot,&s_willshoot6};
statetype s_willshoot6          = {false,SPR_WILL_SHOOT4,10,NULL,(statefunc)T_Shoot,&s_willchase1};


/*
===============
=
= SpawnWill
=
===============
*/

void SpawnWill (int tilex, int tiley)
{
    if (SoundBlasterPresent && DigiMode != sds_Off)
        s_willdie2.tictime = 70;

    SpawnNewObj (tilex,tiley,&s_willstand);
    newobj->obclass = willobj;
    newobj->hitpoints = starthitpoints[gamestate.difficulty][en_will] *
        LWMP_BOSSHPFACTOR;
    newobj->flags |= FL_SHOOTABLE|FL_AMBUSH;
    if (!loadedgame)
        gamestate.killtotal++;
}


/*
================
=
= T_Will
=
================
*/

void T_Will (objtype *ob)
{
    int32_t move;
    int     dx,dy,dist;
    boolean dodge;

    ob->shooting = false;
    T_FindNewTarget(ob);

    dodge = false;
    dx = abs(ob->tilex - player_mp->tilex);
    dy = abs(ob->tiley - player_mp->tiley);
    dist = dx>dy ? dx : dy;

    if (CheckLine(ob))                                              // got a shot at player?
    {
        ob->hidden = false;
        if ( (unsigned) US_RndT() < (tics<<3) && objfreelist)
        {
            ob->shooting = true;
            //
            // go into attack frame
            //
            if (ob->obclass == willobj)
                NewState (ob,&s_willshoot1);
            else if (ob->obclass == angelobj)
                NewState (ob,&s_angelshoot1);
            else
                NewState (ob,&s_deathshoot1);
            return;
        }
        dodge = true;
    }
    else
        ob->hidden = true;

    if (ob->dir == nodir)
    {
        if (dodge)
            SelectDodgeDir (ob);
        else
            SelectChaseDir (ob);
        if (ob->dir == nodir)
            return;                                                 // object is blocked in
    }

    move = ob->speed*tics;

    while (move)
    {
        if (ob->distance < 0)
        {
            //
            // waiting for a door to open
            //
            OpenDoor (-ob->distance-1);
            if (doorobjlist[-ob->distance-1].action != dr_open)
                return;
            ob->distance = TILEGLOBAL;      // go ahead, the door is now open
            TryWalk(ob);
        }

        if (move < ob->distance)
        {
            MoveObj (ob,move);
            break;
        }

        //
        // reached goal tile, so select another one
        //

        //
        // fix position to account for round off during moving
        //
        ob->x = ((int32_t)ob->tilex<<TILESHIFT)+TILEGLOBAL/2;
        ob->y = ((int32_t)ob->tiley<<TILESHIFT)+TILEGLOBAL/2;

        move -= ob->distance;

        if (dist <4)
            SelectRunDir (ob);
        else if (dodge)
            SelectDodgeDir (ob);
        else
            SelectChaseDir (ob);

        if (ob->dir == nodir)
            return;                                                 // object is blocked in
    }

}


//
// death
//
extern  statetype s_deathstand;

extern  statetype s_deathchase1;
extern  statetype s_deathchase1s;
extern  statetype s_deathchase2;
extern  statetype s_deathchase3;
extern  statetype s_deathchase3s;
extern  statetype s_deathchase4;

extern  statetype s_deathdie1;
extern  statetype s_deathdie2;
extern  statetype s_deathdie3;
extern  statetype s_deathdie4;
extern  statetype s_deathdie5;
extern  statetype s_deathdie6;
extern  statetype s_deathdie7;
extern  statetype s_deathdie8;
extern  statetype s_deathdie9;

extern  statetype s_deathshoot1;
extern  statetype s_deathshoot2;
extern  statetype s_deathshoot3;
extern  statetype s_deathshoot4;
extern  statetype s_deathshoot5;


statetype s_deathstand          = {false,SPR_DEATH_W1,0,(statefunc)T_Stand,NULL,&s_deathstand};

statetype s_deathchase1         = {false,SPR_DEATH_W1,10,(statefunc)T_Will,NULL,&s_deathchase1s};
statetype s_deathchase1s        = {false,SPR_DEATH_W1,3,NULL,NULL,&s_deathchase2};
statetype s_deathchase2         = {false,SPR_DEATH_W2,8,(statefunc)T_Will,NULL,&s_deathchase3};
statetype s_deathchase3         = {false,SPR_DEATH_W3,10,(statefunc)T_Will,NULL,&s_deathchase3s};
statetype s_deathchase3s        = {false,SPR_DEATH_W3,3,NULL,NULL,&s_deathchase4};
statetype s_deathchase4         = {false,SPR_DEATH_W4,8,(statefunc)T_Will,NULL,&s_deathchase1};

statetype s_deathdeathcam       = {false,SPR_DEATH_W1,1,NULL,NULL,&s_deathdie1};

statetype s_deathdie1           = {false,SPR_DEATH_W1,1,NULL,(statefunc)A_DeathScream,&s_deathdie2};
statetype s_deathdie2           = {false,SPR_DEATH_W1,10,NULL,NULL,&s_deathdie3};
statetype s_deathdie3           = {false,SPR_DEATH_DIE1,10,NULL,NULL,&s_deathdie4};
statetype s_deathdie4           = {false,SPR_DEATH_DIE2,10,NULL,NULL,&s_deathdie5};
statetype s_deathdie5           = {false,SPR_DEATH_DIE3,10,NULL,NULL,&s_deathdie6};
statetype s_deathdie6           = {false,SPR_DEATH_DIE4,10,NULL,NULL,&s_deathdie7};
statetype s_deathdie7           = {false,SPR_DEATH_DIE5,10,NULL,NULL,&s_deathdie8};
statetype s_deathdie8           = {false,SPR_DEATH_DIE6,10,NULL,NULL,&s_deathdie9};
statetype s_deathdie9           = {false,SPR_DEATH_DEAD,0,NULL,NULL,&s_deathdie9};

statetype s_deathshoot1         = {false,SPR_DEATH_SHOOT1,30,NULL,NULL,&s_deathshoot2};
statetype s_deathshoot2         = {false,SPR_DEATH_SHOOT2,10,NULL,(statefunc)T_Launch,&s_deathshoot3};
statetype s_deathshoot3         = {false,SPR_DEATH_SHOOT4,10,NULL,(statefunc)T_Shoot,&s_deathshoot4};
statetype s_deathshoot4         = {false,SPR_DEATH_SHOOT3,10,NULL,(statefunc)T_Launch,&s_deathshoot5};
statetype s_deathshoot5         = {false,SPR_DEATH_SHOOT4,10,NULL,(statefunc)T_Shoot,&s_deathchase1};


/*
===============
=
= SpawnDeath
=
===============
*/

void SpawnDeath (int tilex, int tiley)
{
    if (SoundBlasterPresent && DigiMode != sds_Off)
        s_deathdie2.tictime = 105;

    SpawnNewObj (tilex,tiley,&s_deathstand);
    newobj->obclass = deathobj;
    newobj->hitpoints = starthitpoints[gamestate.difficulty][en_death] *
        LWMP_BOSSHPFACTOR;
    newobj->flags |= FL_SHOOTABLE|FL_AMBUSH;
    if (!loadedgame)
        gamestate.killtotal++;
}

/*
===============
=
= T_Launch
=
===============
*/

void T_Launch (objtype *ob)
{
    int32_t deltax,deltay;
    float   angle;
    int     iangle_mp;

    deltax = player_mp->x - ob->x;
    deltay = ob->y - player_mp->y;
    angle = (float) atan2 ((float) deltay, (float) deltax);
    if (angle<0)
        angle = (float) (M_PI*2+angle);
    iangle_mp = (int) (angle/(M_PI*2)*ANGLES);
    if (ob->obclass == deathobj)
    {
        T_Shoot (ob);
        if (ob->state == &s_deathshoot2)
        {
            iangle_mp-=4;
            if (iangle_mp<0)
                iangle_mp+=ANGLES;
        }
        else
        {
            iangle_mp+=4;
            if (iangle_mp>=ANGLES)
                iangle_mp-=ANGLES;
        }
    }

    GetNewActor ();
    newobj->state = &s_rocket;
    newobj->ticcount = 1;

    newobj->tilex = ob->tilex;
    newobj->tiley = ob->tiley;
    newobj->x = ob->x;
    newobj->y = ob->y;
    newobj->obclass = rocketobj;
    switch(ob->obclass)
    {
        case deathobj:
            newobj->state = &s_hrocket;
            newobj->obclass = hrocketobj;
            PlaySoundLocActor (KNIGHTMISSILESND,newobj);
            break;
        case angelobj:
            newobj->state = &s_spark1;
            newobj->obclass = sparkobj;
            PlaySoundLocActor (ANGELFIRESND,newobj);
            break;
        default:
            PlaySoundLocActor (MISSILEFIRESND,newobj);
    }

    newobj->dir = nodir;
    newobj->angle = iangle_mp;
    newobj->speed = 0x2000l;
    newobj->flags = FL_NEVERMARK;
    newobj->active = ac_yes;
}



//
// angel
//
void A_Relaunch (objtype *ob);
void A_Victory (objtype *ob);
void A_StartAttack (objtype *ob);
void A_Breathing (objtype *ob);

extern  statetype s_angelstand;

extern  statetype s_angelchase1;
extern  statetype s_angelchase1s;
extern  statetype s_angelchase2;
extern  statetype s_angelchase3;
extern  statetype s_angelchase3s;
extern  statetype s_angelchase4;

extern  statetype s_angeldie1;
extern  statetype s_angeldie11;
extern  statetype s_angeldie2;
extern  statetype s_angeldie3;
extern  statetype s_angeldie4;
extern  statetype s_angeldie5;
extern  statetype s_angeldie6;
extern  statetype s_angeldie7;
extern  statetype s_angeldie8;
extern  statetype s_angeldie9;

extern  statetype s_angelshoot1;
extern  statetype s_angelshoot2;
extern  statetype s_angelshoot3;
extern  statetype s_angelshoot4;
extern  statetype s_angelshoot5;
extern  statetype s_angelshoot6;

extern  statetype s_angeltired;
extern  statetype s_angeltired2;
extern  statetype s_angeltired3;
extern  statetype s_angeltired4;
extern  statetype s_angeltired5;
extern  statetype s_angeltired6;
extern  statetype s_angeltired7;

extern  statetype s_spark1;
extern  statetype s_spark2;
extern  statetype s_spark3;
extern  statetype s_spark4;


statetype s_angelstand          = {false,SPR_ANGEL_W1,0,(statefunc)T_Stand,NULL,&s_angelstand};

statetype s_angelchase1         = {false,SPR_ANGEL_W1,10,(statefunc)T_Will,NULL,&s_angelchase1s};
statetype s_angelchase1s        = {false,SPR_ANGEL_W1,3,NULL,NULL,&s_angelchase2};
statetype s_angelchase2         = {false,SPR_ANGEL_W2,8,(statefunc)T_Will,NULL,&s_angelchase3};
statetype s_angelchase3         = {false,SPR_ANGEL_W3,10,(statefunc)T_Will,NULL,&s_angelchase3s};
statetype s_angelchase3s        = {false,SPR_ANGEL_W3,3,NULL,NULL,&s_angelchase4};
statetype s_angelchase4         = {false,SPR_ANGEL_W4,8,(statefunc)T_Will,NULL,&s_angelchase1};

statetype s_angeldie1           = {false,SPR_ANGEL_W1,1,NULL,(statefunc)A_DeathScream,&s_angeldie11};
statetype s_angeldie11          = {false,SPR_ANGEL_W1,1,NULL,NULL,&s_angeldie2};
statetype s_angeldie2           = {false,SPR_ANGEL_DIE1,10,NULL,(statefunc)A_Slurpie,&s_angeldie3};
statetype s_angeldie3           = {false,SPR_ANGEL_DIE2,10,NULL,NULL,&s_angeldie4};
statetype s_angeldie4           = {false,SPR_ANGEL_DIE3,10,NULL,NULL,&s_angeldie5};
statetype s_angeldie5           = {false,SPR_ANGEL_DIE4,10,NULL,NULL,&s_angeldie6};
statetype s_angeldie6           = {false,SPR_ANGEL_DIE5,10,NULL,NULL,&s_angeldie7};
statetype s_angeldie7           = {false,SPR_ANGEL_DIE6,10,NULL,NULL,&s_angeldie8};
statetype s_angeldie8           = {false,SPR_ANGEL_DIE7,10,NULL,NULL,&s_angeldie9};
statetype s_angeldie9           = {false,SPR_ANGEL_DEAD,130,NULL,(statefunc)A_Victory,&s_angeldie9};

statetype s_angelshoot1         = {false,SPR_ANGEL_SHOOT1,10,NULL,(statefunc)A_StartAttack,&s_angelshoot2};
statetype s_angelshoot2         = {false,SPR_ANGEL_SHOOT2,20,NULL,(statefunc)T_Launch,&s_angelshoot3};
statetype s_angelshoot3         = {false,SPR_ANGEL_SHOOT1,10,NULL,(statefunc)A_Relaunch,&s_angelshoot2};

statetype s_angeltired          = {false,SPR_ANGEL_TIRED1,40,NULL,(statefunc)A_Breathing,&s_angeltired2};
statetype s_angeltired2         = {false,SPR_ANGEL_TIRED2,40,NULL,NULL,&s_angeltired3};
statetype s_angeltired3         = {false,SPR_ANGEL_TIRED1,40,NULL,(statefunc)A_Breathing,&s_angeltired4};
statetype s_angeltired4         = {false,SPR_ANGEL_TIRED2,40,NULL,NULL,&s_angeltired5};
statetype s_angeltired5         = {false,SPR_ANGEL_TIRED1,40,NULL,(statefunc)A_Breathing,&s_angeltired6};
statetype s_angeltired6         = {false,SPR_ANGEL_TIRED2,40,NULL,NULL,&s_angeltired7};
statetype s_angeltired7         = {false,SPR_ANGEL_TIRED1,40,NULL,(statefunc)A_Breathing,&s_angelchase1};

statetype s_spark1              = {false,SPR_SPARK1,6,(statefunc)T_Projectile,NULL,&s_spark2};
statetype s_spark2              = {false,SPR_SPARK2,6,(statefunc)T_Projectile,NULL,&s_spark3};
statetype s_spark3              = {false,SPR_SPARK3,6,(statefunc)T_Projectile,NULL,&s_spark4};
statetype s_spark4              = {false,SPR_SPARK4,6,(statefunc)T_Projectile,NULL,&s_spark1};


void A_Slurpie (objtype *)
{
    SD_PlaySound(SLURPIESND);
}

void A_Breathing (objtype *)
{
    SD_PlaySound(ANGELTIREDSND);
}

/*
===============
=
= SpawnAngel
=
===============
*/

void SpawnAngel (int tilex, int tiley)
{
    if (SoundBlasterPresent && DigiMode != sds_Off)
        s_angeldie11.tictime = 105;

    SpawnNewObj (tilex,tiley,&s_angelstand);
    newobj->obclass = angelobj;
    newobj->hitpoints = starthitpoints[gamestate.difficulty][en_angel] *
        LWMP_BOSSHPFACTOR;
    newobj->flags |= FL_SHOOTABLE|FL_AMBUSH;
    if (!loadedgame)
        gamestate.killtotal++;
}


/*
=================
=
= A_Victory
=
=================
*/

void A_Victory (objtype *)
{
    SetPlayState(ex_victorious);
}


/*
=================
=
= A_StartAttack
=
=================
*/

void A_StartAttack (objtype *ob)
{
    ob->temp1 = 0;
}


/*
=================
=
= A_Relaunch
=
=================
*/

void A_Relaunch (objtype *ob)
{
    if (++ob->temp1 == 3)
    {
        NewState (ob,&s_angeltired);
        return;
    }

    if (US_RndT()&1)
    {
        NewState (ob,&s_angelchase1);
        return;
    }
}




//
// spectre
//
void T_SpectreWait (objtype *ob);
void A_Dormant (objtype *ob);

extern  statetype s_spectrewait1;
extern  statetype s_spectrewait2;
extern  statetype s_spectrewait3;
extern  statetype s_spectrewait4;

extern  statetype s_spectrechase1;
extern  statetype s_spectrechase2;
extern  statetype s_spectrechase3;
extern  statetype s_spectrechase4;

extern  statetype s_spectredie1;
extern  statetype s_spectredie2;
extern  statetype s_spectredie3;
extern  statetype s_spectredie4;

extern  statetype s_spectrewake;

statetype s_spectrewait1        = {false,SPR_SPECTRE_W1,10,(statefunc)T_Stand,NULL,&s_spectrewait2};
statetype s_spectrewait2        = {false,SPR_SPECTRE_W2,10,(statefunc)T_Stand,NULL,&s_spectrewait3};
statetype s_spectrewait3        = {false,SPR_SPECTRE_W3,10,(statefunc)T_Stand,NULL,&s_spectrewait4};
statetype s_spectrewait4        = {false,SPR_SPECTRE_W4,10,(statefunc)T_Stand,NULL,&s_spectrewait1};

statetype s_spectrechase1       = {false,SPR_SPECTRE_W1,10,(statefunc)T_Ghosts,NULL,&s_spectrechase2};
statetype s_spectrechase2       = {false,SPR_SPECTRE_W2,10,(statefunc)T_Ghosts,NULL,&s_spectrechase3};
statetype s_spectrechase3       = {false,SPR_SPECTRE_W3,10,(statefunc)T_Ghosts,NULL,&s_spectrechase4};
statetype s_spectrechase4       = {false,SPR_SPECTRE_W4,10,(statefunc)T_Ghosts,NULL,&s_spectrechase1};

statetype s_spectredie1         = {false,SPR_SPECTRE_F1,10,NULL,NULL,&s_spectredie2};
statetype s_spectredie2         = {false,SPR_SPECTRE_F2,10,NULL,NULL,&s_spectredie3};
statetype s_spectredie3         = {false,SPR_SPECTRE_F3,10,NULL,NULL,&s_spectredie4};
statetype s_spectredie4         = {false,SPR_SPECTRE_F4,300,NULL,NULL,&s_spectrewake};
statetype s_spectrewake         = {false,SPR_SPECTRE_F4,10,NULL,(statefunc)A_Dormant,&s_spectrewake};

/*
===============
=
= SpawnSpectre
=
===============
*/

void SpawnSpectre (int tilex, int tiley)
{
    SpawnNewObj (tilex,tiley,&s_spectrewait1);
    newobj->obclass = spectreobj;
    newobj->hitpoints = starthitpoints[gamestate.difficulty][en_spectre] *
        LWMP_BOSSHPFACTOR;
    newobj->flags |= FL_SHOOTABLE|FL_AMBUSH|FL_BONUS; // |FL_NEVERMARK|FL_NONMARK;
    if (!loadedgame)
        gamestate.killtotal++;
}


/*
===============
=
= A_Dormant
=
===============
*/

void A_Dormant (objtype *ob)
{
    int32_t     deltax,deltay;
    int         xl,xh,yl,yh;
    int         x,y;
    uintptr_t   tile;

    deltax = ob->x - player_mp->x;
    if (deltax < -MINACTORDIST || deltax > MINACTORDIST)
        goto moveok;
    deltay = ob->y - player_mp->y;
    if (deltay < -MINACTORDIST || deltay > MINACTORDIST)
        goto moveok;

    return;
moveok:

    xl = (ob->x-MINDIST) >> TILESHIFT;
    xh = (ob->x+MINDIST) >> TILESHIFT;
    yl = (ob->y-MINDIST) >> TILESHIFT;
    yh = (ob->y+MINDIST) >> TILESHIFT;

    for (y=yl ; y<=yh ; y++)
        for (x=xl ; x<=xh ; x++)
        {
            tile = (uintptr_t)actorat[x][y];
            if (!tile)
                continue;
            if (!ISPOINTER(tile))
                return;
            if (((objtype *)tile)->flags&FL_SHOOTABLE)
                return;
        }

        ob->flags |= FL_AMBUSH | FL_SHOOTABLE;
        ob->flags &= ~FL_ATTACKMODE;
        ob->flags &= ~FL_NONMARK;      // stuck bugfix 1
        ob->dir = nodir;
        NewState (ob,&s_spectrewait1);
}


#endif


//
// wbarrel
//

extern statetype s_wbarrelstand;
extern statetype s_wbarreldie1;
extern statetype s_wbarreldie2;
extern statetype s_wbarreldie3;
extern statetype s_wbarreldie4;
extern statetype s_wbarreldead;

statetype s_wbarrelstand = {false,SPR_WBARREL_STAND,0,(statefunc)T_Wbarrel,NULL,&s_wbarrelstand};
statetype s_wbarreldie1 = {false,SPR_WBARREL_DIE1,10,NULL,(statefunc)A_DeathScream,&s_wbarreldie2};
statetype s_wbarreldie2 = {false,SPR_WBARREL_DIE2,10,NULL,NULL,&s_wbarreldie3};
statetype s_wbarreldie3 = {false,SPR_WBARREL_DIE3,10,NULL,NULL,&s_wbarreldie4};
statetype s_wbarreldie4 = {false,SPR_WBARREL_DIE4,10,NULL,NULL,&s_wbarreldead};
statetype s_wbarreldead = {false,SPR_WBARREL_DEAD,0,NULL,NULL,&s_wbarreldead};

/*
===============
=
= SpawnWbarrel
=
===============
*/

void SpawnWbarrel (int tilex, int tiley)
{
    SpawnNewObj (tilex,tiley,&s_wbarrelstand);
    newobj->obclass = wbarrelobj;
    newobj->hitpoints = starthitpoints[gamestate.difficulty][en_wbarrel];
    newobj->flags |= FL_SHOOTABLE;
    newobj->actorDist = MINACTORDISTBAR;
}

//
// tbarrel
//

extern statetype s_tbarrelstand;
extern statetype s_tbarreldie1;
extern statetype s_tbarreldie2;
extern statetype s_tbarreldie3;
extern statetype s_tbarreldead;

statetype s_tbarrelstand = {false,SPR_TBARREL_STAND,0,(statefunc)T_Tbarrel,NULL,&s_tbarrelstand};
statetype s_tbarreldie1 = {false,SPR_TBARREL_DIE1,10,NULL,(statefunc)A_DeathScream,&s_tbarreldie2};
statetype s_tbarreldie2 = {false,SPR_TBARREL_DIE2,10,NULL,NULL,&s_tbarreldie3};
statetype s_tbarreldie3 = {false,SPR_TBARREL_DIE3,10,NULL,(statefunc)A_EbarrelSplashDamage,&s_tbarreldead};
statetype s_tbarreldead = {false,SPR_TBARREL_DEAD,0,NULL,NULL,&s_tbarreldead};


/*
===============
=
= SpawnTbarrel
=
===============
*/

void SpawnTbarrel (int tilex, int tiley)
{
    SpawnNewObj (tilex,tiley,&s_tbarrelstand);
    newobj->obclass = tbarrelobj;
    newobj->hitpoints = starthitpoints[gamestate.difficulty][en_tbarrel];
    newobj->flags |= FL_SHOOTABLE;
    newobj->actorDist = MINACTORDISTBAR;
}

//
// ebarrel
//

extern statetype s_ebarrelstand;
extern statetype s_ebarreldie1;
extern statetype s_ebarreldie2;
extern statetype s_ebarreldie3;
extern statetype s_ebarreldead;

statetype s_ebarrelstand = {false,SPR_EBARREL_STAND,0,NULL,NULL,&s_ebarrelstand};
statetype s_ebarreldie1 = {false,SPR_EBARREL_DIE1,10,NULL,(statefunc)A_DeathScream,&s_ebarreldie2};
statetype s_ebarreldie2 = {false,SPR_EBARREL_DIE2,10,NULL,NULL,&s_ebarreldie3};
statetype s_ebarreldie3 = {false,SPR_EBARREL_DIE3,10,NULL,(statefunc)A_EbarrelSplashDamage,&s_ebarreldead};
statetype s_ebarreldead = {false,SPR_EBARREL_DEAD,0,NULL,NULL,&s_ebarreldead};

extern statetype s_dmebarreldie1;
extern statetype s_dmebarreldie2;
extern statetype s_dmebarreldie3;
extern statetype s_dmebarreldead;
extern statetype s_dmebarrelretry;
extern statetype s_dmebarrelrespawn1;
extern statetype s_dmebarrelrespawn2;
extern statetype s_dmebarrelrespawn3;
extern statetype s_dmebarrelrespawn4;
extern statetype s_dmebarrelrespawn5;

statetype s_dmebarreldie1 = {false,SPR_EBARREL_DIE1,10,NULL,(statefunc)A_DeathScream,&s_dmebarreldie2};
statetype s_dmebarreldie2 = {false,SPR_EBARREL_DIE2,10,NULL,NULL,&s_dmebarreldie3};
statetype s_dmebarreldie3 = {false,SPR_EBARREL_DIE3,10,NULL,(statefunc)A_EbarrelSplashDamage,&s_dmebarreldead};
statetype s_dmebarreldead = {false,SPR_EBARREL_DEAD,540,NULL,(statefunc)A_EbarrelRespawn,&s_dmebarrelrespawn1};
statetype s_dmebarrelretry = {false,SPR_EBARREL_DEAD,1,NULL,NULL,&s_dmebarreldead};
statetype s_dmebarrelrespawn1 = {false,SPR_RESPAWN_1,4,NULL,NULL,&s_dmebarrelrespawn2};
statetype s_dmebarrelrespawn2 = {false,SPR_RESPAWN_2,4,NULL,NULL,&s_dmebarrelrespawn3};
statetype s_dmebarrelrespawn3 = {false,SPR_RESPAWN_3,4,NULL,NULL,&s_dmebarrelrespawn4};
statetype s_dmebarrelrespawn4 = {false,SPR_RESPAWN_4,4,NULL,NULL,&s_dmebarrelrespawn5};
statetype s_dmebarrelrespawn5 = {false,SPR_RESPAWN_5,4,NULL,NULL,&s_ebarrelstand};


/*
===============
=
= SpawnEbarrel
=
===============
*/

void SpawnEbarrel (int tilex, int tiley)
{
    SpawnNewObj (tilex,tiley,&s_ebarrelstand);
    newobj->obclass = ebarrelobj;
    newobj->hitpoints = starthitpoints[gamestate.difficulty][en_ebarrel];
    newobj->flags |= FL_SHOOTABLE | FL_PROJHITS;
    newobj->actorDist = MINACTORDISTBAR;
}


/*
===============
=
= A_EbarrelSplashDamage
=
===============
*/

void A_EbarrelSplashDamage (objtype *ob)
{
    SplashDamage(ob, 200, FP(2), true);
}


/*
===============
=
= A_EbarrelRespawn
=
===============
*/

void A_EbarrelRespawn (objtype *ob)
{
    int         c;
    int32_t     deltax,deltay;
    bool        too_close = false;

    // don't spawn barrel over any active players
    for (LWMP_REPEAT(c))
    {
        if ((player_mp->flags & FL_SHOOTABLE) == 0)
        {
            continue;
        }

        deltax = ob->x - player_mp->x;
        if (deltax < -MINPLAYERDIST || deltax > MINPLAYERDIST)
        {
            continue;
        }

        deltay = ob->y - player_mp->y;
        if (deltay < -MINPLAYERDIST || deltay > MINPLAYERDIST)
        {
            continue;
        }

        too_close = true;
    }

    if (too_close)
    {
        // try again later
        NewState(ob, &s_dmebarrelretry);
        return;
    }

    ob->flags |= FL_SHOOTABLE;
    ob->hitpoints = starthitpoints[gamestate.difficulty][en_ebarrel];
    ob->flags &= ~FL_NONMARK;
}


/*
===============
=
= T_Wbarrel
=
===============
*/

void T_Wbarrel(objtype *ob)
{
    const unsigned maxhp = 100;
    const unsigned hpinc = tics;
    ob->hitpoints = std::min(ob->hitpoints + hpinc, maxhp);
}


/*
===============
=
= T_Tbarrel
=
===============
*/

void T_Tbarrel(objtype *ob)
{
    const unsigned maxhp = 100;
    const unsigned hpinc = tics * 2;
    ob->hitpoints = std::min(ob->hitpoints + hpinc, maxhp);
}


//
// toxbarrel
//

extern statetype s_toxbarrelstand;
extern statetype s_toxbarrelstand2;
extern statetype s_toxbarrelstand3;

statetype s_toxbarrelstand = {false,SPR_PBARREL_STAND1,10,NULL,NULL,&s_toxbarrelstand2};
statetype s_toxbarrelstand2 = {false,SPR_PBARREL_STAND2,10,NULL,NULL,&s_toxbarrelstand3};
statetype s_toxbarrelstand3 = {false,SPR_PBARREL_STAND3,10,NULL,NULL,&s_toxbarrelstand};

extern statetype s_dmtoxbarreldie1;
extern statetype s_dmtoxbarreldie2;
extern statetype s_dmtoxbarreldie3;
extern statetype s_dmtoxbarreldie4;
extern statetype s_dmtoxbarreldie5;
extern statetype s_dmtoxbarreldead;
extern statetype s_dmtoxbarreldead2;
extern statetype s_dmtoxbarreldead3;
extern statetype s_dmtoxbarrelretry;
extern statetype s_dmtoxbarrelrespawn1;
extern statetype s_dmtoxbarrelrespawn2;
extern statetype s_dmtoxbarrelrespawn3;
extern statetype s_dmtoxbarrelrespawn4;
extern statetype s_dmtoxbarrelrespawn5;

statetype s_dmtoxbarreldie1 = {false,SPR_PBARREL_DIE1,10,NULL,(statefunc)A_DeathScream,&s_dmtoxbarreldie2};
statetype s_dmtoxbarreldie2 = {false,SPR_PBARREL_DIE2,10,NULL,NULL,&s_dmtoxbarreldie3};
statetype s_dmtoxbarreldie3 = {false,SPR_PBARREL_DIE3,10,NULL,NULL,&s_dmtoxbarreldie4};
statetype s_dmtoxbarreldie4 = {false,SPR_PBARREL_DIE4,10,NULL,NULL,&s_dmtoxbarreldie5};
statetype s_dmtoxbarreldie5 = {false,SPR_PBARREL_DIE5,10,NULL,NULL,&s_dmtoxbarreldead};
statetype s_dmtoxbarreldead = {false,SPR_PBARREL_DEAD1,15,NULL,(statefunc)A_ToxbarrelDead,&s_dmtoxbarreldead2};
statetype s_dmtoxbarreldead2 = {false,SPR_PBARREL_DEAD2,15,NULL,(statefunc)A_ToxbarrelDead,&s_dmtoxbarreldead3};
statetype s_dmtoxbarreldead3 = {false,SPR_PBARREL_DEAD3,15,NULL,(statefunc)A_ToxbarrelDead,&s_dmtoxbarreldead};
statetype s_dmtoxbarrelretry = {false,SPR_PBARREL_DEAD1,1,NULL,NULL,&s_dmtoxbarreldead};
statetype s_dmtoxbarrelrespawn1 = {false,SPR_RESPAWN_1,4,NULL,NULL,&s_dmtoxbarrelrespawn2};
statetype s_dmtoxbarrelrespawn2 = {false,SPR_RESPAWN_2,4,NULL,NULL,&s_dmtoxbarrelrespawn3};
statetype s_dmtoxbarrelrespawn3 = {false,SPR_RESPAWN_3,4,NULL,NULL,&s_dmtoxbarrelrespawn4};
statetype s_dmtoxbarrelrespawn4 = {false,SPR_RESPAWN_4,4,NULL,NULL,&s_dmtoxbarrelrespawn5};
statetype s_dmtoxbarrelrespawn5 = {false,SPR_RESPAWN_5,4,NULL,NULL,&s_toxbarrelstand};


/*
===============
=
= SpawnToxbarrel
=
===============
*/

void SpawnToxbarrel (int tilex, int tiley)
{
    SpawnNewObj (tilex,tiley,&s_toxbarrelstand);
    newobj->obclass = toxbarrelobj;
    newobj->hitpoints = starthitpoints[gamestate.difficulty][en_toxbarrel];
    newobj->flags |= FL_SHOOTABLE | FL_PROJHITS;
    newobj->actorDist = MINACTORDISTBAR;
}


/*
===============
=
= A_ToxbarrelSplashDamage
=
===============
*/

void A_ToxbarrelSplashDamage (objtype *ob)
{
    SplashDamage(ob, 20 + (US_RndT() % 10), FP(1) - 0x100l, true);
}


/*
===============
=
= A_ToxbarrelRespawn
=
===============
*/

void A_ToxbarrelRespawn (objtype *ob)
{
    int         c;
    int32_t     deltax,deltay;
    bool        too_close = false;

    // don't spawn barrel over any active players
    for (LWMP_REPEAT(c))
    {
        if ((player_mp->flags & FL_SHOOTABLE) == 0)
        {
            continue;
        }

        deltax = ob->x - player_mp->x;
        if (deltax < -MINPLAYERDIST || deltax > MINPLAYERDIST)
        {
            continue;
        }

        deltay = ob->y - player_mp->y;
        if (deltay < -MINPLAYERDIST || deltay > MINPLAYERDIST)
        {
            continue;
        }

        too_close = true;
    }

    if (too_close)
    {
        // try again later
        NewState(ob, &s_dmtoxbarrelretry);
        return;
    }

    ob->flags |= FL_SHOOTABLE;
    ob->hitpoints = starthitpoints[gamestate.difficulty][en_toxbarrel];
    ob->flags &= ~FL_NONMARK;

    NewState(ob, &s_dmtoxbarrelrespawn1);
}


/*
===============
=
= A_ToxbarrelDead
=
===============
*/

void A_ToxbarrelDead (objtype *ob)
{
    ob->temp1++;

    if((ob->temp1 % 3) == 0)
    {
        A_ToxbarrelSplashDamage(ob);
    }

    if(ob->temp1 >= 540 / 15)
    {
        ob->temp1 = 0;
        A_ToxbarrelRespawn(ob);
    }
}


/*
=============================================================================

                            SCHABBS / GIFT / FAT

=============================================================================
*/

#ifndef SPEAR
/*
===============
=
= SpawnGhosts
=
===============
*/

void SpawnGhosts (int which, int tilex, int tiley)
{
    switch(which)
    {
        case en_blinky:
            SpawnNewObj (tilex,tiley,&s_blinkychase1);
            break;
        case en_clyde:
            SpawnNewObj (tilex,tiley,&s_clydechase1);
            break;
        case en_pinky:
            SpawnNewObj (tilex,tiley,&s_pinkychase1);
            break;
        case en_inky:
            SpawnNewObj (tilex,tiley,&s_inkychase1);
            break;
    }

    newobj->obclass = ghostobj;
    newobj->speed = SPDDOG;

    newobj->dir = east;
    newobj->flags |= FL_AMBUSH;
    if (!loadedgame)
    {
        gamestate.killtotal++;
        gamestate.killcount++;
    }
}



void    T_Gift (objtype *ob);
void    T_GiftThrow (objtype *ob);

void    T_Fat (objtype *ob);
void    T_FatThrow (objtype *ob);

//
// schabb
//
extern  statetype s_schabbstand;

extern  statetype s_schabbchase1;
extern  statetype s_schabbchase1s;
extern  statetype s_schabbchase2;
extern  statetype s_schabbchase3;
extern  statetype s_schabbchase3s;
extern  statetype s_schabbchase4;

extern  statetype s_schabbdie1;
extern  statetype s_schabbdie2;
extern  statetype s_schabbdie3;
extern  statetype s_schabbdie4;
extern  statetype s_schabbdie5;
extern  statetype s_schabbdie6;

extern  statetype s_schabbshoot1;
extern  statetype s_schabbshoot2;

extern  statetype s_needle1;
extern  statetype s_needle2;
extern  statetype s_needle3;
extern  statetype s_needle4;

extern  statetype s_schabbdeathcam;


statetype s_schabbstand         = {false,SPR_SCHABB_W1,0,(statefunc)T_Stand,NULL,&s_schabbstand};

statetype s_schabbchase1        = {false,SPR_SCHABB_W1,10,(statefunc)T_Schabb,NULL,&s_schabbchase1s};
statetype s_schabbchase1s       = {false,SPR_SCHABB_W1,3,NULL,NULL,&s_schabbchase2};
statetype s_schabbchase2        = {false,SPR_SCHABB_W2,8,(statefunc)T_Schabb,NULL,&s_schabbchase3};
statetype s_schabbchase3        = {false,SPR_SCHABB_W3,10,(statefunc)T_Schabb,NULL,&s_schabbchase3s};
statetype s_schabbchase3s       = {false,SPR_SCHABB_W3,3,NULL,NULL,&s_schabbchase4};
statetype s_schabbchase4        = {false,SPR_SCHABB_W4,8,(statefunc)T_Schabb,NULL,&s_schabbchase1};

statetype s_schabbdeathcam      = {false,SPR_SCHABB_W1,1,NULL,NULL,&s_schabbdie1};

statetype s_schabbdie1          = {false,SPR_SCHABB_W1,10,NULL,(statefunc)A_DeathScream,&s_schabbdie2};
statetype s_schabbdie2          = {false,SPR_SCHABB_W1,10,NULL,NULL,&s_schabbdie3};
statetype s_schabbdie3          = {false,SPR_SCHABB_DIE1,10,NULL,NULL,&s_schabbdie4};
statetype s_schabbdie4          = {false,SPR_SCHABB_DIE2,10,NULL,NULL,&s_schabbdie5};
statetype s_schabbdie5          = {false,SPR_SCHABB_DIE3,10,NULL,NULL,&s_schabbdie6};
statetype s_schabbdie6          = {false,SPR_SCHABB_DEAD,20,NULL,(statefunc)A_StartDeathCam,&s_schabbdie6};

statetype s_schabbshoot1        = {false,SPR_SCHABB_SHOOT1,30,NULL,NULL,&s_schabbshoot2};
statetype s_schabbshoot2        = {false,SPR_SCHABB_SHOOT2,10,NULL,(statefunc)T_SchabbThrow,&s_schabbchase1};

statetype s_needle1             = {false,SPR_HYPO1,6,(statefunc)T_Projectile,NULL,&s_needle2};
statetype s_needle2             = {false,SPR_HYPO2,6,(statefunc)T_Projectile,NULL,&s_needle3};
statetype s_needle3             = {false,SPR_HYPO3,6,(statefunc)T_Projectile,NULL,&s_needle4};
statetype s_needle4             = {false,SPR_HYPO4,6,(statefunc)T_Projectile,NULL,&s_needle1};


//
// gift
//
extern  statetype s_giftstand;

extern  statetype s_giftchase1;
extern  statetype s_giftchase1s;
extern  statetype s_giftchase2;
extern  statetype s_giftchase3;
extern  statetype s_giftchase3s;
extern  statetype s_giftchase4;

extern  statetype s_giftdie1;
extern  statetype s_giftdie2;
extern  statetype s_giftdie3;
extern  statetype s_giftdie4;
extern  statetype s_giftdie5;
extern  statetype s_giftdie6;

extern  statetype s_giftshoot1;
extern  statetype s_giftshoot2;

extern  statetype s_needle1;
extern  statetype s_needle2;
extern  statetype s_needle3;
extern  statetype s_needle4;

extern  statetype s_giftdeathcam;

extern  statetype s_boom1;
extern  statetype s_boom2;
extern  statetype s_boom3;


statetype s_giftstand           = {false,SPR_GIFT_W1,0,(statefunc)T_Stand,NULL,&s_giftstand};

statetype s_giftchase1          = {false,SPR_GIFT_W1,10,(statefunc)T_Gift,NULL,&s_giftchase1s};
statetype s_giftchase1s         = {false,SPR_GIFT_W1,3,NULL,NULL,&s_giftchase2};
statetype s_giftchase2          = {false,SPR_GIFT_W2,8,(statefunc)T_Gift,NULL,&s_giftchase3};
statetype s_giftchase3          = {false,SPR_GIFT_W3,10,(statefunc)T_Gift,NULL,&s_giftchase3s};
statetype s_giftchase3s         = {false,SPR_GIFT_W3,3,NULL,NULL,&s_giftchase4};
statetype s_giftchase4          = {false,SPR_GIFT_W4,8,(statefunc)T_Gift,NULL,&s_giftchase1};

statetype s_giftdeathcam        = {false,SPR_GIFT_W1,1,NULL,NULL,&s_giftdie1};

statetype s_giftdie1            = {false,SPR_GIFT_W1,1,NULL,(statefunc)A_DeathScream,&s_giftdie2};
statetype s_giftdie2            = {false,SPR_GIFT_W1,10,NULL,NULL,&s_giftdie3};
statetype s_giftdie3            = {false,SPR_GIFT_DIE1,10,NULL,NULL,&s_giftdie4};
statetype s_giftdie4            = {false,SPR_GIFT_DIE2,10,NULL,NULL,&s_giftdie5};
statetype s_giftdie5            = {false,SPR_GIFT_DIE3,10,NULL,NULL,&s_giftdie6};
statetype s_giftdie6            = {false,SPR_GIFT_DEAD,20,NULL,(statefunc)A_StartDeathCam,&s_giftdie6};

statetype s_giftshoot1          = {false,SPR_GIFT_SHOOT1,30,NULL,NULL,&s_giftshoot2};
statetype s_giftshoot2          = {false,SPR_GIFT_SHOOT2,10,NULL,(statefunc)T_GiftThrow,&s_giftchase1};


//
// fat
//
extern  statetype s_fatstand;

extern  statetype s_fatchase1;
extern  statetype s_fatchase1s;
extern  statetype s_fatchase2;
extern  statetype s_fatchase3;
extern  statetype s_fatchase3s;
extern  statetype s_fatchase4;

extern  statetype s_fatdie1;
extern  statetype s_fatdie2;
extern  statetype s_fatdie3;
extern  statetype s_fatdie4;
extern  statetype s_fatdie5;
extern  statetype s_fatdie6;

extern  statetype s_fatshoot1;
extern  statetype s_fatshoot2;
extern  statetype s_fatshoot3;
extern  statetype s_fatshoot4;
extern  statetype s_fatshoot5;
extern  statetype s_fatshoot6;

extern  statetype s_needle1;
extern  statetype s_needle2;
extern  statetype s_needle3;
extern  statetype s_needle4;

extern  statetype s_fatdeathcam;


statetype s_fatstand            = {false,SPR_FAT_W1,0,(statefunc)T_Stand,NULL,&s_fatstand};

statetype s_fatchase1           = {false,SPR_FAT_W1,10,(statefunc)T_Fat,NULL,&s_fatchase1s};
statetype s_fatchase1s          = {false,SPR_FAT_W1,3,NULL,NULL,&s_fatchase2};
statetype s_fatchase2           = {false,SPR_FAT_W2,8,(statefunc)T_Fat,NULL,&s_fatchase3};
statetype s_fatchase3           = {false,SPR_FAT_W3,10,(statefunc)T_Fat,NULL,&s_fatchase3s};
statetype s_fatchase3s          = {false,SPR_FAT_W3,3,NULL,NULL,&s_fatchase4};
statetype s_fatchase4           = {false,SPR_FAT_W4,8,(statefunc)T_Fat,NULL,&s_fatchase1};

statetype s_fatdeathcam         = {false,SPR_FAT_W1,1,NULL,NULL,&s_fatdie1};

statetype s_fatdie1             = {false,SPR_FAT_W1,1,NULL,(statefunc)A_DeathScream,&s_fatdie2};
statetype s_fatdie2             = {false,SPR_FAT_W1,10,NULL,NULL,&s_fatdie3};
statetype s_fatdie3             = {false,SPR_FAT_DIE1,10,NULL,NULL,&s_fatdie4};
statetype s_fatdie4             = {false,SPR_FAT_DIE2,10,NULL,NULL,&s_fatdie5};
statetype s_fatdie5             = {false,SPR_FAT_DIE3,10,NULL,NULL,&s_fatdie6};
statetype s_fatdie6             = {false,SPR_FAT_DEAD,20,NULL,(statefunc)A_StartDeathCam,&s_fatdie6};

statetype s_fatshoot1           = {false,SPR_FAT_SHOOT1,30,NULL,NULL,&s_fatshoot2};
statetype s_fatshoot2           = {false,SPR_FAT_SHOOT2,10,NULL,(statefunc)T_GiftThrow,&s_fatshoot3};
statetype s_fatshoot3           = {false,SPR_FAT_SHOOT3,10,NULL,(statefunc)T_Shoot,&s_fatshoot4};
statetype s_fatshoot4           = {false,SPR_FAT_SHOOT4,10,NULL,(statefunc)T_Shoot,&s_fatshoot5};
statetype s_fatshoot5           = {false,SPR_FAT_SHOOT3,10,NULL,(statefunc)T_Shoot,&s_fatshoot6};
statetype s_fatshoot6           = {false,SPR_FAT_SHOOT4,10,NULL,(statefunc)T_Shoot,&s_fatchase1};


/*
===============
=
= SpawnSchabbs
=
===============
*/

void SpawnSchabbs (int tilex, int tiley)
{
    if (DigiMode != sds_Off)
        s_schabbdie2.tictime = 140;
    else
        s_schabbdie2.tictime = 5;

    SpawnNewObj (tilex,tiley,&s_schabbstand);
    newobj->speed = SPDPATROL;

    newobj->obclass = schabbobj;
    newobj->hitpoints = starthitpoints[gamestate.difficulty][en_schabbs] *
        LWMP_BOSSHPFACTOR;
    newobj->dir = nodir;
    newobj->flags |= FL_SHOOTABLE|FL_AMBUSH;
    if (!loadedgame)
        gamestate.killtotal++;
}


/*
===============
=
= SpawnGift
=
===============
*/

void SpawnGift (int tilex, int tiley)
{
    if (DigiMode != sds_Off)
        s_giftdie2.tictime = 140;
    else
        s_giftdie2.tictime = 5;

    SpawnNewObj (tilex,tiley,&s_giftstand);
    newobj->speed = SPDPATROL;

    newobj->obclass = giftobj;
    newobj->hitpoints = starthitpoints[gamestate.difficulty][en_gift] *
        LWMP_BOSSHPFACTOR;
    newobj->dir = nodir;
    newobj->flags |= FL_SHOOTABLE|FL_AMBUSH;
    if (!loadedgame)
        gamestate.killtotal++;
}


/*
===============
=
= SpawnFat
=
===============
*/

void SpawnFat (int tilex, int tiley)
{
    if (DigiMode != sds_Off)
        s_fatdie2.tictime = 140;
    else
        s_fatdie2.tictime = 5;

    SpawnNewObj (tilex,tiley,&s_fatstand);
    newobj->speed = SPDPATROL;

    newobj->obclass = fatobj;
    newobj->hitpoints = starthitpoints[gamestate.difficulty][en_fat] *
        LWMP_BOSSHPFACTOR;
    newobj->dir = nodir;
    newobj->flags |= FL_SHOOTABLE|FL_AMBUSH;
    if (!loadedgame)
        gamestate.killtotal++;
}


/*
=================
=
= T_SchabbThrow
=
=================
*/

void T_SchabbThrow (objtype *ob)
{
    int32_t deltax,deltay;
    float   angle;
    int     iangle_mp;

    deltax = player_mp->x - ob->x;
    deltay = ob->y - player_mp->y;
    angle = (float) atan2((float) deltay, (float) deltax);
    if (angle<0)
        angle = (float) (M_PI*2+angle);
    iangle_mp = (int) (angle/(M_PI*2)*ANGLES);

    GetNewActor ();
    newobj->state = &s_needle1;
    newobj->ticcount = 1;

    newobj->tilex = ob->tilex;
    newobj->tiley = ob->tiley;
    newobj->x = ob->x;
    newobj->y = ob->y;
    newobj->obclass = needleobj;
    newobj->dir = nodir;
    newobj->angle = iangle_mp;
    newobj->speed = 0x2000l;

    newobj->flags = FL_NEVERMARK;
    newobj->active = ac_yes;

    PlaySoundLocActor (SCHABBSTHROWSND,newobj);
}

/*
=================
=
= T_GiftThrow
=
=================
*/

void T_GiftThrow (objtype *ob)
{
    int32_t deltax,deltay;
    float   angle;
    int     iangle_mp;

    deltax = player_mp->x - ob->x;
    deltay = ob->y - player_mp->y;
    angle = (float) atan2((float) deltay, (float) deltax);
    if (angle<0)
        angle = (float) (M_PI*2+angle);
    iangle_mp = (int) (angle/(M_PI*2)*ANGLES);

    GetNewActor ();
    newobj->state = &s_rocket;
    newobj->ticcount = 1;

    newobj->tilex = ob->tilex;
    newobj->tiley = ob->tiley;
    newobj->x = ob->x;
    newobj->y = ob->y;
    newobj->obclass = rocketobj;
    newobj->dir = nodir;
    newobj->angle = iangle_mp;
    newobj->speed = 0x2000l;
    newobj->flags = FL_NEVERMARK;
    newobj->active = ac_yes;

#ifndef APOGEE_1_0          // T_GiftThrow will never be called in shareware v1.0
    PlaySoundLocActor (MISSILEFIRESND,newobj);
#endif
}


/*
=================
=
= T_Schabb
=
=================
*/

void T_Schabb (objtype *ob)
{
    int32_t move;
    int     dx,dy,dist;
    boolean dodge;

    ob->shooting = false;
    T_FindNewTarget(ob);

    dodge = false;
    dx = abs(ob->tilex - player_mp->tilex);
    dy = abs(ob->tiley - player_mp->tiley);
    dist = dx>dy ? dx : dy;

    if (CheckLine(ob))                                              // got a shot at player?
    {
        ob->hidden = false;
        if ( (unsigned) US_RndT() < (tics<<3) && objfreelist)
        {
            ob->shooting = true;
            //
            // go into attack frame
            //
            NewState (ob,&s_schabbshoot1);
            return;
        }
        dodge = true;
    }
    else
        ob->hidden = true;

    if (ob->dir == nodir)
    {
        if (dodge)
            SelectDodgeDir (ob);
        else
            SelectChaseDir (ob);
        if (ob->dir == nodir)
            return;                                                 // object is blocked in
    }

    move = ob->speed*tics;

    while (move)
    {
        if (ob->distance < 0)
        {
            //
            // waiting for a door to open
            //
            OpenDoor (-ob->distance-1);
            if (doorobjlist[-ob->distance-1].action != dr_open)
                return;
            ob->distance = TILEGLOBAL;      // go ahead, the door is now open
            TryWalk(ob);
        }

        if (move < ob->distance)
        {
            MoveObj (ob,move);
            break;
        }

        //
        // reached goal tile, so select another one
        //

        //
        // fix position to account for round off during moving
        //
        ob->x = ((int32_t)ob->tilex<<TILESHIFT)+TILEGLOBAL/2;
        ob->y = ((int32_t)ob->tiley<<TILESHIFT)+TILEGLOBAL/2;

        move -= ob->distance;

        if (dist <4)
            SelectRunDir (ob);
        else if (dodge)
            SelectDodgeDir (ob);
        else
            SelectChaseDir (ob);

        if (ob->dir == nodir)
            return;                                                 // object is blocked in
    }
}


/*
=================
=
= T_Gift
=
=================
*/

void T_Gift (objtype *ob)
{
    int32_t move;
    int     dx,dy,dist;
    boolean dodge;

    ob->shooting = false;
    T_FindNewTarget(ob);

    dodge = false;
    dx = abs(ob->tilex - player_mp->tilex);
    dy = abs(ob->tiley - player_mp->tiley);
    dist = dx>dy ? dx : dy;

    if (CheckLine(ob))                                              // got a shot at player?
    {
        ob->hidden = false;
        if ( (unsigned) US_RndT() < (tics<<3) && objfreelist)
        {
            ob->shooting = true;
            //
            // go into attack frame
            //
            NewState (ob,&s_giftshoot1);
            return;
        }
        dodge = true;
    }
    else
        ob->hidden = true;

    if (ob->dir == nodir)
    {
        if (dodge)
            SelectDodgeDir (ob);
        else
            SelectChaseDir (ob);
        if (ob->dir == nodir)
            return;                                                 // object is blocked in
    }

    move = ob->speed*tics;

    while (move)
    {
        if (ob->distance < 0)
        {
            //
            // waiting for a door to open
            //
            OpenDoor (-ob->distance-1);
            if (doorobjlist[-ob->distance-1].action != dr_open)
                return;
            ob->distance = TILEGLOBAL;      // go ahead, the door is now open
            TryWalk(ob);
        }

        if (move < ob->distance)
        {
            MoveObj (ob,move);
            break;
        }

        //
        // reached goal tile, so select another one
        //

        //
        // fix position to account for round off during moving
        //
        ob->x = ((int32_t)ob->tilex<<TILESHIFT)+TILEGLOBAL/2;
        ob->y = ((int32_t)ob->tiley<<TILESHIFT)+TILEGLOBAL/2;

        move -= ob->distance;

        if (dist <4)
            SelectRunDir (ob);
        else if (dodge)
            SelectDodgeDir (ob);
        else
            SelectChaseDir (ob);

        if (ob->dir == nodir)
            return;                                                 // object is blocked in
    }
}


/*
=================
=
= T_Fat
=
=================
*/

void T_Fat (objtype *ob)
{
    int32_t move;
    int     dx,dy,dist;
    boolean dodge;

    ob->shooting = false;
    T_FindNewTarget(ob);

    dodge = false;
    dx = abs(ob->tilex - player_mp->tilex);
    dy = abs(ob->tiley - player_mp->tiley);
    dist = dx>dy ? dx : dy;

    if (CheckLine(ob))                                              // got a shot at player?
    {
        ob->hidden = false;
        if ( (unsigned) US_RndT() < (tics<<3) && objfreelist)
        {
            ob->shooting = true;
            //
            // go into attack frame
            //
            NewState (ob,&s_fatshoot1);
            return;
        }
        dodge = true;
    }
    else
        ob->hidden = true;

    if (ob->dir == nodir)
    {
        if (dodge)
            SelectDodgeDir (ob);
        else
            SelectChaseDir (ob);
        if (ob->dir == nodir)
            return;                                                 // object is blocked in
    }

    move = ob->speed*tics;

    while (move)
    {
        if (ob->distance < 0)
        {
            //
            // waiting for a door to open
            //
            OpenDoor (-ob->distance-1);
            if (doorobjlist[-ob->distance-1].action != dr_open)
                return;
            ob->distance = TILEGLOBAL;      // go ahead, the door is now open
            TryWalk(ob);
        }

        if (move < ob->distance)
        {
            MoveObj (ob,move);
            break;
        }

        //
        // reached goal tile, so select another one
        //

        //
        // fix position to account for round off during moving
        //
        ob->x = ((int32_t)ob->tilex<<TILESHIFT)+TILEGLOBAL/2;
        ob->y = ((int32_t)ob->tiley<<TILESHIFT)+TILEGLOBAL/2;

        move -= ob->distance;

        if (dist <4)
            SelectRunDir (ob);
        else if (dodge)
            SelectDodgeDir (ob);
        else
            SelectChaseDir (ob);

        if (ob->dir == nodir)
            return;                                                 // object is blocked in
    }
}


/*
=============================================================================

                                    HITLERS

=============================================================================
*/


//
// fake
//
extern  statetype s_fakestand;

extern  statetype s_fakechase1;
extern  statetype s_fakechase1s;
extern  statetype s_fakechase2;
extern  statetype s_fakechase3;
extern  statetype s_fakechase3s;
extern  statetype s_fakechase4;

extern  statetype s_fakedie1;
extern  statetype s_fakedie2;
extern  statetype s_fakedie3;
extern  statetype s_fakedie4;
extern  statetype s_fakedie5;
extern  statetype s_fakedie6;

extern  statetype s_fakeshoot1;
extern  statetype s_fakeshoot2;
extern  statetype s_fakeshoot3;
extern  statetype s_fakeshoot4;
extern  statetype s_fakeshoot5;
extern  statetype s_fakeshoot6;
extern  statetype s_fakeshoot7;
extern  statetype s_fakeshoot8;
extern  statetype s_fakeshoot9;

extern  statetype s_fire1;
extern  statetype s_fire2;

statetype s_fakestand           = {false,SPR_FAKE_W1,0,(statefunc)T_Stand,NULL,&s_fakestand};

statetype s_fakechase1          = {false,SPR_FAKE_W1,10,(statefunc)T_Fake,NULL,&s_fakechase1s};
statetype s_fakechase1s         = {false,SPR_FAKE_W1,3,NULL,NULL,&s_fakechase2};
statetype s_fakechase2          = {false,SPR_FAKE_W2,8,(statefunc)T_Fake,NULL,&s_fakechase3};
statetype s_fakechase3          = {false,SPR_FAKE_W3,10,(statefunc)T_Fake,NULL,&s_fakechase3s};
statetype s_fakechase3s         = {false,SPR_FAKE_W3,3,NULL,NULL,&s_fakechase4};
statetype s_fakechase4          = {false,SPR_FAKE_W4,8,(statefunc)T_Fake,NULL,&s_fakechase1};

statetype s_fakedie1            = {false,SPR_FAKE_DIE1,10,NULL,(statefunc)A_DeathScream,&s_fakedie2};
statetype s_fakedie2            = {false,SPR_FAKE_DIE2,10,NULL,NULL,&s_fakedie3};
statetype s_fakedie3            = {false,SPR_FAKE_DIE3,10,NULL,NULL,&s_fakedie4};
statetype s_fakedie4            = {false,SPR_FAKE_DIE4,10,NULL,NULL,&s_fakedie5};
statetype s_fakedie5            = {false,SPR_FAKE_DIE5,10,NULL,NULL,&s_fakedie6};
statetype s_fakedie6            = {false,SPR_FAKE_DEAD,0,NULL,NULL,&s_fakedie6};

statetype s_fakeshoot1          = {false,SPR_FAKE_SHOOT,8,NULL,(statefunc)T_FakeFire,&s_fakeshoot2};
statetype s_fakeshoot2          = {false,SPR_FAKE_SHOOT,8,NULL,(statefunc)T_FakeFire,&s_fakeshoot3};
statetype s_fakeshoot3          = {false,SPR_FAKE_SHOOT,8,NULL,(statefunc)T_FakeFire,&s_fakeshoot4};
statetype s_fakeshoot4          = {false,SPR_FAKE_SHOOT,8,NULL,(statefunc)T_FakeFire,&s_fakeshoot5};
statetype s_fakeshoot5          = {false,SPR_FAKE_SHOOT,8,NULL,(statefunc)T_FakeFire,&s_fakeshoot6};
statetype s_fakeshoot6          = {false,SPR_FAKE_SHOOT,8,NULL,(statefunc)T_FakeFire,&s_fakeshoot7};
statetype s_fakeshoot7          = {false,SPR_FAKE_SHOOT,8,NULL,(statefunc)T_FakeFire,&s_fakeshoot8};
statetype s_fakeshoot8          = {false,SPR_FAKE_SHOOT,8,NULL,(statefunc)T_FakeFire,&s_fakeshoot9};
statetype s_fakeshoot9          = {false,SPR_FAKE_SHOOT,8,NULL,NULL,&s_fakechase1};

statetype s_fire1               = {false,SPR_FIRE1,6,NULL,(statefunc)T_Projectile,&s_fire2};
statetype s_fire2               = {false,SPR_FIRE2,6,NULL,(statefunc)T_Projectile,&s_fire1};

//
// hitler
//
extern  statetype s_mechachase1;
extern  statetype s_mechachase1s;
extern  statetype s_mechachase2;
extern  statetype s_mechachase3;
extern  statetype s_mechachase3s;
extern  statetype s_mechachase4;

extern  statetype s_mechadie1;
extern  statetype s_mechadie2;
extern  statetype s_mechadie3;
extern  statetype s_mechadie4;

extern  statetype s_mechashoot1;
extern  statetype s_mechashoot2;
extern  statetype s_mechashoot3;
extern  statetype s_mechashoot4;
extern  statetype s_mechashoot5;
extern  statetype s_mechashoot6;


extern  statetype s_hitlerchase1;
extern  statetype s_hitlerchase1s;
extern  statetype s_hitlerchase2;
extern  statetype s_hitlerchase3;
extern  statetype s_hitlerchase3s;
extern  statetype s_hitlerchase4;

extern  statetype s_hitlerdie1;
extern  statetype s_hitlerdie2;
extern  statetype s_hitlerdie3;
extern  statetype s_hitlerdie4;
extern  statetype s_hitlerdie5;
extern  statetype s_hitlerdie6;
extern  statetype s_hitlerdie7;
extern  statetype s_hitlerdie8;
extern  statetype s_hitlerdie9;
extern  statetype s_hitlerdie10;

extern  statetype s_hitlershoot1;
extern  statetype s_hitlershoot2;
extern  statetype s_hitlershoot3;
extern  statetype s_hitlershoot4;
extern  statetype s_hitlershoot5;
extern  statetype s_hitlershoot6;

extern  statetype s_hitlerdeathcam;

statetype s_mechastand          = {false,SPR_MECHA_W1,0,(statefunc)T_Stand,NULL,&s_mechastand};

statetype s_mechachase1         = {false,SPR_MECHA_W1,10,(statefunc)T_Chase,(statefunc)A_MechaSound,&s_mechachase1s};
statetype s_mechachase1s        = {false,SPR_MECHA_W1,6,NULL,NULL,&s_mechachase2};
statetype s_mechachase2         = {false,SPR_MECHA_W2,8,(statefunc)T_Chase,NULL,&s_mechachase3};
statetype s_mechachase3         = {false,SPR_MECHA_W3,10,(statefunc)T_Chase,(statefunc)A_MechaSound,&s_mechachase3s};
statetype s_mechachase3s        = {false,SPR_MECHA_W3,6,NULL,NULL,&s_mechachase4};
statetype s_mechachase4         = {false,SPR_MECHA_W4,8,(statefunc)T_Chase,NULL,&s_mechachase1};

statetype s_mechadie1           = {false,SPR_MECHA_DIE1,10,NULL,(statefunc)A_DeathScream,&s_mechadie2};
statetype s_mechadie2           = {false,SPR_MECHA_DIE2,10,NULL,NULL,&s_mechadie3};
statetype s_mechadie3           = {false,SPR_MECHA_DIE3,10,NULL,(statefunc)A_HitlerMorph,&s_mechadie4};
statetype s_mechadie4           = {false,SPR_MECHA_DEAD,0,NULL,NULL,&s_mechadie4};

statetype s_mechashoot1         = {false,SPR_MECHA_SHOOT1,30,NULL,NULL,&s_mechashoot2};
statetype s_mechashoot2         = {false,SPR_MECHA_SHOOT2,10,NULL,(statefunc)T_Shoot,&s_mechashoot3};
statetype s_mechashoot3         = {false,SPR_MECHA_SHOOT3,10,NULL,(statefunc)T_Shoot,&s_mechashoot4};
statetype s_mechashoot4         = {false,SPR_MECHA_SHOOT2,10,NULL,(statefunc)T_Shoot,&s_mechashoot5};
statetype s_mechashoot5         = {false,SPR_MECHA_SHOOT3,10,NULL,(statefunc)T_Shoot,&s_mechashoot6};
statetype s_mechashoot6         = {false,SPR_MECHA_SHOOT2,10,NULL,(statefunc)T_Shoot,&s_mechachase1};


statetype s_hitlerchase1        = {false,SPR_HITLER_W1,6,(statefunc)T_Chase,NULL,&s_hitlerchase1s};
statetype s_hitlerchase1s       = {false,SPR_HITLER_W1,4,NULL,NULL,&s_hitlerchase2};
statetype s_hitlerchase2        = {false,SPR_HITLER_W2,2,(statefunc)T_Chase,NULL,&s_hitlerchase3};
statetype s_hitlerchase3        = {false,SPR_HITLER_W3,6,(statefunc)T_Chase,NULL,&s_hitlerchase3s};
statetype s_hitlerchase3s       = {false,SPR_HITLER_W3,4,NULL,NULL,&s_hitlerchase4};
statetype s_hitlerchase4        = {false,SPR_HITLER_W4,2,(statefunc)T_Chase,NULL,&s_hitlerchase1};

statetype s_hitlerdeathcam      = {false,SPR_HITLER_W1,10,NULL,NULL,&s_hitlerdie1};

statetype s_hitlerdie1          = {false,SPR_HITLER_W1,1,NULL,(statefunc)A_DeathScream,&s_hitlerdie2};
statetype s_hitlerdie2          = {false,SPR_HITLER_W1,10,NULL,NULL,&s_hitlerdie3};
statetype s_hitlerdie3          = {false,SPR_HITLER_DIE1,10,NULL,(statefunc)A_Slurpie,&s_hitlerdie4};
statetype s_hitlerdie4          = {false,SPR_HITLER_DIE2,10,NULL,NULL,&s_hitlerdie5};
statetype s_hitlerdie5          = {false,SPR_HITLER_DIE3,10,NULL,NULL,&s_hitlerdie6};
statetype s_hitlerdie6          = {false,SPR_HITLER_DIE4,10,NULL,NULL,&s_hitlerdie7};
statetype s_hitlerdie7          = {false,SPR_HITLER_DIE5,10,NULL,NULL,&s_hitlerdie8};
statetype s_hitlerdie8          = {false,SPR_HITLER_DIE6,10,NULL,NULL,&s_hitlerdie9};
statetype s_hitlerdie9          = {false,SPR_HITLER_DIE7,10,NULL,NULL,&s_hitlerdie10};
statetype s_hitlerdie10         = {false,SPR_HITLER_DEAD,20,NULL,(statefunc)A_StartDeathCam,&s_hitlerdie10};

statetype s_hitlershoot1        = {false,SPR_HITLER_SHOOT1,30,NULL,NULL,&s_hitlershoot2};
statetype s_hitlershoot2        = {false,SPR_HITLER_SHOOT2,10,NULL,(statefunc)T_Shoot,&s_hitlershoot3};
statetype s_hitlershoot3        = {false,SPR_HITLER_SHOOT3,10,NULL,(statefunc)T_Shoot,&s_hitlershoot4};
statetype s_hitlershoot4        = {false,SPR_HITLER_SHOOT2,10,NULL,(statefunc)T_Shoot,&s_hitlershoot5};
statetype s_hitlershoot5        = {false,SPR_HITLER_SHOOT3,10,NULL,(statefunc)T_Shoot,&s_hitlershoot6};
statetype s_hitlershoot6        = {false,SPR_HITLER_SHOOT2,10,NULL,(statefunc)T_Shoot,&s_hitlerchase1};



/*
===============
=
= SpawnFakeHitler
=
===============
*/

void SpawnFakeHitler (int tilex, int tiley)
{
    if (DigiMode != sds_Off)
        s_hitlerdie2.tictime = 140;
    else
        s_hitlerdie2.tictime = 5;

    SpawnNewObj (tilex,tiley,&s_fakestand);
    newobj->speed = SPDPATROL;

    newobj->obclass = fakeobj;
    newobj->hitpoints = starthitpoints[gamestate.difficulty][en_fake] *
        LWMP_BOSSHPFACTOR;
    newobj->dir = nodir;
    newobj->flags |= FL_SHOOTABLE|FL_AMBUSH;
    if (!loadedgame)
        gamestate.killtotal++;
}


/*
===============
=
= SpawnHitler
=
===============
*/

void SpawnHitler (int tilex, int tiley)
{
    if (DigiMode != sds_Off)
        s_hitlerdie2.tictime = 140;
    else
        s_hitlerdie2.tictime = 5;


    SpawnNewObj (tilex,tiley,&s_mechastand);
    newobj->speed = SPDPATROL;

    newobj->obclass = mechahitlerobj;
    newobj->hitpoints = starthitpoints[gamestate.difficulty][en_hitler] *
        LWMP_BOSSHPFACTOR;
    newobj->dir = nodir;
    newobj->flags |= FL_SHOOTABLE|FL_AMBUSH;
    if (!loadedgame)
        gamestate.killtotal++;
}


/*
===============
=
= A_HitlerMorph
=
===============
*/

void A_HitlerMorph (objtype *ob)
{
    short hitpoints[4]={500,700,800,900};

    SpawnNewObj (ob->tilex,ob->tiley,&s_hitlerchase1);
    newobj->speed = SPDPATROL*5;

    newobj->x = ob->x;
    newobj->y = ob->y;

    newobj->distance = ob->distance;
    newobj->dir = ob->dir;
    newobj->flags = ob->flags | FL_SHOOTABLE;
    newobj->flags &= ~FL_NONMARK;   // hitler stuck with nodir fix

    newobj->obclass = realhitlerobj;
    newobj->hitpoints = hitpoints[gamestate.difficulty];
}


////////////////////////////////////////////////////////
//
// A_MechaSound
// A_Slurpie
//
////////////////////////////////////////////////////////
void A_MechaSound (objtype *ob)
{
    int c;

    for (LWMP_REPEAT(c))
    {
        if (areabyplayer_mp[ob->areanumber])
        {
            PlaySoundLocActor (MECHSTEPSND,ob);
            LWMP_ENDREPEAT(c);
            break;
        }
    }
}

void A_Slurpie (objtype *)
{
    SD_PlaySound(SLURPIESND);
}

/*
=================
=
= T_FakeFire
=
=================
*/

void T_FakeFire (objtype *ob)
{
    int32_t deltax,deltay;
    float   angle;
    int     iangle_mp;

    if (!objfreelist)       // stop shooting if over MAXACTORS
    {
        NewState (ob,&s_fakechase1);
        return;
    }

    deltax = player_mp->x - ob->x;
    deltay = ob->y - player_mp->y;
    angle = (float) atan2((float) deltay, (float) deltax);
    if (angle<0)
        angle = (float)(M_PI*2+angle);
    iangle_mp = (int) (angle/(M_PI*2)*ANGLES);

    GetNewActor ();
    newobj->state = &s_fire1;
    newobj->ticcount = 1;

    newobj->tilex = ob->tilex;
    newobj->tiley = ob->tiley;
    newobj->x = ob->x;
    newobj->y = ob->y;
    newobj->dir = nodir;
    newobj->angle = iangle_mp;
    newobj->obclass = fireobj;
    newobj->speed = 0x1200l;
    newobj->flags = FL_NEVERMARK;
    newobj->active = ac_yes;

    PlaySoundLocActor (FLAMETHROWERSND,newobj);
}



/*
=================
=
= T_Fake
=
=================
*/

void T_Fake (objtype *ob)
{
    int32_t move;

    ob->shooting = false;
    T_FindNewTarget(ob);

    if (CheckLine(ob))                      // got a shot at player?
    {
        ob->hidden = false;
        if ( (unsigned) US_RndT() < (tics<<1) && objfreelist)
        {
            ob->shooting = true;
            //
            // go into attack frame
            //
            NewState (ob,&s_fakeshoot1);
            return;
        }
    }
    else
        ob->hidden = true;

    if (ob->dir == nodir)
    {
        SelectDodgeDir (ob);
        if (ob->dir == nodir)
            return;                                                 // object is blocked in
    }

    move = ob->speed*tics;

    while (move)
    {
        if (move < ob->distance)
        {
            MoveObj (ob,move);
            break;
        }

        //
        // reached goal tile, so select another one
        //

        //
        // fix position to account for round off during moving
        //
        ob->x = ((int32_t)ob->tilex<<TILESHIFT)+TILEGLOBAL/2;
        ob->y = ((int32_t)ob->tiley<<TILESHIFT)+TILEGLOBAL/2;

        move -= ob->distance;

        SelectDodgeDir (ob);

        if (ob->dir == nodir)
            return;                                                 // object is blocked in
    }
}

#endif
/*
============================================================================

STAND

============================================================================
*/


/*
===============
=
= T_Stand
=
===============
*/

void T_Stand (objtype *ob)
{
    SightPlayer (ob);
}


/*
============================================================================

CHASE

============================================================================
*/

/*
=================
=
= T_FindNewTarget
=
=================
*/

void T_FindNewTarget(objtype *ob)
{
    int c;
    int mindist, dist;
    int newtarget;
    int dx, dy;

    if (LWMP_NUM_PLAYERS == 1)
    {
        return;
    }

    ob->findnewtarget_tics += tics;
    if (ob->findnewtarget_tics < FINDNEWTARGET_TIMEOUT)
    {
        return;
    }

    ob->findnewtarget_tics = 0;

    mindist = 256;
    newtarget = -1;

    // find the closest visible player
    for (LWMP_REPEAT(c))
    {
        if (CheckLine(ob))      // got a shot at player?
        {
            dx = abs(ob->tilex - player_mp->tilex);
            dy = abs(ob->tiley - player_mp->tiley);
            dist = dx > dy ? dx : dy;

            if (dist < mindist)
            {
                mindist = dist;
                newtarget = LWMP_GetInstance();
            }
        }
    }

    // change targets
    if (newtarget >= 0)
    {
        ob->targetplayer = newtarget + 1;
    }
}

/*
=================
=
= T_Chase
=
=================
*/

void T_Chase (objtype *ob)
{
    int32_t move,target;
    int     dx,dy,dist,chance;
    boolean dodge;

    ob->shooting = false;
    ob->attackingPlayer = 0;

    if (ZombieMode::enabled() && ob->hitpoints<=0)
    {
        if (ZombieMode::wantsRest(ob))
        {
            return;
        }
    }
    else if (ob->hitpoints<=0)
    {
        KillActor (ob);
        return;
    }

    if (gamestate.victoryflag)
        return;

    T_FindNewTarget(ob);

    dodge = false;
    if (CheckLine(ob))      // got a shot at player?
    {
        ob->hidden = false;
        dx = abs(ob->tilex - player_mp->tilex);
        dy = abs(ob->tiley - player_mp->tiley);
        dist = dx>dy ? dx : dy;

        if (ZombieMode::enabled())
        {
            ZombieMode::shootChance(ob, chance);
        }
        else if (ZombieHarvesterMode::wasAlive(ob))
        {
            ZombieHarvesterMode::shootChance(ob, chance);
        }
#ifdef PLAYDEMOLIKEORIGINAL
        else if(DEMOCOND_ORIG)
        {
            if(!dist || (dist == 1 && ob->distance < 0x4000))
                chance = 300;
            else
                chance = (tics<<4)/dist;
        }
        else
#endif
        {
            if (dist)
                chance = (tics<<4)/dist;
            else
                chance = 300;

            if (dist == 1)
            {
                target = abs(ob->x - player_mp->x);
                if (target < 0x14000l)
                {
                    target = abs(ob->y - player_mp->y);
                    if (target < 0x14000l)
                        chance = 300;
                }
            }
        }

        if ( US_RndT()<chance)
        {
            ob->shooting = true;
            //
            // go into attack frame
            //
            switch (ob->obclass)
            {
                case guardobj:
                    NewState (ob,&s_grdshoot1);
                    break;
                case officerobj:
                    NewState (ob,&s_ofcshoot1);
                    break;
                case mutantobj:
                    NewState (ob,&s_mutshoot1);
                    break;
                case ssobj:
                    NewState (ob,&s_ssshoot1);
                    break;
                case bjmutantobj:
                    BjMutant_EnterShootState(ob);
                    break;
#ifndef SPEAR
                case bossobj:
                    NewState (ob,&s_bossshoot1);
                    break;
                case gretelobj:
                    NewState (ob,&s_gretelshoot1);
                    break;
                case mechahitlerobj:
                    NewState (ob,&s_mechashoot1);
                    break;
                case realhitlerobj:
                    NewState (ob,&s_hitlershoot1);
                    break;
#else
                case angelobj:
                    NewState (ob,&s_angelshoot1);
                    break;
                case transobj:
                    NewState (ob,&s_transshoot1);
                    break;
                case uberobj:
                    NewState (ob,&s_ubershoot1);
                    break;
                case willobj:
                    NewState (ob,&s_willshoot1);
                    break;
                case deathobj:
                    NewState (ob,&s_deathshoot1);
                    break;
#endif
            }
            Gib::exitPainHold(ob);
            return;
        }
        dodge = true;
    }
    else
        ob->hidden = true;

    if (ob->dir == nodir)
    {
        if (dodge)
            SelectDodgeDir (ob);
        else
            SelectChaseDir (ob);
        if (ob->dir == nodir)
            return;                                                 // object is blocked in
    }

    move = ob->speed*tics;

    while (move)
    {
        if (ob->distance < 0)
        {
            //
            // waiting for a door to open
            //
            OpenDoor (-ob->distance-1);
            if (doorobjlist[-ob->distance-1].action != dr_open)
                return;
            ob->distance = TILEGLOBAL;      // go ahead, the door is now open
            DEMOIF_SDL
            {
                TryWalk(ob);
            }
        }

        if (move < ob->distance)
        {
            MoveObj (ob,move);
            break;
        }

        //
        // reached goal tile, so select another one
        //

        //
        // fix position to account for round off during moving
        //
        ob->x = ((int32_t)ob->tilex<<TILESHIFT)+TILEGLOBAL/2;
        ob->y = ((int32_t)ob->tiley<<TILESHIFT)+TILEGLOBAL/2;

        move -= ob->distance;

        if (dodge)
            SelectDodgeDir (ob);
        else
            SelectChaseDir (ob);

        if (ob->dir == nodir)
            return;                                                 // object is blocked in
    }
}


/*
=================
=
= T_Ghosts
=
=================
*/

void T_Ghosts (objtype *ob)
{
    int32_t move;

    if (ob->dir == nodir)
    {
        SelectChaseDir (ob);
        if (ob->dir == nodir)
            return;                                                 // object is blocked in
    }

    move = ob->speed*tics;

    while (move)
    {
        if (move < ob->distance)
        {
            MoveObj (ob,move);
            break;
        }

        //
        // reached goal tile, so select another one
        //

        //
        // fix position to account for round off during moving
        //
        ob->x = ((int32_t)ob->tilex<<TILESHIFT)+TILEGLOBAL/2;
        ob->y = ((int32_t)ob->tiley<<TILESHIFT)+TILEGLOBAL/2;

        move -= ob->distance;

        SelectChaseDir (ob);

        if (ob->dir == nodir)
            return;                                                 // object is blocked in
    }
}

/*
=================
=
= T_DogChase
=
=================
*/

void T_DogChase (objtype *ob)
{
    int32_t    move;
    int32_t    dx,dy;

    ob->shooting = false;
    ob->attackingPlayer = 0;

    if (ZombieMode::enabled() && ob->hitpoints<=0)
    {
        if (ZombieMode::wantsRest(ob))
        {
            return;
        }
    }

    if (ob->dir == nodir)
    {
        SelectDodgeDir (ob);
        if (ob->dir == nodir)
            return;                                                 // object is blocked in
    }

    move = ob->speed*tics;

    while (move)
    {
        //
        // check for byte range
        //
        dx = player_mp->x - ob->x;
        if (dx<0)
            dx = -dx;
        dx -= move;
        if (dx <= MINACTORDIST)
        {
            dy = player_mp->y - ob->y;
            if (dy<0)
                dy = -dy;
            dy -= move;
            if (dy <= MINACTORDIST)
            {
                ob->shooting = true;
                switch (ob->obclass)
                {
                case bjmutantdogobj:
                    BjMutant_EnterShootState(ob);
                    break;
                default:
                    NewState (ob,&s_dogjump1);
                    break;
                }
                return;
            }
        }

        if (move < ob->distance)
        {
            MoveObj (ob,move);
            break;
        }

        //
        // reached goal tile, so select another one
        //

        //
        // fix position to account for round off during moving
        //
        ob->x = ((int32_t)ob->tilex<<TILESHIFT)+TILEGLOBAL/2;
        ob->y = ((int32_t)ob->tiley<<TILESHIFT)+TILEGLOBAL/2;

        move -= ob->distance;

        SelectDodgeDir (ob);

        if (ob->dir == nodir)
            return;                                                 // object is blocked in
    }
}



/*
============================================================================

                                    PATH

============================================================================
*/


/*
===============
=
= SelectPathDir
=
===============
*/

void SelectPathDir (objtype *ob)
{
    unsigned spot;

    spot = MAPSPOT(ob->tilex,ob->tiley,1)-ICONARROWS;

    if (spot<8)
    {
        // new direction
        ob->dir = (dirtype)(spot);
    }

    ob->distance = TILEGLOBAL;

    if (!TryWalk (ob))
        ob->dir = nodir;
}


/*
===============
=
= T_Path
=
===============
*/

void T_Path (objtype *ob)
{
    int32_t    move;

    if (SightPlayer (ob))
        return;

    if (ob->dir == nodir)
    {
        SelectPathDir (ob);
        if (ob->dir == nodir)
            return;                                 // all movement is blocked
    }


    move = ob->speed*tics;

    while (move)
    {
        if (ob->distance < 0)
        {
            //
            // waiting for a door to open
            //
            OpenDoor (-ob->distance-1);
            if (doorobjlist[-ob->distance-1].action != dr_open)
                return;
            ob->distance = TILEGLOBAL;      // go ahead, the door is now open
            DEMOIF_SDL
            {
                TryWalk(ob);
            }
        }

        if (move < ob->distance)
        {
            MoveObj (ob,move);
            break;
        }

        if (ob->tilex>MAPSIZE || ob->tiley>MAPSIZE)
        {
            sprintf (str, "T_Path hit a wall at %u,%u, dir %u",
                ob->tilex,ob->tiley,ob->dir);
            Quit (str);
        }

        ob->x = ((int32_t)ob->tilex<<TILESHIFT)+TILEGLOBAL/2;
        ob->y = ((int32_t)ob->tiley<<TILESHIFT)+TILEGLOBAL/2;
        move -= ob->distance;

        SelectPathDir (ob);

        if (ob->dir == nodir)
            return;                                 // all movement is blocked
    }
}


/*
=============================================================================

                                    FIGHT

=============================================================================
*/


/*
===============
=
= T_Shoot
=
= Try to damage the player, based on skill level and player's speed
=
===============
*/

void T_Shoot (objtype *ob)
{
    int     dx,dy,dist;
    int     hitchance,damage;

    if (ZombieMode::enabled())
    {
        ZombieMode::bite(ob);
        return;
    }
    else if (ZombieHarvesterMode::wasAlive(ob))
    {
        ZombieHarvesterMode::bite(ob);
        return;
    }

    hitchance = 128;

    if (!areabyplayer_mp[ob->areanumber])
        return;

    if (CheckLine (ob))                    // player is not behind a wall
    {
        dx = abs(ob->tilex - player_mp->tilex);
        dy = abs(ob->tiley - player_mp->tiley);
        dist = dx>dy ? dx:dy;

        if (ob->obclass == ssobj || ob->obclass == bossobj)
        {
            dist = dist*2/3; // ss are better shots
        }

        if (thrustspeed_mp >= RUNSPEED)
        {
            if (ob->flags&FL_VISABLE)
                hitchance = 160-dist*16;                // player can see to dodge
            else
                hitchance = 160-dist*8;
        }
        else
        {
            if (ob->flags&FL_VISABLE)
                hitchance = 256-dist*16;                // player can see to dodge
            else
                hitchance = 256-dist*8;
        }

        // see if the shot was a hit

        if (US_RndT()<hitchance)
        {
            if (dist<2)
                damage = US_RndT()>>2;
            else if (dist<4)
                damage = US_RndT()>>3;
            else
                damage = US_RndT()>>4;

            TakeDamage (damage,ob);
        }
    }

    switch(ob->obclass)
    {
        case ssobj:
            PlaySoundLocActor(SSFIRESND,ob);
            break;
        case bjmutantobj:
            BjMutant_PlayShootSound(ob);
            break;
#ifndef SPEAR
#ifndef APOGEE_1_0
        case giftobj:
        case fatobj:
            PlaySoundLocActor(MISSILEFIRESND,ob);
            break;
#endif
        case mechahitlerobj:
        case realhitlerobj:
        case bossobj:
            PlaySoundLocActor(BOSSFIRESND,ob);
            break;
        case schabbobj:
            PlaySoundLocActor(SCHABBSTHROWSND,ob);
            break;
        case fakeobj:
            PlaySoundLocActor(FLAMETHROWERSND,ob);
            break;
#endif
        default:
            PlaySoundLocActor(NAZIFIRESND,ob);
    }
}


/*
===============
=
= T_Bite
=
===============
*/

void T_Bite (objtype *ob)
{
    int32_t    dx,dy;

    switch (ob->obclass)
    {
    case bjmutantdogobj:
        BjMutant_PlayShootSound(ob);
        break;
    default:
        PlaySoundLocActor(DOGATTACKSND,ob);     // JAB
        break;
    }

    dx = player_mp->x - ob->x;
    if (dx<0)
        dx = -dx;
    dx -= TILEGLOBAL;
    if (dx <= MINACTORDIST)
    {
        dy = player_mp->y - ob->y;
        if (dy<0)
            dy = -dy;
        dy -= TILEGLOBAL;
        if (dy <= MINACTORDIST)
        {
            if (US_RndT()<180)
            {
                TakeDamage (US_RndT()>>4,ob);
                return;
            }
        }
    }
}

void Object_StopSoundPosChannel(objtype *ob, int channel)
{
    for (int i = 0; i < lwlib_CountArray(ob->soundPosChannel); i++)
    {
        int &objSoundPosChannel = ob->soundPosChannel[i];

        if (objSoundPosChannel > 0 && 
            (channel == -1 || objSoundPosChannel - 1 == channel - 1))
        {
            SD_StopChannel(objSoundPosChannel - 1);
            objSoundPosChannel = 0;
        }
    }

    ClearObjectSoundLoc((ob - objlist) + 1, channel);
}

void Object_FadeOutSoundPosChannel(objtype *ob, int channel)
{
    for (int i = 0; i < lwlib_CountArray(ob->soundPosChannel); i++)
    {
        int &objSoundPosChannel = ob->soundPosChannel[i];

        if (objSoundPosChannel > 0 && 
            (channel == -1 || objSoundPosChannel - 1 == channel - 1))
        {
            SD_FadeOutChannel(objSoundPosChannel - 1);
        }
    }
}

bool Object_SoundPlaying(objtype *ob, int channel)
{
    for (int i = 0; i < lwlib_CountArray(ob->soundPosChannel); i++)
    {
        int &objSoundPosChannel = ob->soundPosChannel[i];

        if (objSoundPosChannel > 0 && 
            (channel == -1 || objSoundPosChannel - 1 == channel - 1) &&
            Mix_Playing(objSoundPosChannel - 1))
        {
            return true;
        }
    }

    return false;
}

#ifndef SPEAR
/*
============================================================================

                                    BJ VICTORY

============================================================================
*/


//
// BJ victory
//

void T_BJRun (objtype *ob);
void T_BJJump (objtype *ob);
void T_BJDone (objtype *ob);
void T_BJYell (objtype *ob);

void T_DeathCam (objtype *ob);

extern  statetype s_bjrun1;
extern  statetype s_bjrun1s;
extern  statetype s_bjrun2;
extern  statetype s_bjrun3;
extern  statetype s_bjrun3s;
extern  statetype s_bjrun4;

extern  statetype s_bjjump1;
extern  statetype s_bjjump2;
extern  statetype s_bjjump3;
extern  statetype s_bjjump4;


statetype s_bjrun1              = {false,SPR_BJ_W1,12,(statefunc)T_BJRun,NULL,&s_bjrun1s};
statetype s_bjrun1s             = {false,SPR_BJ_W1,3, NULL,NULL,&s_bjrun2};
statetype s_bjrun2              = {false,SPR_BJ_W2,8,(statefunc)T_BJRun,NULL,&s_bjrun3};
statetype s_bjrun3              = {false,SPR_BJ_W3,12,(statefunc)T_BJRun,NULL,&s_bjrun3s};
statetype s_bjrun3s             = {false,SPR_BJ_W3,3, NULL,NULL,&s_bjrun4};
statetype s_bjrun4              = {false,SPR_BJ_W4,8,(statefunc)T_BJRun,NULL,&s_bjrun1};


statetype s_bjjump1             = {false,SPR_BJ_JUMP1,14,(statefunc)T_BJJump,NULL,&s_bjjump2};
statetype s_bjjump2             = {false,SPR_BJ_JUMP2,14,(statefunc)T_BJJump,(statefunc)T_BJYell,&s_bjjump3};
statetype s_bjjump3             = {false,SPR_BJ_JUMP3,14,(statefunc)T_BJJump,NULL,&s_bjjump4};
statetype s_bjjump4             = {false,SPR_BJ_JUMP4,300,NULL,(statefunc)T_BJDone,&s_bjjump4};


statetype s_deathcam            = {false,0,0,NULL,NULL,NULL};


/*
===============
=
= SpawnBJVictory
=
===============
*/

void SpawnBJVictory (void)
{
    int c;
    int tilex, tiley;
    objtype *victoryPlayer;

    victoryPlayer = player_mp;

    for (LWMP_REPEAT(c))
    {
        if (c != 0)
        {
            if (abs(player_mp->tilex - victoryPlayer->tilex) > 2 || 
                abs(player_mp->tiley - victoryPlayer->tiley) > 2)
            {
                continue;
            }
        }

        SpawnNewObj (player_mp->tilex,player_mp->tiley+1,&s_bjrun1);
        newobj->x = player_mp->x;
        newobj->y = player_mp->y;
        newobj->obclass = bjobj;
        newobj->dir = north;
        newobj->temp1 = 6;                      // tiles to run forward
        newobj->player_index = player_mp->player_index;

        player_mp->victoryBjSpawn = true;
        levelcompletedby = LWMP_GetInstance();
    }
}



/*
===============
=
= T_BJRun
=
===============
*/

void T_BJRun (objtype *ob)
{
    int32_t    move;

    move = BJRUNSPEED*tics;

    while (move)
    {
        if (move < ob->distance)
        {
            MoveObj (ob,move);
            break;
        }


        ob->x = ((int32_t)ob->tilex<<TILESHIFT)+TILEGLOBAL/2;
        ob->y = ((int32_t)ob->tiley<<TILESHIFT)+TILEGLOBAL/2;
        move -= ob->distance;

        SelectPathDir (ob);

        if ( !(--ob->temp1) )
        {
            NewState (ob,&s_bjjump1);
            return;
        }
    }
}


/*
===============
=
= T_BJJump
=
===============
*/

void T_BJJump (objtype *ob)
{
    int32_t    move;

    move = BJJUMPSPEED*tics;
    MoveObj (ob,move);
}


/*
===============
=
= T_BJYell
=
===============
*/

void T_BJYell (objtype *ob)
{
    PlaySoundLocActor(YEAHSND,ob);  // JAB
}


/*
===============
=
= T_BJDone
=
===============
*/

void T_BJDone (objtype *)
{
    SetPlayState(ex_victorious);  // exit castle tile
}



//===========================================================================


/*
===============
=
= CheckPosition
=
===============
*/

boolean CheckPosition (objtype *ob)
{
    int     x,y,xl,yl,xh,yh;
    objtype *check;

    xl = (ob->x-PLAYERSIZE) >> TILESHIFT;
    yl = (ob->y-PLAYERSIZE) >> TILESHIFT;

    xh = (ob->x+PLAYERSIZE) >> TILESHIFT;
    yh = (ob->y+PLAYERSIZE) >> TILESHIFT;

    //
    // check for solid walls
    //
    for (y=yl;y<=yh;y++)
    {
        for (x=xl;x<=xh;x++)
        {
            check = actorat[x][y];
            if (check && !ISPOINTER(check))
                return false;
        }
    }

    return true;
}


/*
===============
=
= A_StartDeathCamKillBy
=
===============
*/

void    A_StartDeathCamKillBy (objtype *ob)
{
    int32_t dx,dy;
    float   fangle;
    int32_t xmove,ymove;
    int32_t dist;
    int c, i;

    FinishPaletteShifts ();

    VW_WaitVBL (100);

    if (gamestate.victoryflag)
    {
        SetPlayState(ex_victorious);  // exit castle tile
        levelcompletedby = LWMP_GetInstance();
        return;
    }

    if(usedoublebuffering) VW_UpdateScreen();

    gamestate.victoryflag = true;
    unsigned fadeheight = viewsize != 21 ? 
        MaxY - LWMP_StatusLines() : MaxY;

    for (LWMP_REPEAT(c))
    {
        VL_BarScaledCoord (0, 0, MaxX, fadeheight, bordercol);
    }
    FizzleFade(0, 0, MaxX, fadeheight, 70, false);

    if (bordercol != VIEWCOLOR)
    {
        CA_CacheGrChunk (STARTFONT+1);
        fontnumber = 1;
        SETFONTCOLOR(15,bordercol);
        PrintX = 68; PrintY = 45;
        US_Print (STR_SEEAGAIN);
        UNCACHEGRCHUNK(STARTFONT+1);
    }
    else
    {
        CacheLump(LEVELEND_LUMP_START,LEVELEND_LUMP_END);
        for (LWMP_NONSPLIT(c))
        {
            #ifdef JAPAN
            #ifndef JAPDEMO
            CA_CacheScreen(C_LETSSEEPIC);
            #endif
            #else
            Write(0,7,STR_SEEAGAIN);
            #endif
        }
    }

    VW_UpdateScreen ();
    if(usedoublebuffering) VW_UpdateScreen();

    IN_UserInput(300);

    //
    // line angle up exactly
    //
    NewState (player_mp,&s_deathcam);

    player_mp->x = gamestate.killx;
    player_mp->y = gamestate.killy;

    dx = ob->x - player_mp->x;
    dy = player_mp->y - ob->y;

    fangle = (float) atan2((float) dy, (float) dx);          // returns -pi to pi
    if (fangle<0)
        fangle = (float) (M_PI*2+fangle);

    player_mp->angle = (short) (fangle/(M_PI*2)*ANGLES);

    //
    // try to position as close as possible without being in a wall
    //
    dist = 0x14000l;
    do
    {
        xmove = FixedMul(dist,costable[player_mp->angle]);
        ymove = -FixedMul(dist,sintable[player_mp->angle]);

        player_mp->x = ob->x - xmove;
        player_mp->y = ob->y - ymove;
        dist += 0x1000;

    } while (!CheckPosition (player_mp));
    plux_mp = (word)(player_mp->x >> UNSIGNEDSHIFT);                      // scale to fit in unsigned
    pluy_mp = (word)(player_mp->y >> UNSIGNEDSHIFT);
    player_mp->tilex = (word)(player_mp->x >> TILESHIFT);         // scale to tile values
    player_mp->tiley = (word)(player_mp->y >> TILESHIFT);

    i = LWMP_GetInstance();
    for (LWMP_REPEAT(c))
    {
        NewState (player_mp,&s_deathcam);
        player_mp->x = player[i]->x;
        player_mp->y = player[i]->y;
        plux_mp = plux[i];
        pluy_mp = pluy[i];
        player_mp->tilex = player[i]->tilex;
        player_mp->tiley = player[i]->tiley;
        player_mp->angle = player[i]->angle;
    }

    //
    // go back to the game
    //
    DrawPlayBorder ();

    fizzlein = true;

    switch (ob->obclass)
    {
#ifndef SPEAR
        case schabbobj:
            NewState (ob,&s_schabbdeathcam);
            break;
        case realhitlerobj:
            NewState (ob,&s_hitlerdeathcam);
            break;
        case giftobj:
            NewState (ob,&s_giftdeathcam);
            break;
        case fatobj:
            NewState (ob,&s_fatdeathcam);
            break;
#endif
    }
}

void    A_StartDeathCam (objtype *ob)
{
    int c;

    for (LWMP_REPEAT_ONCE(c, gamestate.killby))
    {
        A_StartDeathCamKillBy(ob);
    }
}

#endif

/*
============================================================================

                                    BJ MUTANT 

============================================================================
*/

extern  statetype s_bjmutantstand;

extern  statetype s_bjmutantpain;
extern  statetype s_bjmutantpain1;

extern  statetype s_bjmutantchaingunshoot1;
extern  statetype s_bjmutantchaingunshoot2;
extern  statetype s_bjmutantchaingunshoot3;
extern  statetype s_bjmutantchaingunshoot4;
extern  statetype s_bjmutantchaingunshoot5;
extern  statetype s_bjmutantchaingunshoot6;
extern  statetype s_bjmutantchaingunshoot7;
extern  statetype s_bjmutantchaingunshoot8;
extern  statetype s_bjmutantchaingunshoot9;

extern  statetype s_bjmutantmachinegunshoot1;
extern  statetype s_bjmutantmachinegunshoot2;
extern  statetype s_bjmutantmachinegunshoot3;
extern  statetype s_bjmutantmachinegunshoot4;
extern  statetype s_bjmutantmachinegunshoot5;
extern  statetype s_bjmutantmachinegunshoot6;
extern  statetype s_bjmutantmachinegunshoot7;
extern  statetype s_bjmutantmachinegunshoot8;
extern  statetype s_bjmutantmachinegunshoot9;

extern  statetype s_bjmutantpistolshoot1;
extern  statetype s_bjmutantpistolshoot2;
extern  statetype s_bjmutantpistolshoot3;
extern  statetype s_bjmutantpistolshoot4;
extern  statetype s_bjmutantpistolshoot5;

extern  statetype s_bjmutantchase1;
extern  statetype s_bjmutantchase1s;
extern  statetype s_bjmutantchase2;
extern  statetype s_bjmutantchase3;
extern  statetype s_bjmutantchase3s;
extern  statetype s_bjmutantchase4;

extern  statetype s_bjmutantdogchase1;
extern  statetype s_bjmutantdogchase1s;
extern  statetype s_bjmutantdogchase2;
extern  statetype s_bjmutantdogchase3;
extern  statetype s_bjmutantdogchase3s;
extern  statetype s_bjmutantdogchase4;

extern  statetype s_bjmutantdie1;
extern  statetype s_bjmutantdie2;
extern  statetype s_bjmutantdie3;
extern  statetype s_bjmutantdie4;

extern statetype s_bjmutantpath1;
extern statetype s_bjmutantpath1s;
extern statetype s_bjmutantpath2;
extern statetype s_bjmutantpath3;
extern statetype s_bjmutantpath3s;
extern statetype s_bjmutantpath4;

extern statetype s_bjmutantmelee1;
extern statetype s_bjmutantmelee2;
extern statetype s_bjmutantmelee3;
extern statetype s_bjmutantmelee4;
extern statetype s_bjmutantmelee5;

statetype s_bjmutantstand             = {false,SPR_BJ_STAND,0,(statefunc)T_Stand,NULL,&s_bjmutantstand};

statetype s_bjmutantpain              = {false,SPR_BJ_PAIN_1,6,NULL,NULL,&s_bjmutantchase1};
statetype s_bjmutantpain1             = {false,SPR_BJ_PAIN_2,6,NULL,NULL,&s_bjmutantchase1};

statetype s_bjmutantchaingunshoot1            = {false,SPR_BJ_CHAINATK1,20,NULL,NULL,&s_bjmutantchaingunshoot2};
statetype s_bjmutantchaingunshoot2            = {false,SPR_BJ_CHAINATK2,20,NULL,(statefunc)T_Shoot,&s_bjmutantchaingunshoot3};
statetype s_bjmutantchaingunshoot3            = {false,SPR_BJ_CHAINATK3,4,NULL,NULL,&s_bjmutantchaingunshoot4};
statetype s_bjmutantchaingunshoot4            = {false,SPR_BJ_CHAINATK2,4,NULL,(statefunc)T_Shoot,&s_bjmutantchaingunshoot5};
statetype s_bjmutantchaingunshoot5            = {false,SPR_BJ_CHAINATK3,4,NULL,NULL,&s_bjmutantchaingunshoot6};
statetype s_bjmutantchaingunshoot6            = {false,SPR_BJ_CHAINATK2,4,NULL,(statefunc)T_Shoot,&s_bjmutantchaingunshoot7};
statetype s_bjmutantchaingunshoot7            = {false,SPR_BJ_CHAINATK3,4,NULL,NULL,&s_bjmutantchaingunshoot8};
statetype s_bjmutantchaingunshoot8            = {false,SPR_BJ_CHAINATK2,4,NULL,(statefunc)T_Shoot,&s_bjmutantchaingunshoot9};
statetype s_bjmutantchaingunshoot9            = {false,SPR_BJ_CHAINATK3,4,NULL,NULL,&s_bjmutantchase1};

statetype s_bjmutantmachinegunshoot1            = {false,SPR_BJ_MACHINEGUNATK1,20,NULL,NULL,&s_bjmutantmachinegunshoot2};
statetype s_bjmutantmachinegunshoot2            = {false,SPR_BJ_MACHINEGUNATK2,20,NULL,(statefunc)T_Shoot,&s_bjmutantmachinegunshoot3};
statetype s_bjmutantmachinegunshoot3            = {false,SPR_BJ_MACHINEGUNATK3,10,NULL,NULL,&s_bjmutantmachinegunshoot4};
statetype s_bjmutantmachinegunshoot4            = {false,SPR_BJ_MACHINEGUNATK2,10,NULL,(statefunc)T_Shoot,&s_bjmutantmachinegunshoot5};
statetype s_bjmutantmachinegunshoot5            = {false,SPR_BJ_MACHINEGUNATK3,10,NULL,NULL,&s_bjmutantmachinegunshoot6};
statetype s_bjmutantmachinegunshoot6            = {false,SPR_BJ_MACHINEGUNATK2,10,NULL,(statefunc)T_Shoot,&s_bjmutantmachinegunshoot7};
statetype s_bjmutantmachinegunshoot7            = {false,SPR_BJ_MACHINEGUNATK3,10,NULL,NULL,&s_bjmutantmachinegunshoot8};
statetype s_bjmutantmachinegunshoot8            = {false,SPR_BJ_MACHINEGUNATK2,10,NULL,(statefunc)T_Shoot,&s_bjmutantmachinegunshoot9};
statetype s_bjmutantmachinegunshoot9            = {false,SPR_BJ_MACHINEGUNATK3,10,NULL,NULL,&s_bjmutantchase1};

statetype s_bjmutantpistolshoot1            = {false,SPR_BJ_PISTOLATK1,20,NULL,NULL,&s_bjmutantpistolshoot2};
statetype s_bjmutantpistolshoot2            = {false,SPR_BJ_PISTOLATK2,20,NULL,(statefunc)T_Shoot,&s_bjmutantpistolshoot3};
statetype s_bjmutantpistolshoot3            = {false,SPR_BJ_PISTOLATK3,10,NULL,NULL,&s_bjmutantpistolshoot4};
statetype s_bjmutantpistolshoot4            = {false,SPR_BJ_PISTOLATK2,10,NULL,(statefunc)T_Shoot,&s_bjmutantpistolshoot5};
statetype s_bjmutantpistolshoot5            = {false,SPR_BJ_PISTOLATK3,10,NULL,NULL,&s_bjmutantchase1};

statetype s_bjmutantchase1            = {false,SPR_BJ_M1,10,(statefunc)T_Chase,NULL,&s_bjmutantchase1s};
statetype s_bjmutantchase1s           = {false,SPR_BJ_M1,3,NULL,NULL,&s_bjmutantchase2};
statetype s_bjmutantchase2            = {false,SPR_BJ_M2,8,(statefunc)T_Chase,NULL,&s_bjmutantchase3};
statetype s_bjmutantchase3            = {false,SPR_BJ_M3,10,(statefunc)T_Chase,NULL,&s_bjmutantchase3s};
statetype s_bjmutantchase3s           = {false,SPR_BJ_M3,3,NULL,NULL,&s_bjmutantchase4};
statetype s_bjmutantchase4            = {false,SPR_BJ_M4,8,(statefunc)T_Chase,NULL,&s_bjmutantchase1};

statetype s_bjmutantdogchase1         = {false,SPR_BJ_M1,10,(statefunc)T_DogChase,NULL,&s_bjmutantdogchase1s};
statetype s_bjmutantdogchase1s        = {false,SPR_BJ_M1,3,NULL,NULL,&s_bjmutantdogchase2};
statetype s_bjmutantdogchase2         = {false,SPR_BJ_M2,8,(statefunc)T_DogChase,NULL,&s_bjmutantdogchase3};
statetype s_bjmutantdogchase3         = {false,SPR_BJ_M3,10,(statefunc)T_DogChase,NULL,&s_bjmutantdogchase3s};
statetype s_bjmutantdogchase3s        = {false,SPR_BJ_M3,3,NULL,NULL,&s_bjmutantdogchase4};
statetype s_bjmutantdogchase4         = {false,SPR_BJ_M4,8,(statefunc)T_DogChase,NULL,&s_bjmutantdogchase1};

statetype s_bjmutantdie1              = {false,SPR_BJ_DIE_1,15,NULL,(statefunc)A_DeathScream,&s_bjmutantdie2};
statetype s_bjmutantdie2              = {false,SPR_BJ_DIE_2,15,NULL,NULL,&s_bjmutantdie3};
statetype s_bjmutantdie3              = {false,SPR_BJ_DIE_3,15,NULL,NULL,&s_bjmutantdie4};
statetype s_bjmutantdie4              = {false,SPR_BJ_DEAD,0,NULL,NULL,&s_bjmutantdie4};

statetype s_bjmutantpath1             = {false,SPR_BJ_M1,20,(statefunc)T_Path,NULL,&s_bjmutantpath1s};
statetype s_bjmutantpath1s            = {false,SPR_BJ_M1,5,NULL,NULL,&s_bjmutantpath2};
statetype s_bjmutantpath2             = {false,SPR_BJ_M2,15,(statefunc)T_Path,NULL,&s_bjmutantpath3};
statetype s_bjmutantpath3             = {false,SPR_BJ_M3,20,(statefunc)T_Path,NULL,&s_bjmutantpath3s};
statetype s_bjmutantpath3s            = {false,SPR_BJ_M3,5,NULL,NULL,&s_bjmutantpath4};
statetype s_bjmutantpath4             = {false,SPR_BJ_M4,15,(statefunc)T_Path,NULL,&s_bjmutantpath1};

statetype s_bjmutantmelee1            = {false,SPR_BJ_KNIFEATK1,10,NULL,NULL,&s_bjmutantmelee2};
statetype s_bjmutantmelee2            = {false,SPR_BJ_KNIFEATK2,10,NULL,(statefunc)T_Bite,&s_bjmutantmelee3};
statetype s_bjmutantmelee3            = {false,SPR_BJ_KNIFEATK3,10,NULL,NULL,&s_bjmutantmelee4};
statetype s_bjmutantmelee4            = {false,SPR_BJ_KNIFEATK4,10,NULL,NULL,&s_bjmutantmelee5};
statetype s_bjmutantmelee5            = {false,SPR_BJ_M1,10,NULL,NULL,&s_bjmutantdogchase1};

void SpawnBjMutant(void)
{
    SpawnNewObj(player_mp->tilex, player_mp->tiley, &s_bjmutantstand);
    newobj->x = player_mp->x; newobj->y = player_mp->y;
    newobj->speed = SPDBJMUTANT;
    newobj->obclass = bjmutantobj;
    newobj->player_index = LWMP_GetInstance();
    newobj->hitpoints = starthitpoints[
        gamestate.difficulty][en_bjmutant];
    newobj->flags |= FL_SHOOTABLE;
    newobj->findnewtarget_tics = FINDNEWTARGET_TIMEOUT;
    PL_FillBackpack(&newobj->bjMutantBackpack);
    newobj->bjMutantWeapon = (gamestate.weapon_mp == wp_knife ? 
        wp_pistol : gamestate.weapon_mp);
    newobj->bjMutantChaseFactor = 3;

    assert(player_mp->angle >= 0 && player_mp->angle < ANGLES);
    newobj->dir = (dirtype)((player_mp->angle + (ANGLEOCT / 2)) /
        ANGLEOCT);
}

void BjMutant_EnterShootState(objtype *ob)
{
    statetype *out[wp_max] = {
        &s_bjmutantmelee1, // wp_knife
        &s_bjmutantpistolshoot1, // wp_pistol
        &s_bjmutantmachinegunshoot1, // wp_machinegun
        &s_bjmutantchaingunshoot1, // wp_chaingun
    };
    NewState (ob, out[ob->bjMutantWeapon]);
}

void BjMutant_PlayShootSound(objtype *ob)
{
    soundnames out[wp_max] = {
        ATKKNIFESND, // wp_knife
        ATKPISTOLSND, // wp_pistol
        ATKMACHINEGUNSND, // wp_machinegun
        ATKGATLINGSND, // wp_chaingun
        ATKROCKETLAUNCHERSND, // wp_rocketlauncher
        ATKFLAMETHROWERSND, // wp_flamethrower
    };
    PlaySoundLocActor(out[ob->bjMutantWeapon], ob);
}


/*
============================================================================

                                  GIB

============================================================================
*/

namespace Gib
{
    typedef std::pair<int32_t, unsigned> DamageStep;
    typedef std::vector<DamageStep> DamageHistory;
    typedef std::map<int, DamageHistory> DamageHistoryMap;

    namespace Constants
    {
        const int instantLimit = 400;
    }

    double extremeDamageRate(objtype *ob)
    {
        return 
            (
                ZombieMode::enabled() ||
                ZombieHarvesterMode::wasAlive(ob)
            ) ? 800.0 : 1400.0;
    }

    static DamageHistoryMap damageHistoryMap;

    void exitPainHold(objtype *ob)
    {
        if (inPainHold(ob))
        {
            //std::cerr << "exit pain hold" << std::endl;
            ob->flags &= ~FL_PAINHOLD;
        }
    }

    DamageHistory &getDamageHistory(objtype *ob)
    {
        return damageHistoryMap[ob - objlist];
    }

    double damageRate(objtype *ob)
    {
        DamageHistory &damageHistory = getDamageHistory(ob);

        unsigned totalDamage = 0;
        int32_t ticsSoFar = 0;
        double maxRate = 0.0;

        for (DamageHistory::size_type i = 0; i < damageHistory.size(); i++)
        {
            const DamageStep &step = 
                damageHistory[damageHistory.size() - i - 1];
            totalDamage += step.second;

            if (ticsSoFar > 0)
            {
                const double secs = TICS2SEC(ticsSoFar);
                const double rate = (double)totalDamage / secs;
                maxRate = std::max(rate, maxRate);
            }

            if (i < damageHistory.size() - 1)
            {
                const DamageStep &prevStep = 
                    damageHistory[damageHistory.size() - i - 2];
                ticsSoFar += std::max(step.first - prevStep.first, (int32_t)0);
            }
        }

        return maxRate;
    }

    unsigned totalDamage(objtype *ob)
    {
        DamageHistory &damageHistory = getDamageHistory(ob);

        unsigned totalDamage = 0;
        for (DamageHistory::size_type i = 0; i < damageHistory.size(); i++)
        {
            totalDamage += damageHistory[i].second;
        }
        
        return totalDamage;
    }

    bool gotInstantGib(objtype *ob, unsigned damage)
    {
        //std::cerr << "inst " << damage << std::endl;
        return ob->hitpoints < 0 && damage >= Constants::instantLimit;
    }

    void takeDamage(objtype *ob, unsigned damage)
    {
        DamageHistory &damageHistory = getDamageHistory(ob);

        if (gotInstantGib(ob, damage))
        {
            damageHistory.clear();
            return;
        }

        if (!inPainHold(ob))
        {
            //std::cerr << "enter pain hold" << std::endl;
            ob->flags |= FL_PAINHOLD;
            ob->painHoldStartTicTime = gamestate.TimeCount;
            damageHistory.clear();
        }

        const DamageStep damageStep = 
            std::make_pair(gamestate.TimeCount, damage);
        damageHistory.push_back(damageStep);
    }

    bool extremeDamage(objtype *ob)
    {
        if (InstagibMode::enabled())
        {
            return true;
        }

        const double minDamagePerSec = extremeDamageRate(ob);
        //const unsigned minTotalDamage = ob->starthitpoints * 2;
        //std::cerr <<
        //    "rate " << damageRate(ob) << "/" << minDamagePerSec <<
        //    //" damage " << totalDamage(ob) << "/" << minTotalDamage <<
        //    std::endl;
        return inPainHold(ob) && damageRate(ob) >= minDamagePerSec/* &&
            totalDamage(ob) >= minTotalDamage*/;
    }

    bool inPainHold(objtype *ob)
    {
        return (ob->flags & FL_PAINHOLD) != 0;
    }

    bool painHoldExpired(objtype *ob)
    {
        const double maxPainHoldDuration = std::min(
            0.2 * ob->starthitpoints / 25, 0.3);

        if (inPainHold(ob))
        {
            const int32_t curTicTime = gamestate.TimeCount;
            int32_t &startTicTime = ob->painHoldStartTicTime;

            if (curTicTime > startTicTime)
            {
                const int32_t painTics = curTicTime - startTicTime;
                const double painSecs = TICS2SEC(painTics);
                //std::cerr << "painSecs " << painSecs << std::endl;
                if (painSecs >= maxPainHoldDuration)
                {
                    return true;
                }
            }
            else
            {
                startTicTime = curTicTime;
            }
        }

        return false;
    }
}


/*
============================================================================

                                  TANK

============================================================================
*/

namespace Tank
{
    statetype s_idle              = {false,SPR_TANK,0,NULL,NULL,&s_idle};
    statetype s_nogun             = {false,SPR_TANK_NOGUN,0,NULL,NULL,&s_nogun};

    static const short starthitpoints = 100;
    static const int maxAmmo = 200;

    static std::vector<objtype *> hitObjs;

    namespace Passenger
    {
        enum e
        {
            driver,
            gunner,
        };
    }

    namespace GameState
    {
        typedef struct
        {
            bool isPassenger;
            Passenger::e passenger;
        } Player;

        typedef struct
        {
            short ammo;
            int objNum;
            short angle;
            double angleFrac;
            int motionObjNum;
            double thrust;
            int controlYAccum;
        } Tank;

        typedef struct
        {
            Player LWMP_DECL(players);
            Tank tank;
        } Data;

        Data &getData(void)
        {
            assert(sizeof(Data) <= sizeof(gamestate.tankdata));
            Data *data = (Data *)(&gamestate.tankdata[0]);
            return *data;
        }

        Player &getPlayer(objtype *ob)
        {
            return getData().players[ObjectPlayerIndex(ob)];
        }

        Tank &getTank(void)
        {
            return getData().tank;
        }

        void reset(void)
        {
            Data &data = getData();
            memset(&data, 0, sizeof(data));
        }
    }

    namespace Motion
    {
        void idle(objtype *ob);
        void accelfwd(objtype *ob);
        void movingfwd(objtype *ob);
        void slowfwd(objtype *ob);
        void accelbwd(objtype *ob);
        void movingbwd(objtype *ob);
        void slowbwd(objtype *ob);
        void fastaccelfwd(objtype *ob);
        void fastmovingfwd(objtype *ob);
        void fastslowfwd(objtype *ob);

        namespace ObjSnd
        {
            enum e
            {
                idle,
                moving,
                ignition,
                fastMoving,
                rev,
            };
        }

        namespace ControlYAccum
        {
            namespace Limits
            {
                enum e
                {
                    accum = 30,
                    dirThres = 20,
                    accumDrop = (dirThres / 5),
                };
            }

            int update(void)
            {
                GameState::Tank &gsTank = GameState::getTank();

                int &accum = gsTank.controlYAccum;
                const int lim = Limits::accum;

                objtype *driverObj = Tank::getDriverObj();
                if (!driverObj)
                {
                    return 0;
                }

                int c;
                for (LWMP_REPEAT_ONCE(c, driverObj - objlist))
                {
                    accum += controly_mp;
                }

                accum = std::min(accum, lim);
                accum = std::max(accum, -lim);

                const int dirThres = Limits::dirThres;
                const int accumDrop = Limits::accumDrop;

                if (accum >= dirThres)
                {
                    accum = std::max(accum - accumDrop, 0);
                    return 1;
                }
                else if (accum <= -dirThres)
                {
                    accum = std::min(accum + accumDrop, 0);
                    return -1;
                }
                return 0;
            }
        }

        statetype s_idle = {false,SPR_DEMO,0,(statefunc)idle,NULL,&s_idle};
        statetype s_accelfwd = {false,SPR_DEMO,0,(statefunc)accelfwd,NULL,&s_accelfwd};
        statetype s_movingfwd = {false,SPR_DEMO,0,(statefunc)movingfwd,NULL,&s_movingfwd};
        statetype s_slowfwd = {false,SPR_DEMO,0,(statefunc)slowfwd,NULL,&s_slowfwd};
        statetype s_accelbwd = {false,SPR_DEMO,0,(statefunc)accelbwd,NULL,&s_accelbwd};
        statetype s_movingbwd = {false,SPR_DEMO,0,(statefunc)movingbwd,NULL,&s_movingbwd};
        statetype s_slowbwd = {false,SPR_DEMO,0,(statefunc)slowbwd,NULL,&s_slowbwd};
        statetype s_fastaccelfwd = {false,SPR_DEMO,0,(statefunc)fastaccelfwd,NULL,&s_fastaccelfwd};
        statetype s_fastmovingfwd = {false,SPR_DEMO,0,(statefunc)fastmovingfwd,NULL,&s_fastmovingfwd};
        statetype s_fastslowfwd = {false,SPR_DEMO,0,(statefunc)fastslowfwd,NULL,&s_fastslowfwd};

        objtype *spawn(lwlib::Point2f pos)
        {
            SpawnNewObj (0,0,&s_idle);
            newobj->obclass = inertobj;
            newobj->flags |= FL_IMMUNE;
            Object::setPos(newobj, pos);
            return newobj;
        }

        void setActive(objtype *ob, activetype active)
        {
            ob->active = active;
            if (active == ac_no)
            {
                Object_StopSoundPosChannel(ob, -1);
                NewState(ob, &s_idle);

                GameState::Tank &gsTank = GameState::getTank();
                gsTank.thrust = 0.0;
                gsTank.controlYAccum = 0;
            }
        }

        bool sndPlaying(objtype *ob, ObjSnd::e objSnd)
        {
            return Object_SoundPlaying(ob, ob->soundPosChannel[objSnd]);
        }

        soundnames getSoundName(ObjSnd::e objSnd)
        {
            soundnames snd = HITWALLSND;

            switch (objSnd)
            {
            case ObjSnd::idle:
                snd = TANKIDLESND;
                break;
            case ObjSnd::moving:
                snd = TANKMOVINGSND;
                break;
            case ObjSnd::ignition:
                snd = TANKIGNITIONSND;
                break;
            case ObjSnd::fastMoving:
                snd = TANKFASTMOVINGSND;
                break;
            case ObjSnd::rev:
                snd = TANKREVSND;
                break;
            default:
                Quit("unknown tank object sound\n");
                break;
            }

            return snd;
        }

        void fadeOutSnd(objtype *ob, ObjSnd::e objSnd)
        {
            Object_FadeOutSoundPosChannel(ob, ob->soundPosChannel[objSnd]);
        }

        void fadeInSnd(objtype *ob, ObjSnd::e objSnd)
        {
            const soundnames snd = getSoundName(objSnd);
            ob->soundPosChannel[objSnd] = PlaySoundLocActor(snd, ob);
        }

        void playSnd(objtype *ob, ObjSnd::e objSnd)
        {
            ob->soundPosChannel[objSnd] =
                PlaySoundLocActor(getSoundName(objSnd), ob);
        }

        bool tryCharge(objtype *ob)
        {
            if (buttonstate_mp[bt_attack])
            {
                NewState(ob, &s_fastaccelfwd);
                fadeOutSnd(ob, ObjSnd::moving);
                fadeInSnd(ob, ObjSnd::fastMoving);
                playSnd(ob, ObjSnd::rev);
                return true;
            }

            return false;
        }

        void playIgnitionSnd(objtype *ob)
        {
            fadeInSnd(ob, ObjSnd::ignition);
        }

        void idle(objtype *ob)
        {
            if (sndPlaying(ob, ObjSnd::ignition))
            {
                return;
            }

            const int dir = ControlYAccum::update();
            if (dir < 0)
            {
                NewState(ob, &s_accelfwd);
                fadeOutSnd(ob, ObjSnd::idle);
                fadeInSnd(ob, ObjSnd::moving);
            }
            else if (dir > 0)
            {
                NewState(ob, &s_accelbwd);
                fadeOutSnd(ob, ObjSnd::idle);
                fadeInSnd(ob, ObjSnd::moving);
            }
        }

        void accelfwd(objtype *ob)
        {
            const double accel = 0.2;
            const double topSpeed = 0.05;
            GameState::Tank &gsTank = GameState::getTank();
            gsTank.thrust += TICS2SEC(tics) * accel;
            if (gsTank.thrust > topSpeed)
            {
                gsTank.thrust = topSpeed;
                NewState(ob, &s_movingfwd);
            }
        }

        void movingfwd(objtype *ob)
        {
            const int dir = ControlYAccum::update();
            if (tryCharge(ob))
            {
            }
            else if (dir >= 0)
            {
                NewState(ob, &s_slowfwd);
                fadeOutSnd(ob, ObjSnd::moving);
                fadeInSnd(ob, ObjSnd::idle);
            }
        }

        void slowfwd(objtype *ob)
        {
            const double decel = 0.2;
            GameState::Tank &gsTank = GameState::getTank();
            gsTank.thrust -= TICS2SEC(tics) * decel;
            if (gsTank.thrust < 0)
            {
                gsTank.thrust = 0;
                NewState(ob, &s_idle);
            }
        }

        void accelbwd(objtype *ob)
        {
            const double accel = 0.1;
            const double topSpeed = 0.03;
            GameState::Tank &gsTank = GameState::getTank();
            gsTank.thrust -= TICS2SEC(tics) * accel;
            if (gsTank.thrust < -topSpeed)
            {
                gsTank.thrust = -topSpeed;
                NewState(ob, &s_movingbwd);
            }
        }

        void movingbwd(objtype *ob)
        {
            const int dir = ControlYAccum::update();
            if (dir <= 0)
            {
                NewState(ob, &s_slowbwd);
                fadeOutSnd(ob, ObjSnd::moving);
                fadeInSnd(ob, ObjSnd::idle);
            }
        }

        void slowbwd(objtype *ob)
        {
            const double decel = 0.1;
            GameState::Tank &gsTank = GameState::getTank();
            gsTank.thrust += TICS2SEC(tics) * decel;
            if (gsTank.thrust > 0)
            {
                gsTank.thrust = 0;
                NewState(ob, &s_idle);
            }
        }

        void fastaccelfwd(objtype *ob)
        {
            const double accel = 0.8;
            const double topSpeed = 0.2;
            GameState::Tank &gsTank = GameState::getTank();
            gsTank.thrust += TICS2SEC(tics) * accel;
            if (gsTank.thrust > topSpeed)
            {
                gsTank.thrust = topSpeed;
                NewState(ob, &s_fastmovingfwd);
            }
        }

        void fastmovingfwd(objtype *ob)
        {
            if (!buttonstate_mp[bt_attack])
            {
                NewState(ob, &s_fastslowfwd);
                fadeOutSnd(ob, ObjSnd::fastMoving);
                fadeInSnd(ob, ObjSnd::moving);
            }
        }

        void fastslowfwd(objtype *ob)
        {
            const double decel = 0.4;
            const double topSpeed = 0.05;
            GameState::Tank &gsTank = GameState::getTank();
            gsTank.thrust -= TICS2SEC(tics) * decel;
            if (gsTank.thrust < topSpeed)
            {
                gsTank.thrust = topSpeed;
                NewState(ob, &s_movingfwd);
            }
        }
    }

    namespace TurnConstants
    {
        enum e
        {
            ang1 = 3,
            ang2 = 45,
            ang3 = 50,
            recentreControlX = 10,
            fastRecentreControlX = 30,
        };
    }

    bool objIsPassenger(objtype *ob)
    {
        GameState::Player &gsPlayer = GameState::getPlayer(ob);
        return gsPlayer.isPassenger;
    }

    bool objIsDriver(objtype *ob)
    {
        GameState::Player &gsPlayer = GameState::getPlayer(ob);
        return gsPlayer.isPassenger &&
            gsPlayer.passenger == Passenger::driver;
    }

    bool objIsGunner(objtype *ob)
    {
        GameState::Player &gsPlayer = GameState::getPlayer(ob);
        return gsPlayer.isPassenger &&
            gsPlayer.passenger == Passenger::gunner;
    }

    objtype *getTankObj(void)
    {
        GameState::Tank &gsTank = GameState::getTank();
        if (gsTank.objNum != 0)
        {
            return objlist + gsTank.objNum;
        }
        return NULL;
    }

    objtype *getDriverObj(void)
    {
        objtype *check;
        for (check = objlist; ObjectIsPlayer(check) &&
            !objIsDriver(check); check = check->next);
        return ObjectIsPlayer(check) ? check : NULL;
    }

    objtype *getGunnerObj(void)
    {
        objtype *check;
        for (check = objlist; ObjectIsPlayer(check) &&
            !objIsGunner(check); check = check->next);
        return ObjectIsPlayer(check) ? check : NULL;
    }

    void turnParms(double &alignVel, int &weaponframe)
    {
        GameState::Tank &gsTank = GameState::getTank();
        const double angleBw = player_mp->angle;

        const double ind1Angle = TurnConstants::ang1;
        const double ind2Angle = TurnConstants::ang2;

        alignVel = 0;
        weaponframe = 0;
        if (angleBw > ind2Angle)
        {
            alignVel = ANGLEQUAD * 1.0;
            weaponframe = 2;
        }
        else if (angleBw > ind1Angle)
        {
            alignVel = ANGLEQUAD * 0.5;
            weaponframe = 1;
        }
        else if (angleBw < -ind2Angle)
        {
            alignVel = -ANGLEQUAD * 1.0;
            weaponframe = 4;
        }
        else if (angleBw < -ind1Angle)
        {
            alignVel = -ANGLEQUAD * 0.5;
            weaponframe = 3;
        }
    }

    void driverTurn(objtype *ob, int controlx)
    {
        const int angleScale = 20;

        anglefrac_mp += controlx;
        const int angleunits = anglefrac_mp/angleScale;
        anglefrac_mp -= angleunits*angleScale;
        ob->angle -= angleunits;

        const int maxAng = TurnConstants::ang3;

        if (ob->angle > maxAng)
            ob->angle = maxAng;
        if (ob->angle < -maxAng)
            ob->angle = -maxAng;
    }

    void explodeEnemies(objtype *ob)
    {
        int c;

        for (int i = 0; i < hitObjs.size(); i++)
        {
            objtype *hitObj = hitObjs[i];

            if (LWMP_ObjectIsGibActor(hitObj))
            {
                const int damage = Gib::Constants::instantLimit;

                if (ObjectIsPlayer(hitObj))
                {
                    for (LWMP_REPEAT_ONCE(c, ObjectPlayerIndex(hitObj)))
                    {
                        TakeDamage (damage, ob);
                    }
                }
                else
                {
                    DamageActor (hitObj, damage);
                }
            }
        }
    }

    void addHitObj(objtype *hitObj)
    {
        hitObjs.push_back(hitObj);
    }

    void controlMovementDriver(objtype *ob)
    {
        int     angle;
        int     angleunits;
        vec3fixed_t playeranglevec;

        thrustspeed_mp = 0;
        thrustvector_mp = vec3fixed_zero();

        driverTurn(ob, controlx_mp);

        GameState::Tank &gsTank = GameState::getTank();
        const short tankAngle = gsTank.angle;

        const double thrust = gsTank.thrust;

        if (thrust > 0)
        {
            hitObjs.clear();

            Thrust (tankAngle,fixedpt_rconst(thrust));

            if (thrust > 0.15)
            {
                explodeEnemies(ob);
            }
        }
        else if (thrust < 0)
        {
            angle = tankAngle + ANGLES/2;
            if (angle >= ANGLES)
                angle -= ANGLES;
            Thrust (angle,fixedpt_rconst(-thrust));
        }

        playeranglevec = vec3fixed_anglevec(tankAngle);
        if (vec3fixed_dot(thrustvector_mp, playeranglevec) < 0)
        {
            player_mp->run_dist -= thrustspeed_mp;
            while (player_mp->run_dist < 0)
            {
                player_mp->run_dist += FP(256);
            }
        }
        else
        {
            player_mp->run_dist += thrustspeed_mp;
            while (player_mp->run_dist >= FP(256))
            {
                player_mp->run_dist -= FP(256);
            }
        }

        if (thrustspeed_mp == 0)
        {
            if (gamestate.moveanimtics_mp == 0)
            {
                // reset running distance when BJ stops moving
                ob->run_dist = 0;
            }
        }
        else
        {
            gamestate.moveanimtics_mp = 15;
        }

        if (gamestate.moveanimtics_mp != 0)
        {
            gamestate.moveanimtics_mp = std::max(
                gamestate.moveanimtics_mp - (int32_t)tics, 0);
        }

        const lwlib::Vector3f thrustVec = 
            lwlib::vec3f(
                fixedpt_toconst(thrustvector_mp.v[0]),
                fixedpt_toconst(thrustvector_mp.v[1]),
                fixedpt_toconst(thrustvector_mp.v[2])
                );
        const lwlib::Vector3f playerVec = 
            lwlib::vec3f(
                fixedpt_toconst(playeranglevec.v[0]),
                fixedpt_toconst(playeranglevec.v[1]),
                fixedpt_toconst(playeranglevec.v[2])
                );
        const double thrustVel = vec_dot(vec_normalize(thrustVec),
            playerVec);
        if (thrustVel != 0)
        {
            double alignVel = 0;
            int weaponframe = 0;
            turnParms(alignVel, weaponframe);

            const double angleInc = TICS2SEC(tics) * alignVel;
            gsTank.angleFrac = lwlib::normalize_angle(gsTank.angleFrac +
                angleInc * lwlib_Sign(thrustVel));
            gsTank.angle = (short)gsTank.angleFrac;
        }
        {
            using namespace TurnConstants;
            const int recentre = (controlx_mp == 0 ?
                fastRecentreControlX : recentreControlX);
            driverTurn(ob, lwlib_SignInt(ob->angle) * recentre);
        }

        {
            objtype *tankObj = getTankObj();
            Object::setPos(tankObj, Object::getPos(ob));
        }

        {
            objtype *motionObj = objlist + gsTank.motionObjNum;
            Object::setPos(motionObj, Object::getPos(ob));
        }
    }

    void controlMovementGunner(objtype *ob)
    {
        thrustspeed_mp = 0;
        thrustvector_mp = vec3fixed_zero();

        TurnAim(ob, controlx_mp);

        {
            objtype *tankObj = getTankObj();
            ob->x = tankObj->x;
            ob->y = tankObj->y;

            Thrust(0, 0);
        }
    }

    void controlMovement(objtype *ob)
    {
        if (objIsGunner(ob))
        {
            controlMovementGunner(ob);
        }
        else if (objIsDriver(ob))
        {
            controlMovementDriver(ob);
        }
    }

    void reset(void)
    {
        GameState::reset();
    }

    void spawn(int tilex, int tiley)
    {
        SpawnNewObj (tilex,tiley,&s_idle);
        newobj->obclass = tankobj;
        newobj->hitpoints = starthitpoints;
        newobj->dir = nodir;
        newobj->flags |= FL_SHOOTABLE;
        const int tankObjNum = newobj - objlist;

        GameState::Tank &gsTank = GameState::getTank();
        gsTank.ammo = maxAmmo;
        gsTank.objNum = tankObjNum;
        gsTank.motionObjNum =
            Motion::spawn(Object::getPos(newobj)) - objlist;
    }

    void die(objtype *ob)
    {
        //NewState (ob,&s_die1);
    }

    void test(void)
    {
        spawn(31, 58);
    }

    bool checkType(objtype *ob)
    {
        return ob != NULL && ob->obclass == tankobj;
    }

    bool passengerExists(void)
    {
        for (int i = 0; i < LWMP_NUM_PLAYERS; i++)
        {
            objtype *playerObj = player[i];

            GameState::Player &gsPlayer = GameState::getPlayer(playerObj);
            if (gsPlayer.isPassenger)
            {
                return true;
            }
        }

        return false;
    }

    bool driverExists(void)
    {
        for (int i = 0; i < LWMP_NUM_PLAYERS; i++)
        {
            objtype *playerObj = player[i];

            GameState::Player &gsPlayer = GameState::getPlayer(playerObj);
            if (gsPlayer.isPassenger && gsPlayer.passenger == Passenger::driver)
            {
                return true;
            }
        }

        return false;
    }

    bool checkUse(objtype *&ob)
    {
        objtype *tankObj = getTankObj();
        if (tankObj != NULL && !objIsPassenger(player_mp))
        {
            objtype *closest = NULL;
            int32_t dist = 0x7fffffff;

            objtype *check = tankObj;
            if (abs(check->viewx_mp - centerx_mp) < shootdelta_mp)
            {
                if (check->transx_mp < dist)
                {
                    dist = check->transx_mp;
                    closest = check;
                }
            }

            if (!closest || dist > 0x18000l)
            {
                return false;
            }

            ob = closest;
            return true;
        }

        return false;
    }

    void use(objtype *ob)
    {
        if (driverExists())
        {
            objtype *playerObj = player_mp;
            GameState::Player &gsPlayer = GameState::getPlayer(playerObj);
            gsPlayer.isPassenger = true;
            gsPlayer.passenger = Passenger::gunner;

            player_mp->x = ob->x;
            player_mp->y = ob->y;
            player_mp->flags &= ~FL_SHOOTABLE;

            player_mp->angle = 0;

            Thrust(0, 0);

            SD_PlaySound(TANKUSEGUNSND);
        }
        else
        {
            objtype *playerObj = player_mp;
            GameState::Player &gsPlayer = GameState::getPlayer(playerObj);
            gsPlayer.isPassenger = true;
            gsPlayer.passenger = Passenger::driver;

            player_mp->x = ob->x;
            player_mp->y = ob->y;

            Thrust(0, 0);

            ob->flags &= ~FL_SHOOTABLE;
            ob->flags |= FL_NODRAW|FL_NEVERMARK;
            actorat[ob->tilex][ob->tiley] = NULL;

            NewState(ob, &s_idle);

            GameState::Tank &gsTank = GameState::getTank();
            gsTank.angleFrac = ob->angle;
            gsTank.angle = (short)gsTank.angleFrac;

            player_mp->angle = 0;

            objtype *motionObj = objlist + gsTank.motionObjNum;
            Motion::setActive(motionObj, ac_yes);

            Motion::playIgnitionSnd(motionObj);
            SD_PlaySound(TANKUSEWHEELSND);

            {
                objtype *gunnerObj = getGunnerObj();
                if (gunnerObj)
                {
                    gunnerObj->flags &= ~FL_SHOOTABLE;
                }
            }
        }

        sb(DrawAmmo ());
        sb(DrawWeapon ());
    }

    bool findExitSpot(bool die)
    {
        objtype *playerObj = player_mp;

        const int32_t basex = player_mp->x;
        const int32_t basey = player_mp->y;
        const int32_t dist = MINACTORDIST * 5 / 4;

        lwlib::Point<int32_t, 2> v = lwlib::vec2i(dist, 0);

        const int rots = US_RndT() % 4;
        for (int i = 0; i < rots; i++)
        {
            v = vec_rot90(v);
        }

        for (int i = 0; i < 4; i++)
        {
            objtype ob = *playerObj;
            ob.x = basex + v.v[0];
            ob.y = basey + v.v[1];
            if (TryMove (&ob))
            {
                playerObj->x = ob.x;
                playerObj->y = ob.y;
                Thrust(0, 0);
                return true;
            }

            v = vec_rot90(v);
        }

        if (die)
        {
            playerObj->x = basex;
            playerObj->y = basey;
            Thrust(0, 0);
            return true;
        }

        return false;
    }

    void exit(bool die)
    {
        if (findExitSpot(die))
        {
            const bool playerWasDriver = objIsDriver(player_mp);

            objtype *playerObj = player_mp;
            GameState::Player &gsPlayer = GameState::getPlayer(playerObj);
            gsPlayer.isPassenger = false;

            player_mp->flags |= FL_SHOOTABLE;

            if (playerWasDriver)
            {
                objtype *tankObj = getTankObj();
                tankObj->flags |= FL_SHOOTABLE;
                tankObj->flags &= ~(FL_NODRAW|FL_NEVERMARK);

                GameState::Tank &gsTank = GameState::getTank();
                objtype *motionObj = objlist + gsTank.motionObjNum;
                Motion::setActive(motionObj, ac_no);

                if (getGunnerObj())
                {
                    NewState(tankObj, &s_nogun);

                    objtype *gunnerObj = getGunnerObj();
                    gunnerObj->flags |= FL_SHOOTABLE;
                }

                tankObj->angle = gsTank.angle;
            }
            else
            {
                if (!getDriverObj())
                {
                    objtype *tankObj = getTankObj();
                    tankObj->flags |= FL_SHOOTABLE;
                    tankObj->flags &= ~(FL_NODRAW|FL_NEVERMARK);
                }

                objtype *tankObj = getTankObj();
                NewState(tankObj, &s_idle);
            }
        }
    }

    weapontype weapon(void)
    {
        const weapontype weapon = (objIsDriver(player_mp) ?
            wp_tankwheel : wp_tankgun);
        return weapon;
    }

    short &ammo(void)
    {
        return GameState::getTank().ammo;
    }

    short displayAmmo(void)
    {
        return ammo() * 99 / maxAmmo;
    }

    void checkWeaponChange(void)
    {
        weapontype newWeapon = wp_tankmax;

        if(buttonstate_mp[bt_nextweapon] && !buttonheld_mp[bt_nextweapon])
        {
            newWeapon = (weapon() == wp_tankwheel ? wp_tankgun : wp_tankwheel);
        }
        else if(buttonstate_mp[bt_prevweapon] && !buttonheld_mp[bt_prevweapon])
        {
            newWeapon = (weapon() == wp_tankwheel ? wp_tankgun : wp_tankwheel);
        }
        else
        {
            if (buttonstate_mp[bt_readyknife])
            {
                newWeapon = wp_tankwheel;
            }
            else if (buttonstate_mp[bt_readypistol])
            {
                newWeapon = wp_tankgun;
            }
        }

        if(newWeapon != wp_tankmax && newWeapon != weapon())
        {
            objtype *playerObj = player_mp;
            GameState::Player &gsPlayer = GameState::getPlayer(playerObj);

            if (newWeapon == wp_tankgun && !getGunnerObj())
            {
                gsPlayer.passenger = Passenger::gunner;

                objtype *tankObj = getTankObj();
                tankObj->flags |= FL_SHOOTABLE;
                tankObj->flags &= ~(FL_NODRAW|FL_NEVERMARK);

                NewState(tankObj, &s_nogun);

                //player_mp->flags &= ~FL_SHOOTABLE;

                GameState::Tank &gsTank = GameState::getTank();
                objtype *motionObj = objlist + gsTank.motionObjNum;
                Motion::setActive(motionObj, ac_no);

                SD_PlaySound(TANKUSEGUNSND);
            }
            else if (newWeapon == wp_tankwheel && !getDriverObj())
            {
                gsPlayer.passenger = Passenger::driver;

                objtype *tankObj = getTankObj();
                tankObj->flags &= ~FL_SHOOTABLE;
                tankObj->flags |= FL_NODRAW|FL_NEVERMARK;

                NewState(tankObj, &s_idle);

                player_mp->flags |= FL_SHOOTABLE;

                GameState::Tank &gsTank = GameState::getTank();
                objtype *motionObj = objlist + gsTank.motionObjNum;
                Motion::setActive(motionObj, ac_yes);

                Motion::playIgnitionSnd(motionObj);
                SD_PlaySound(TANKUSEWHEELSND);
            }

            sb(DrawWeapon());
        }
    }

    fixed camZ(void)
    {
        return objIsGunner(player_mp) ? 0x2800 : 0x2000;
    }

    short viewAngle(objtype *ob)
    {
        GameState::Tank &gsTank = GameState::getTank();
        if (objIsDriver(ob))
        {
            return gsTank.angle;
        }
        return lwlib::normalize_angle(gsTank.angle + ob->angle);
    }

    int weaponShape(void)
    {
        double alignVel = 0;
        int weaponframe = 0;
        turnParms(alignVel, weaponframe);

        const int shapenum = weaponscale[wp_tankwheel] + weaponframe;
        return shapenum;
    }

    bool canSpawn(void)
    {
        return LWMP_NUM_PLAYERS >= 2 &&
            !InstagibMode::enabled() && !DefuseMode::enabled() &&
            !VampireMode::enabled() && !CtfMode::enabled() &&
            !RampageMode::enabled();
    }
}


/*
============================================================================

                                  MG42

============================================================================
*/

namespace Mg42
{
    statetype s_idle              = {false,SPR_MG42,0,NULL,NULL,&s_idle};

    static const int maxAmmo = 200;

    namespace GameState
    {
        typedef struct
        {
            bool isPassenger;
        } Player;

        typedef struct
        {
            short ammo;
            int objNum;
        } Mg42;

        typedef struct
        {
            Player LWMP_DECL(players);
            Mg42 mg42;
        } Data;

        Data &getData(void)
        {
            assert(sizeof(Data) <= sizeof(gamestate.mg42data));
            Data *data = (Data *)(&gamestate.mg42data[0]);
            return *data;
        }

        Player &getPlayer(objtype *ob)
        {
            return getData().players[ObjectPlayerIndex(ob)];
        }

        Mg42 &getMg42(void)
        {
            return getData().mg42;
        }

        void reset(void)
        {
            Data &data = getData();
            memset(&data, 0, sizeof(data));
        }
    }

    bool objIsPassenger(objtype *ob)
    {
        GameState::Player &gsPlayer = GameState::getPlayer(ob);
        return gsPlayer.isPassenger;
    }

    objtype *getMg42Obj(void)
    {
        GameState::Mg42 &gsMg42 = GameState::getMg42();
        if (gsMg42.objNum != 0)
        {
            return objlist + gsMg42.objNum;
        }
        return NULL;
    }

    void controlMovement(objtype *ob)
    {
        thrustspeed_mp = 0;
        thrustvector_mp = vec3fixed_zero();

        TurnAim(ob, controlx_mp);

        {
            objtype *mg42Obj = getMg42Obj();
            ob->x = mg42Obj->x;
            ob->y = mg42Obj->y;

            Thrust(0, 0);
        }
    }

    void reset(void)
    {
        GameState::reset();
    }

    void spawn(int tilex, int tiley)
    {
        SpawnNewObj (tilex,tiley,&s_idle);
        newobj->obclass = inertobj;
        newobj->hitpoints = 100;
        newobj->dir = east;
        newobj->flags |= FL_SHOOTABLE;
        const int mg42ObjNum = newobj - objlist;

        GameState::Mg42 &gsMg42 = GameState::getMg42();
        gsMg42.ammo = maxAmmo;
        gsMg42.objNum = mg42ObjNum;
    }

    void test(void)
    {
        spawn(31, 58);
    }

    bool passengerExists(void)
    {
        for (int i = 0; i < LWMP_NUM_PLAYERS; i++)
        {
            objtype *playerObj = player[i];

            GameState::Player &gsPlayer = GameState::getPlayer(playerObj);
            if (gsPlayer.isPassenger)
            {
                return true;
            }
        }

        return false;
    }

    bool checkUse(objtype *&ob)
    {
        objtype *mg42Obj = getMg42Obj();
        if (mg42Obj != NULL && !objIsPassenger(player_mp))
        {
            objtype *closest = NULL;
            int32_t dist = 0x7fffffff;

            objtype *check = mg42Obj;
            if (abs(check->viewx_mp - centerx_mp) < shootdelta_mp)
            {
                if (check->transx_mp < dist)
                {
                    dist = check->transx_mp;
                    closest = check;
                }
            }

            if (!closest || dist > 0x18000l)
            {
                return false;
            }

            ob = closest;
            return true;
        }

        return false;
    }

    void use(objtype *ob)
    {
        objtype *playerObj = player_mp;
        GameState::Player &gsPlayer = GameState::getPlayer(playerObj);
        gsPlayer.isPassenger = true;

        player_mp->x = ob->x;
        player_mp->y = ob->y;

        Thrust(0, 0);

        ob->flags &= ~FL_SHOOTABLE;
        ob->flags |= FL_NODRAW|FL_NEVERMARK;
        actorat[ob->tilex][ob->tiley] = NULL;

        sb(DrawAmmo ());
        sb(DrawWeapon ());

        SD_PlaySound(MG42USESND);
    }

    bool findExitSpot(bool die)
    {
        objtype *playerObj = player_mp;

        const int32_t basex = player_mp->x;
        const int32_t basey = player_mp->y;
        const int32_t dist = MINACTORDIST * 5 / 4;

        lwlib::Point<int32_t, 2> v = lwlib::vec2i(dist, 0);

        for (int i = 0; i < 4; i++)
        {
            objtype ob = *playerObj;
            ob.x = basex + v.v[0];
            ob.y = basey + v.v[1];
            if (TryMove (&ob))
            {
                playerObj->x = ob.x;
                playerObj->y = ob.y;
                Thrust(0, 0);
                return true;
            }

            v = vec_rot90(v);
        }

        if (die)
        {
            playerObj->x = basex;
            playerObj->y = basey;
            Thrust(0, 0);
            return true;
        }

        return false;
    }

    void exit(bool die)
    {
        if (findExitSpot(die))
        {
            objtype *playerObj = player_mp;
            GameState::Player &gsPlayer = GameState::getPlayer(playerObj);
            gsPlayer.isPassenger = false;

            player_mp->flags |= FL_SHOOTABLE;

            objtype *mg42Obj = getMg42Obj();
            mg42Obj->flags |= FL_SHOOTABLE;
            mg42Obj->flags &= ~(FL_NODRAW|FL_NEVERMARK);
        }
    }

    weapontype weapon(void)
    {
        const weapontype weapon = wp_mg42;
        return weapon;
    }

    short &ammo(void)
    {
        return GameState::getMg42().ammo;
    }

    short displayAmmo(void)
    {
        return ammo() * 99 / maxAmmo;
    }

    fixed camZ(void)
    {
        return 0;
    }

    bool canSpawn(void)
    {
        return LWMP_NUM_PLAYERS >= 2 &&
            !InstagibMode::enabled() && !DefuseMode::enabled() &&
            !VampireMode::enabled() && !CtfMode::enabled() &&
            !RampageMode::enabled();
    }
}


/*
============================================================================

                          SPAWN SPECIAL OBJECTS

============================================================================
*/

namespace SpawnSpecialObjects
{
    class Area
    {
    public:
        int hp;
        std::vector<lwlib::Point2i> spots;
        int areanumber;

        Area() : hp(0), areanumber(0)
        {
        }
    };

    typedef std::vector<Area> AreaVec;

    AreaVec areas;

    void reset(void)
    {
        areas.clear();
        areas.resize(NUMAREAS);
    }

    bool tileAvail(lwlib::Point2i pos)
    {
        int x, y;
        vec2_get(pos, x, y);
        if 
            (
                !(
                    MAPSPOT(x, y, 1) == 0 && 
                    tilemap[x][y] == 0 && 
                    MAPSPOT(x, y, 0) >= AREATILE &&
                    MAPSPOT(x, y, 0) < AREATILE + NUMAREAS
                )
            )
        {
            return false;
        }

        const word areaTile = MAPSPOT(x, y, 0);

        for (int i = 0; i < 9; i++)
        {
            const lwlib::Point2i posi =
                pos + lwlib::vec2i((i / 3) - 1, (i % 3) - 1);

            int x, y;
            vec2_get(posi, x, y);

            if 
                (
                    !(
                        x >= 0 && x < MAPSIZE && y >= 0 && y < MAPSIZE &&
                        MAPSPOT(x, y, 1) == 0 && tilemap[x][y] == 0 &&
                        MAPSPOT(x, y, 0) == areaTile
                    )
                )
            {
                return false;
            }
        }

        return true;
    }

    void enemyAt(int tilex, int tiley, int hp, int areanumber)
    {
        const lwlib::Point2i pos = lwlib::vec2i(tilex, tiley);

        if (areanumber >= 0 && areanumber < NUMAREAS)
        {
            Area &area = areas[areanumber];
            area.hp += hp;
            area.areanumber = areanumber;
        }
    }

    int genRandom(int i)
    {
        return std::rand() % i;
    }

    void scanPlane(void)
    {
        const lwlib::Point2i low = lwlib::vec2i_zero();
        const lwlib::Point2i high = low + lwlib::vec2i(MAPSIZE, MAPSIZE);

        lwlib::Point2i tilepos = low;
        do
        {
            if (tileAvail(tilepos))
            {
                int x, y;
                vec2_get(tilepos, x, y);

                const int areaNum = MAPSPOT(x, y, 0) - AREATILE;
                Area &area = areas[areaNum];
                area.spots.push_back(tilepos);
            }

            tilepos = vec_box_inc(low, high, tilepos);
        } while (tilepos != low);
    }

    void getDoorAreas(int door, int &area1, int &area2)
    {
        word *map;

        map = mapsegs[0] + (doorobjlist[door].tiley<<mapshift)
            +doorobjlist[door].tilex;

        if (doorobjlist[door].vertical)
        {
            area1 = *(map+1);
            area2 = *(map-1);
        }
        else
        {
            area1 = *(map-mapwidth);
            area2 = *(map+mapwidth);
        }
        area1 -= AREATILE;
        area2 -= AREATILE;
    }

    bool connectedToArea(int area1, int area2)
    {
        if (area1 == area2)
        {
            return false;
        }

        for (int door = 0; door < doornum; door++)
        {
            int a1, a2;
            getDoorAreas(door, a1, a2);

            if 
                (
                    a1 != a2 && 
                    (
                        (a1 == area1 && a2 == area2) ||
                        (a1 == area2 && a2 == area1)
                    )
                )
            {
                return true;
            }
        }

        return false;
    }

    void priConnectedToArea(AreaVec &areas, int areanumber)
    {
        AreaVec priAreas;

        AreaVec::iterator it = areas.begin();
        while (it != areas.end())
        {
            Area &area = *it;
            if (connectedToArea(area.areanumber, areanumber))
            {
                priAreas.push_back(area);
                it = areas.erase(it);
            }
            else
            {
                ++it;
            }
        }

        areas.insert(areas.begin(), priAreas.begin(), priAreas.end());
    }

    void go(void)
    {
        scanPlane();

        typedef void (*SpawnFn)(int x, int y);
        typedef int MinHp;
        typedef std::pair<SpawnFn, MinHp> SpecialSpawn;
        typedef std::vector<SpecialSpawn> SpecialSpawnVec;

        SpecialSpawnVec specialSpawns;

        if (DefuseMode::enabled())
        {
            specialSpawns.push_back(std::make_pair(&DefuseBomb::spawn, 0));
            //specialSpawns.push_back(std::make_pair(&DefuseBomb::spawn, 300));
            //specialSpawns.push_back(std::make_pair(&DefuseBomb::spawn, 300));
        }
        if (Tank::canSpawn())
        {
            specialSpawns.push_back(std::make_pair(&Tank::spawn, 300));
        }
        if (Mg42::canSpawn())
        {
            specialSpawns.push_back(std::make_pair(&Mg42::spawn, 300));
        }
        if (HarvesterMode::enabled())
        {
            int numHpAreas = 0;
            for (AreaVec::size_type i = 0; i < areas.size(); i++)
            {
                const Area &area = areas[i];
                if (area.hp != 0)
                {
                    numHpAreas++;
                }
            }

            std::map<int, int> numPillars;
            numPillars[gd_baby] = numHpAreas;
            numPillars[gd_easy] = numHpAreas;
            numPillars[gd_medium] = numHpAreas * 3 / 4;
            numPillars[gd_hard] = numHpAreas * 2 / 4;

            for (int i = 0; i < numPillars[gamestate.difficulty]; i++)
            {
                specialSpawns.push_back(
                    std::make_pair(&HarvesterMode::spawnPillar, 0));
            }
        }
        if (ZombieHarvesterMode::enabled())
        {
            int numHpAreas = 0;
            for (AreaVec::size_type i = 0; i < areas.size(); i++)
            {
                const Area &area = areas[i];
                if (area.hp != 0)
                {
                    numHpAreas++;
                }
            }

            std::map<int, int> numPillars;
            numPillars[gd_baby] = numHpAreas;
            numPillars[gd_easy] = numHpAreas;
            numPillars[gd_medium] = numHpAreas * 3 / 4;
            numPillars[gd_hard] = numHpAreas * 2 / 4;

            for (int i = 0; i < numPillars[gamestate.difficulty]; i++)
            {
                specialSpawns.push_back(
                    std::make_pair(&ZombieHarvesterMode::spawnPillar, 0));
            }
        }

        std::srand(unsigned(std::time(0)));
        std::random_shuffle(areas.begin(), areas.end(), genRandom);

        if (HarvesterMode::enabled() || ZombieHarvesterMode::enabled())
        {
            priConnectedToArea(areas, player[0]->areanumber);
        }

        for (AreaVec::size_type i = 0; i < areas.size(); i++)
        {
            const Area &area = areas[i];
            const std::vector<lwlib::Point2i> &spots = area.spots;

            if (area.hp == 0 || spots.empty() ||
                area.areanumber == player[0]->areanumber)
            {
                continue;
            }

            SpecialSpawnVec::iterator foundIt = specialSpawns.end();
            for (SpecialSpawnVec::iterator it = specialSpawns.begin();
                it != specialSpawns.end(); it++)
            {
                SpecialSpawn &specialSpawn = *it;

                if (area.hp >= it->second)
                {
                    foundIt = it;
                    break;
                }
            }

            if (foundIt != specialSpawns.end())
            {
                SpecialSpawn &specialSpawn = *foundIt;

                lwlib::Point2i spot = spots[genRandom(spots.size())];

                int x, y;
                vec2_get(spot, x, y);

                specialSpawn.first(x, y);
                specialSpawns.erase(foundIt);
            }
        }

        if (CtfMode::enabled())
        {
            CtfFlag::spawn();
        }
    }
}


/*
============================================================================

                            INSTAGIB MODE

============================================================================
*/

namespace InstagibMode
{
    bool enabled(void)
    {
        return gamestate.mode == GameMode::instagib;
    }

    void remapStatic(int &type)
    {
        if (enabled())
        {
            switch (statinfo[type].type)
            {
            case bo_alpo:
                type = spr_to_statinfo_type[SPR_STAT_29];
                break;
            case bo_food:
                type = spr_to_statinfo_type[SPR_STAT_30];
                break;
            case bo_firstaid:
                type = spr_to_statinfo_type[SPR_STAT_31];
                break;
            }
        }
    }
}


/*
============================================================================

                                DEFUSE BOMB

============================================================================
*/

namespace DefuseBomb
{
    void idle(objtype *ob);

    extern statetype s_idle1;
    extern statetype s_idle2;
    extern statetype s_defused;

    statetype s_idle1              = {false,SPR_DEFUSEBOMB_IDLE1,15,(statefunc)idle,NULL,&s_idle2};
    statetype s_idle2              = {false,SPR_DEFUSEBOMB_IDLE2,15,(statefunc)idle,NULL,&s_idle1};
    statetype s_defused            = {false,SPR_DEFUSEBOMB_DEFUSED,0,NULL,NULL,&s_defused};

    void spawn(int tilex, int tiley)
    {
        SpawnNewObj (tilex,tiley,&s_idle1);
        newobj->obclass = inertobj;
        //newobj->hitpoints = 100;
        newobj->dir = east;
        //newobj->flags |= FL_SHOOTABLE | FL_IMMUNE;
        newobj->active = ac_yes;
    }

    void test(void)
    {
        spawn(31, 58);
    }

    bool checkUse(objtype *ob)
    {
        objtype *closest = NULL;
        int32_t dist = 0x7fffffff;

        objtype *check = ob;
        if (abs(check->viewx_mp - centerx_mp) < shootdelta_mp)
        {
            if (check->transx_mp < dist)
            {
                dist = check->transx_mp;
                closest = check;
            }
        }

        if (!closest || dist > 0x18000l)
        {
            return false;
        }

        return true;
    }

    bool checkUseAny(objtype *ob)
    {
        int c;
        for (LWMP_REPEAT(c))
        {
            if (buttonstate_mp[bt_use] && checkUse(ob))
            {
                LWMP_ENDREPEAT(c);
                return true;
            }
        }
        return false;
    }
    
    void idle(objtype *ob)
    {
        int c;

        const int sec = gamestate.TimeCount / 70;
        if (sec >= ParTimes::curLevelSeconds())
        {
            playstate = ex_died;
            killerobj = ob;
        }

        if (checkUseAny(ob))
        {
            NewState(ob, &s_defused);
            DefuseMode::dispTimer() = false;
        }
    }
}


/*
============================================================================

                              DEFUSE MODE

============================================================================
*/

void LatchNumber (int x, int y, unsigned width, int32_t number, bool zeropad = false);

namespace DefuseMode
{
    namespace GameState
    {
        typedef struct
        {
            bool dispTimer;
        } Data;

        Data &getData(void)
        {
            assert(sizeof(Data) <= sizeof(gamestate.defusemodedata));
            Data *data = (Data *)(&gamestate.defusemodedata[0]);
            return *data;
        }

        void reset(void)
        {
            Data &data = getData();
            memset(&data, 0, sizeof(data));
            data.dispTimer = true;
        }
    }

    void reset(void)
    {
        GameState::reset();
    }

    bool enabled(void)
    {
        return gamestate.mode == GameMode::defuse;
    }

    bool &dispTimer(void)
    {
        return GameState::getData().dispTimer;
    }

    int timeLeft(void)
    {
        const int sec = gamestate.TimeCount / 70;
        const int lim = ParTimes::curLevelSeconds();
        return lim - sec;
    }

    void drawTimeLeft(void)
    {
        LatchNumber (1,-10,3,timeLeft());
    }
}


/*
============================================================================

                              VAMPIRE MODE

============================================================================
*/

namespace VampireMode
{
    namespace Constants
    {
        static const double maxBlood = MaxX / 4;
        static const double feedAmount = maxBlood / 12;
        static const int drinkBloodAmount = 3 * feedAmount;
    }

    namespace GameState
    {
        typedef struct
        {
            double blood;
        } Data;

        Data &getData(void)
        {
            assert(sizeof(Data) <= sizeof(gamestate.vampiremodedata));
            Data *data = (Data *)(&gamestate.vampiremodedata[0]);
            return *data;
        }

        void reset(void)
        {
            Data &data = getData();
            memset(&data, 0, sizeof(data));
        }
    }

    double &blood(void)
    {
        return GameState::getData().blood;
    }

    bool canFeed(void)
    {
        return blood() < Constants::maxBlood;
    }

    void reset(void)
    {
        GameState::reset();
        blood() = Constants::maxBlood;
    }

    bool enabled(void)
    {
        return gamestate.mode == GameMode::vampire;
    }

    void drawBloodBar(void)
    {
        VWB_Bar (10, 10, blood(), 10, 4);
    }

    void drain(void)
    {
        const double dt = TICS2SEC(tics);
        const double barSeconds[gd_max] = { 45, 40, 35, 30 };
        const double rate = Constants::maxBlood /
            barSeconds[gamestate.difficulty];
        blood() = std::max(blood() - dt * rate, 0.0);
        if (blood() <= 0.0)
        {
            playstate = ex_died;
            killerobj = NULL;
        }
    }

    void feed(void)
    {
        SD_PlaySound (GETHEARTSND);
        blood() = std::min(blood() + Constants::feedAmount,
            Constants::maxBlood);
    }

    bool canDrinkBlood(void)
    {
        return gamestate.health_mp != 100 || canFeed();
    }

    void drinkBlood(int hp)
    {
        SD_PlaySound (SLURPIESND);
        HealSelf (hp);
        blood() = std::min(blood() + Constants::drinkBloodAmount,
            Constants::maxBlood);
    }

    void remapShape(wl_stat_t itemnumber, short &shapenum)
    {
        switch (itemnumber)
        {
        case bo_clip2:
            {
                const int anim[2] = { SPR_HEART1, SPR_HEART2 };
                shapenum = anim[(frameon/48)%2];
            }
            break;
        }

        switch (shapenum)
        {
        case SPR_STAT_0:
            shapenum = SPR_STAT_38;
            break;
        case SPR_STAT_36:
            shapenum = SPR_BLOODWELL;
            break;
#ifndef SPEAR
        case SPR_STAT_10:
            shapenum = SPR_BLOODSINK;
            break;
        case SPR_EMPTYSINK:
            shapenum = SPR_STAT_10;
            break;
        case SPR_STAT_15:
            shapenum = SPR_BLOODKITCHEN;
            break;
        case SPR_EMPTYKITCHEN:
            shapenum = SPR_STAT_44;
            break;
#endif
        }
    }

    void checkUse(statobj_t *statptr)
    {
        if (enabled() && canDrinkBlood())
        {
            switch (statptr->shapenum)
            {
            case SPR_STAT_36:
                buttonheld_mp[bt_use] = true;
                statptr->shapenum = SPR_STAT_37;

                VampireMode::drinkBlood(25);
                StartBonusFlash ();
                break;

#ifndef SPEAR
            case SPR_STAT_10:
                buttonheld_mp[bt_use] = true;
                statptr->shapenum = SPR_EMPTYSINK;

                VampireMode::drinkBlood(15);
                StartBonusFlash ();
                break;
#endif
            }
        }
    }
}


/*
============================================================================

                                 CTF MODE

============================================================================
*/

namespace CtfMode
{
    bool enabled(void)
    {
        return gamestate.mode == GameMode::ctf;
    }

    int limit(void)
    {
        return ParTimes::curLevelSeconds() * 3 / 2;
    }

    int timeLeft(void)
    {
        const int sec = gamestate.TimeCount / 70;
        const int lim = limit();
        return lim - sec;
    }

    void drawTimeLeft(void)
    {
        LatchNumber (1,-10,3,timeLeft());

        int secs;
        if (CtfFlag::droppedSecs(secs))
        {
            secs = std::min(secs, 9);
            LatchNumber (3,-30,1,secs);
        }
    }
}


/*
============================================================================

                                 CTF FLAG

============================================================================
*/


namespace CtfFlag
{
    void idle(objtype *ob);

    extern statetype s_idle1;
    extern statetype s_idle2;
    extern statetype s_idle3;
    extern statetype s_idle4;
    extern statetype s_idle5;
    extern statetype s_idle6;

    statetype s_idle1      = {false,SPR_FLAG_ANIM1,10,(statefunc)idle,NULL,&s_idle2};
    statetype s_idle2      = {false,SPR_FLAG_ANIM2,10,(statefunc)idle,NULL,&s_idle3};
    statetype s_idle3      = {false,SPR_FLAG_ANIM3,10,(statefunc)idle,NULL,&s_idle4};
    statetype s_idle4      = {false,SPR_FLAG_ANIM4,10,(statefunc)idle,NULL,&s_idle5};
    statetype s_idle5      = {false,SPR_FLAG_ANIM5,10,(statefunc)idle,NULL,&s_idle6};
    statetype s_idle6      = {false,SPR_FLAG_ANIM6,10,(statefunc)idle,NULL,&s_idle1};

    namespace GameState
    {
        typedef struct
        {
            bool onFlag;
        } Player;

        typedef struct
        {
            int oid;
            int playerOid;
            int32_t droppedTics;
            lwlib::Point2f startPos;
        } Flag;

        typedef struct
        {
            Player LWMP_DECL(players);
            Flag flag;
        } Data;

        Data &getData(void)
        {
            assert(sizeof(Data) <= sizeof(gamestate.ctfflagdata));
            Data *data = (Data *)(&gamestate.ctfflagdata[0]);
            return *data;
        }

        Player &getPlayer(objtype *ob)
        {
            return getData().players[ObjectPlayerIndex(ob)];
        }

        Flag &getFlag(void)
        {
            return getData().flag;
        }

        void reset(void)
        {
            Data &data = getData();
            memset(&data, 0, sizeof(data));
        }
    }

    void reset(void)
    {
        GameState::reset();
    }

    objtype *getFlagPlayerObj(void)
    {
        GameState::Flag &gsFlag = GameState::getFlag();
        return gsFlag.playerOid != 0 ?
            &objlist[gsFlag.playerOid - 1] : NULL;
    }

    objtype *spawn(void)
    {
        SpawnNewObj (0,0,&s_idle1);
        newobj->obclass = inertobj;
        newobj->active = ac_yes;
        newobj->flags |= FL_NODRAW;

        GameState::Flag &gsFlag = GameState::getFlag();
        gsFlag.oid = (newobj - objlist) + 1;
        gsFlag.playerOid = (gamestate.mapon % 2) + 1;
        gsFlag.startPos = Object::getPos(getFlagPlayerObj());

        GameState::Player &gsPlayer =
            GameState::getPlayer(getFlagPlayerObj());
        gsPlayer.onFlag = true;

        return newobj;
    }

    objtype *getFlagObj(void)
    {
        GameState::Flag &gsFlag = GameState::getFlag();
        return &objlist[gsFlag.oid - 1];
    }

    bool objIsCarrier(objtype *playerObj)
    {
        return CtfMode::enabled() && playerObj == getFlagPlayerObj();
    }

    bool drop(void)
    {
        if (objIsCarrier(player_mp))
        {
            GameState::Flag &gsFlag = GameState::getFlag();
            gsFlag.playerOid = 0;
            gsFlag.droppedTics = 1;
            SD_PlaySound (DROPFLAGSND);
            {
                objtype *flagObj = getFlagObj();
                flagObj->flags &= ~FL_NODRAW;
            }
            sb(DrawWeapon());
            return true;
        }

        return false;
    }

    void idle(objtype *ob)
    {
        objtype *flagPlayerObj = getFlagPlayerObj();
        if (flagPlayerObj != NULL)
        {
            ob->x = flagPlayerObj->x;
            ob->y = flagPlayerObj->y;
            ob->tilex = (short)(ob->x >> TILESHIFT);
            ob->tiley = (short)(ob->y >> TILESHIFT);
        }
        else
        {
            for (int i = 0; i < LWMP_NUM_PLAYERS; i++)
            {
                objtype *playerObj = player[i];
                GameState::Player &gsPlayer =
                    GameState::getPlayer(playerObj);
                const bool onFlag = 
                    abs(playerObj->x - ob->x) <= (TILEGLOBAL / 2) &&
                    abs(playerObj->y - ob->y) <= (TILEGLOBAL / 2);
                if (onFlag != gsPlayer.onFlag &&
                    playerObj->state == &s_player)
                {
                    gsPlayer.onFlag = onFlag;

                    if (onFlag)
                    {
                        GameState::Flag &gsFlag = GameState::getFlag();
                        gsFlag.playerOid = i + 1;
                        gsFlag.droppedTics = 0;

                        gamestate.attacktics_mp = 0;
                    }
                }
            }

            if (getFlagPlayerObj())
            {
                SD_PlaySound (GETFLAGSND);
                ob->flags |= FL_NODRAW;

                {
                    int c;
                    const int ind = getFlagPlayerObj() - objlist;

                    for (LWMP_REPEAT_ONCE(c, ind))
                    {
                        sb(DrawWeapon());
                    }
                }
            }
            else
            {
                GameState::Flag &gsFlag = GameState::getFlag();
                if (gsFlag.droppedTics > 0)
                {
                    gsFlag.droppedTics += tics;
                    if (TICS2SEC(gsFlag.droppedTics) >= 10.0)
                    {
                        gsFlag.droppedTics = 0;
                        Object::setPos(ob, gsFlag.startPos);
                        SD_PlaySound (RTNFLAGSND);
                    }
                }
            }
        }

        const int sec = gamestate.TimeCount / 70;
        if (sec >= CtfMode::limit())
        {
            playstate = ex_died;
            killerobj = ob;
        }
    }

    bool droppedSecs(int &secs)
    {
        GameState::Flag &gsFlag = GameState::getFlag();
        if (gsFlag.droppedTics > 0)
        {
            secs = (int)(10.0 - TICS2SEC(gsFlag.droppedTics));
            return true;
        }
        return false;
    }
}


/*
============================================================================

                               HARVESTER MODE

============================================================================
*/

namespace HarvesterMode
{
    namespace Pillar
    {
        extern statetype s_skull1;
        extern statetype s_skull2;

        statetype s_idle      = {false,SPR_HPILLAR_IDLE,0,NULL,NULL,&s_idle};
        statetype s_skull1    = {false,SPR_HPILLAR_SKULL1,10,NULL,NULL,&s_skull2};
        statetype s_skull2    = {false,SPR_HPILLAR_SKULL2,10,NULL,NULL,&s_skull1};

        void spawn(int tilex, int tiley)
        {
            SpawnNewObj (tilex,tiley,&s_idle);
            newobj->obclass = inertobj;
            newobj->active = ac_yes;
        }

        void enterSkull(objtype *ob)
        {
            NewState(ob, &s_skull1);
        }

        void clearSkull(objtype *ob)
        {
            NewState(ob, &s_idle);
        }

        bool checkPickup(objtype *ob, int &playerNum)
        {
            for (int i = 0; i < LWMP_NUM_PLAYERS; i++)
            {
                objtype *playerObj = player[i];
                const bool on = 
                    abs(playerObj->x - ob->x) <= (TILEGLOBAL / 2) &&
                    abs(playerObj->y - ob->y) <= (TILEGLOBAL / 2);
                if (on)
                {
                    playerNum = i;
                    return true;
                }
            }

            return false;
        }
    }

    namespace Soul
    {
        extern statetype s_anim1;
        extern statetype s_anim2;
        extern statetype s_anim3;
        extern statetype s_anim4;
        extern statetype s_anim5;

        statetype s_anim1     = {false,SPR_HSOUL1,10,NULL,NULL,&s_anim2};
        statetype s_anim2     = {false,SPR_HSOUL2,10,NULL,NULL,&s_anim3};
        statetype s_anim3     = {false,SPR_HSOUL3,10,NULL,NULL,&s_anim4};
        statetype s_anim4     = {false,SPR_HSOUL4,10,NULL,NULL,&s_anim5};
        statetype s_anim5     = {false,SPR_HSOUL5,10,NULL,NULL,NULL};

        void spawn(objtype *ob)
        {
            SpawnNewObj (ob->tilex,ob->tiley,&s_anim1);
            newobj->obclass = inertobj;
            newobj->active = ac_yes;
            newobj->x = ob->x;
            newobj->y = ob->y;
        }
    }

    static const double limitSecs = 10.0;
    static const double soulSecs = 6.0;

    namespace GameState
    {
        typedef struct
        {
            bool spawned;
            double secs;
            int objNums[NUMAREAS];
            int objCount;
        } Data;

        Data &getData(void)
        {
            assert(sizeof(Data) <= sizeof(gamestate.harvestermodedata));
            Data *data = (Data *)(&gamestate.harvestermodedata[0]);
            return *data;
        }

        void reset(void)
        {
            Data &data = getData();
            memset(&data, 0, sizeof(data));
        }
    }

    bool enabled(void)
    {
        return gamestate.mode == GameMode::harvester;
    }

    void reset(void)
    {
        GameState::reset();
    }

    int timeLeft(void)
    {
        GameState::Data &gsData = GameState::getData();
        return limitSecs - gsData.secs;
    }

    void drawTimeLeft(void)
    {
        GameState::Data &gsData = GameState::getData();
        if (gsData.spawned)
        {
            LatchNumber (1,-10,3,timeLeft());
        }
    }

    void spawnPillar(int tilex, int tiley)
    {
        GameState::Data &gsData = GameState::getData();
        if (gsData.objCount < NUMAREAS)
        {
            Pillar::spawn(tilex, tiley);
            gsData.objNums[gsData.objCount++] = newobj - objlist;
        }
    }

    bool spawnSkulls(void)
    {
        GameState::Data &gsData = GameState::getData();
        for (int i = 0; i < gsData.objCount; i++)
        {
            objtype *ob = objlist + gsData.objNums[i];
            Pillar::enterSkull(ob);
        }

        if (gsData.objCount > 0)
        {
            if (!gsData.spawned)
            {
                gsData.spawned = true;
                gsData.secs = 0.0;
            }
            else
            {
                gsData.secs = std::max(gsData.secs - soulSecs, 0.0);
            }

            return true;
        }

        return false;
    }

    void clearSkulls(void)
    {
        GameState::Data &gsData = GameState::getData();
        for (int i = 0; i < gsData.objCount; i++)
        {
            objtype *ob = objlist + gsData.objNums[i];
            Pillar::clearSkull(ob);
        }
        SD_PlaySound (HSKULLCLEARSND);
    }

    bool enemyValid(objtype *ob)
    {
        switch (ob->obclass)
        {
        case guardobj:
        case officerobj:
        case mutantobj:
        case ssobj:
        case dogobj:
        case bjmutantobj:
        case bjmutantdogobj:
            return true;
        default:
            return false;
        }
    }

    bool checkPickup(int &playerNum)
    {
        GameState::Data &gsData = GameState::getData();

        for (int i = 0; i < gsData.objCount; i++)
        {
            objtype *ob = objlist + gsData.objNums[i];
            if (Pillar::checkPickup(ob, playerNum))
            {
                return true;
            }
        }

        return false;
    }
    
    void poll(void)
    {
        GameState::Data &gsData = GameState::getData();
        if (gsData.spawned)
        {
            gsData.secs += TICS2SEC(tics);

            int playerNum = -1;
            if (checkPickup(playerNum))
            {
                gsData.spawned = false;
                clearSkulls();

                int c;
                for (LWMP_REPEAT_ONCE(c, playerNum))
                {
                    StartBonusFlash();
                }
            }

            if (gsData.secs >= 10.0)
            {
                playstate = ex_died;
                killerobj = NULL;
            }
        }
    }

    void enemyKilled(objtype *ob)
    {
        if (enabled() && enemyValid(ob) && spawnSkulls())
        {
            Soul::spawn(ob);
            SD_PlaySound (HSKULLSPAWNSND);
        }
    }
}


/*
============================================================================

                              BJ MUTANT MODE

============================================================================
*/

namespace BjMutantMode
{
    bool enabled(void)
    {
        return gamestate.mode == GameMode::bjmutant;
    }

    void replaceWeap(weapontype &wp)
    {
        if (enabled() && US_RndT() < 128)
        {
            if (wp == wp_pistol)
            {
                wp = wp_machinegun;
            }
        }
        if (enabled() && US_RndT() < 128)
        {
            if (wp == wp_machinegun)
            {
                wp = wp_chaingun;
            }
        }
    }

    void scaleHp(short &hp)
    {
        if (enabled())
        {
            if (hp == 1)
            {
                hp = 25;
            }
            hp = std::min(hp * 3, 150);
        }
    }

    void scaleAmmo(int &ammo)
    {
        if (enabled())
        {
            ammo *= 2;
        }
    }
}


/*
============================================================================

                              RAMPAGE MODE

============================================================================
*/

namespace RampageMode
{
    namespace HitObj
    {
        static Vec *l = NULL;

        void reset(Vec *l)
        {
            HitObj::l = l;
        }

        void next(objtype *ob)
        {
            if (l)
            {
                l->push_back(ob);
            }
        }
    }

    bool enabled(void)
    {
        return gamestate.mode == GameMode::rampage;
    }

    namespace Player
    {
        namespace Constants
        {
            static const int angerUnitTicCount = 50;
            static const int nudgeDamage = Gib::Constants::instantLimit;
            static const int ammoToHp = 6;
        }

        namespace State
        {
            typedef struct
            {
                int32_t angerTics;
            } Data;

            Data &getData(void)
            {
                const int objNum = ObjectPlayerIndex(player_mp);
                assert(sizeof(Data) <=
                    sizeof(gamestate.rampageplayerdata[objNum]));
                Data *data = (Data *)(&gamestate.rampageplayerdata[objNum][0]);
                return *data;
            }
        }

        void drainAmmo(void)
        {
            State::Data &sData = State::getData();

            if (gamestate.ammo_mp > 0)
            {
                sData.angerTics += tics;
                if (sData.angerTics >= Constants::angerUnitTicCount)
                {
                    sData.angerTics %= Constants::angerUnitTicCount;
                    gamestate.ammo_mp--;
                    sb(DrawAmmo ());
                }
            }
            else
            {
                sData.angerTics = 0;
            }
        }

        bool firstHalfAttack(void)
        {
            const int periodX2 = 6 * 4;
            const int period = periodX2 / 2;
            const int atkTics = gamestate.attacktics_mp;
            return player_mp->state == &s_attack && 
                atkTics < period;
        }

        void clipMove(int32_t xmove, int32_t ymove)
        {
            if (buttonstate_mp[bt_run] && gamestate.ammo_mp > 0 && 
                firstHalfAttack())
            {
                typedef HitObj::Vec Vec;
                Vec objs;
                HitObj::reset(&objs);

                ClipMove(player_mp, xmove, ymove);

                HitObj::reset(NULL);

                for (Vec::size_type i = 0; i < objs.size(); i++)
                {
                    objtype *ob = objs[i];
                    const int orighitpoints = ob->hitpoints;

                    const int damage = Constants::nudgeDamage;
                    DamageActor(ob, damage);

                    RampageMode::launch(ob);
                    SD_PlaySound (ATKPUNCHCHARGESND);

                    int hp = gamestate.ammo_mp * Constants::ammoToHp;

                    if (orighitpoints > 0)
                    {
                        hp = std::max(hp - orighitpoints, 0);
                    }

                    gamestate.ammo_mp = hp / Constants::ammoToHp;
                    sb(DrawAmmo ());
                }
            }
            else
            {
                ClipMove(player_mp, xmove, ymove);
            }
        }

        void drawRageBar(void)
        {
            const int gap = 15;
            const int w = ((MaxX - (gap * 2)) * gamestate.ammo_mp) / 99;
            VWB_Bar (gap, 10, w, 10, 4);
        }

        void resistDamage(int &points)
        {
            const int maxResistPerc = 80;
            const int resistPerc = gamestate.ammo_mp * maxResistPerc / 99;
            points = (points > 0 ? 
                std::max(points * (100 - resistPerc) / 100, 1) : 0);
        }
    }

    namespace Enemy
    {
        namespace Proj
        {
            extern statetype s_move;
            extern statetype s_dead;

            void move(objtype *ob);

            statetype s_move       = {false,SPR_DEMO,0,(statefunc)move,NULL,&s_move};
            statetype s_dead       = {false,SPR_DEMO,0,NULL,NULL,&s_dead};

            void spawn(int angle)
            {
                GetNewActor ();
                newobj->state = &s_move;
                newobj->obclass = inertobj;
                newobj->active = ac_yes;
                newobj->speed = 0x2000l;
                newobj->dir = nodir;
                newobj->flags = FL_NEVERMARK;
                newobj->angle = angle;
            }

            void move(objtype *ob)
            {
                int32_t deltax,deltay;
                int32_t speed;
                int      xl,yl,xh,yh,x,y;
                objtype *check;

                speed = (int32_t)ob->speed*tics;

                {
                    double spd = fixedpt_toconst(ob->speed) * SECS2TICS(1);

                    const double dt = TICS2SEC(tics);
                    const double decel = 20.0;
                    spd -= decel * dt;

                    if (spd <= 0.0)
                    {
                        NewState(ob, &s_dead);
                        return;
                    }

                    ob->speed = fixedpt_rconst(spd / SECS2TICS(1));
                }

                deltax = FixedMul(speed,costable[ob->angle]);
                deltay = -FixedMul(speed,sintable[ob->angle]);

                if (deltax>0x10000l)
                    deltax = 0x10000l;
                if (deltay>0x10000l)
                    deltay = 0x10000l;

                ob->x += deltax;
                ob->y += deltay;

                const int sz = 0x4000;
                xl = (ob->x-sz) >> TILESHIFT;
                yl = (ob->y-sz) >> TILESHIFT;

                xh = (ob->x+sz) >> TILESHIFT;
                yh = (ob->y+sz) >> TILESHIFT;

                //
                // check for solid walls
                //
                for (y=yl;y<=yh;y++)
                {
                    for (x=xl;x<=xh;x++)
                    {
                        check = actorat[x][y];
                        if (check && !ISPOINTER(check))
                        {
                            NewState(ob, &s_dead);
                            return;
                        }

                        if (check && ISPOINTER(check) &&
                            (check->flags & FL_SHOOTABLE) != 0 &&
                            LABS(ob->x - check->x) < sz &&
                            LABS(ob->y - check->y) < sz)
                        {
                            const int damage = (US_RndT() >> 3) + 20;
                            DamageActor(check,damage);
                            NewState(ob, &s_dead);
                        }
                    }
                }
            }
        }

        class State
        {
            objtype *ob;

            typedef struct
            {
                int moveObjNum;
                bool launch;
                int angle;
            } Data;

            Data &data(void) const
            {
                assert(sizeof(Data) <= sizeof(ob->rampagedata));
                return *((Data *)(&ob->rampagedata[0]));
            }

        public:
            explicit State(objtype *ob_) : ob(ob_)
            {
            }

            int &moveObjNum(void) const
            {
                return data().moveObjNum;
            }

            bool &launch(void) const
            {
                return data().launch;
            }

            int &angle(void) const
            {
                return data().angle;
            }
        };

        void die(objtype *ob)
        {
            State obS(ob);

            if (obS.launch())
            {
                int &moveObjNum = obS.moveObjNum();

                if (moveObjNum == 0)
                {
                    Proj::spawn(obS.angle());
                    moveObjNum = newobj - objlist;

                    Object::setPos(newobj, Object::getPos(ob));
                }
                else
                {
                    objtype *moveObj = objlist + moveObjNum;
                    Object::setPos(ob, Object::getPos(moveObj));
                }
            }
        }

        void resetLaunch(objtype *ob)
        {
            State(ob).launch() = false;
        }

        void launch(objtype *ob)
        {
            State(ob).launch() = true;
            State(ob).angle() = player_mp->angle;
        }
    }

    void resetLaunch(objtype *ob)
    {
        if (enabled())
        {
            Enemy::resetLaunch(ob);
        }
    }

    void launch(objtype *ob)
    {
        if (enabled())
        {
            Enemy::launch(ob);
        }
    }

    void enemyKilled(objtype *ob)
    {
        if (enabled())
        {
            GiveAmmo(4);
        }
    }

    void remapStatic(int &type)
    {
        if (enabled())
        {
            switch (statinfo[type].type)
            {
            case bo_machinegun:
            case bo_chaingun:
                type = spr_to_statinfo_type[SPR_RAMPAGEAMMO];
                break;
            }
        }
    }
}


/*
============================================================================

                              ZOMBIE MODE

============================================================================
*/

namespace ZombieMode
{
    namespace Constants
    {
        static const int32_t restTics = 200;
    }

    bool enabled(void)
    {
        return gamestate.mode == GameMode::zombie;
    }

    void shootChance(objtype *ob, int &chance)
    {
        int32_t move,target;

        move = ob->speed*tics;

        chance = 0;

        target = abs(ob->x - player_mp->x) - move;
        //std::cerr << "x " << target << std::endl;
        if (target <= MINACTORDIST)
        {
            target = abs(ob->y - player_mp->y) - move;
            //std::cerr << "y " << target << std::endl;
            if (target <= MINACTORDIST)
            {
                chance = 300;
            }
        }
    }

    void bite(objtype *ob)
    {
        int32_t    dx,dy;

        PlaySoundLocActor(ZOMBIEATTACKSND, ob);

        dx = player_mp->x - ob->x;
        if (dx<0)
            dx = -dx;
        dx -= TILEGLOBAL;
        if (dx <= MINACTORDIST)
        {
            dy = player_mp->y - ob->y;
            if (dy<0)
                dy = -dy;
            dy -= TILEGLOBAL;
            if (dy <= MINACTORDIST)
            {
                if (US_RndT()<180)
                {
                    TakeDamage (US_RndT()>>4,ob);
                    return;
                }
            }
        }
    }

    class State
    {
        objtype *ob;

        typedef struct
        {
            bool wasAlive;
            int32_t duration;
        } Data;

        Data &data(void) const
        {
            assert(sizeof(Data) <= sizeof(ob->zombiedata));
            return *((Data *)(&ob->zombiedata[0]));
        }

    public:
        explicit State(objtype *ob_) : ob(ob_)
        {
        }

        bool &wasAlive(void) const
        {
            return data().wasAlive;
        }

        int32_t &duration(void) const
        {
            return data().duration;
        }
    };

    bool visible(objtype *ob)
    {
        int c;

        for (LWMP_REPEAT(c))
        {
            if (ob->visflags_mp & FL_VISABLE)
            {
                LWMP_ENDREPEAT(c);
                return true;
            }
        }

        return false;
    }

    bool tooClose(objtype *ob, int dist)
    {
        int c;

        for (LWMP_REPEAT(c))
        {
            const int dx = ABS(ob->tilex - player_mp->tilex);
            const int dy = ABS(ob->tiley - player_mp->tiley);
            if (dx <= dist && dy <= dist)
            {
                LWMP_ENDREPEAT(c);
                return true;
            }
        }

        return false;
    }

    void rest(objtype *ob)
    {
        if (!State(ob).wasAlive())
        {
            return;
        }

        int32_t &duration = State(ob).duration();

        duration = std::min(duration + (int32_t)tics, Constants::restTics);

        if (actorat[ob->tilex][ob->tiley] == NULL &&
            duration >= Constants::restTics && !visible(ob) &&
            !tooClose(ob, 2))
        {
            duration = 0;

            actorat[ob->tilex][ob->tiley] = ob;
            ob->flags |= FL_SHOOTABLE;
            ob->flags &= ~FL_NEVERMARK;

            ob->hitpoints = ob->starthitpoints;

            switch (ob->obclass)
            {
                case guardobj:
                    NewState(ob, &s_grdchase1);
                    break;

                case officerobj:
                    NewState(ob, &s_ofcchase1);
                    break;

                case mutantobj:
                    NewState(ob, &s_mutchase1);
                    break;

                case ssobj:
                    NewState(ob, &s_sschase1);
                    break;

                case dogobj:
                    NewState(ob, &s_dogchase1);
                    break;
            }
        }
    }

    bool fakeKill(objtype *ob)
    {
        State(ob).wasAlive() = true;

        switch (ob->obclass)
        {
            case guardobj:
                NewState (ob,&s_grddie1);
                Enemy::dropAmmo(ob);
                break;

            case officerobj:
                NewState (ob,&s_ofcdie1);
                Enemy::dropAmmo(ob);
                break;

            case mutantobj:
                NewState (ob,&s_mutdie1);
                Enemy::dropAmmo(ob);
                break;

            case ssobj:
                NewState (ob,&s_ssdie1);
                Enemy::dropAmmo(ob);
                break;

            case dogobj:
                NewState (ob,&s_dogdie1);
                break;
        }

        ob->flags &= ~FL_SHOOTABLE;
        ob->flags |= FL_NEVERMARK;

        if (actorat[ob->tilex][ob->tiley] == ob)
        {
            actorat[ob->tilex][ob->tiley] = NULL;
        }

        return true;
    }

    bool wantsRest(objtype *ob)
    {
        fakeKill(ob);
        return true;
    }

    void scaleHp(short &hp)
    {
        if (enabled())
        {
            if (hp != 1)
            {
                hp = std::min(hp * 5, 200);
            }
            else
            {
                hp = 80; // dog
            }
        }
    }

    void scaleSpeed(int32_t &speed)
    {
        if (enabled())
        {
            speed = speed * 3 / 8;
        }
    }

    void scaleAmmo(int &ammo)
    {
        if (enabled())
        {
            ammo *= 2;
        }
    }

    void setShade(int &shade)
    {
        if (enabled())
        {
            shade = COLORREMAP_SHADE_ZOMBIE + 1;
        }
    }

    namespace Enemy
    {
        namespace SightSound
        {
            typedef std::map<classtype, soundnames> Map;

            static Map m;

            bool get(classtype obclass, soundnames &snd)
            {
                if (m.empty())
                {
                    m[guardobj] = HITWALLSND;
                    m[officerobj] = HITWALLSND;
                    m[ssobj] = HITWALLSND;
                }
                if (m.find(obclass) != m.end())
                {
                    snd = m[obclass];
                    return true;
                }
                return false;
            }
        }

        bool sightSound(objtype *ob, soundnames &snd)
        {
            return SightSound::get(ob->obclass, snd);
        }

        namespace DieSound
        {
            typedef std::map<classtype, soundnames> Map;

            static Map m;

            bool get(classtype obclass, soundnames &snd)
            {
                if (m.empty())
                {
                    m[guardobj] = AHHHGSND;
                    m[officerobj] = AHHHGSND;
                    m[ssobj] = AHHHGSND;
                }
                if (m.find(obclass) != m.end())
                {
                    snd = m[obclass];
                    return true;
                }
                return false;
            }
        }

        bool dieSound(objtype *ob, soundnames &snd)
        {
            return DieSound::get(ob->obclass, snd);
        }
    }

    bool enemySightSound(objtype *ob, soundnames &snd)
    {
        return enabled() && Enemy::sightSound(ob, snd);
    }

    bool enemyDieSound(objtype *ob, soundnames &snd)
    {
        return enabled() && Enemy::dieSound(ob, snd);
    }

    namespace ShapeRemap
    {
        typedef std::map<int, int> Map;

        static Map m;

        int get(int shapenum)
        {
            if (m.empty())
            {
                m[SPR_GRD_SHOOT2] = SPR_GRD_PAIN_1;
                m[SPR_GRD_SHOOT3] = SPR_GRD_PAIN_2;
                m[SPR_SS_SHOOT2] = SPR_SS_PAIN_1;
                m[SPR_SS_SHOOT3] = SPR_SS_PAIN_2;
                m[SPR_OFC_SHOOT2] = SPR_OFC_PAIN_1;
                m[SPR_OFC_SHOOT3] = SPR_OFC_PAIN_2;
                m[SPR_MUT_SHOOT2] = SPR_MUT_PAIN_1;
                m[SPR_MUT_SHOOT3] = SPR_MUT_PAIN_2;
                m[SPR_MUT_SHOOT4] = SPR_MUT_PAIN_1;
            }

            if (m.find(shapenum) != m.end())
            {
                return m[shapenum];
            }

            return 0;
        }
    }

    void remapShapeNum(int &shapenum)
    {
        if (enabled())
        {
            const short newShapeNum = ShapeRemap::get(shapenum);
            if (newShapeNum != 0)
            {
                shapenum = newShapeNum;
            }
        }
    }
}


/*
============================================================================

                         ZOMBIE HARVESTER MODE

============================================================================
*/

namespace ZombieHarvesterMode
{
    bool enabled(void)
    {
        return gamestate.mode == GameMode::zombieharvester;
    }

    void drawTimeLeft(void)
    {
        Harvester::drawTimeLeft();
    }

    bool wasAlive(objtype *ob)
    {
        return enabled() && Zombie::wasAlive(ob);
    }

    void spawnPillar(int tilex, int tiley)
    {
        Harvester::spawnPillar(tilex, tiley);
    }

    void reset(void)
    {
        Harvester::reset();
    }

    void poll(void)
    {
        Harvester::poll();
    }

    void enemyKilled(objtype *ob)
    {
        Harvester::enemyKilled(ob);
    }

    void shootChance(objtype *ob, int &chance)
    {
        Zombie::shootChance(ob, chance);
    }

    void bite(objtype *ob)
    {
        Zombie::bite(ob);
    }

    void die(objtype *ob)
    {
        Zombie::rest(ob);
    }

    bool enemySightSound(objtype *ob, soundnames &snd)
    {
        return wasAlive(ob) && Zombie::enemySightSound(ob, snd);
    }

    bool enemyDieSound(objtype *ob, soundnames &snd)
    {
        return wasAlive(ob) && Zombie::enemyDieSound(ob, snd);
    }

    void remapShapeNum(objtype *ob, int &shapenum)
    {
        if (wasAlive(ob))
        {
            Zombie::remapShapeNum(shapenum);
        }
    }

    void fakeKill(objtype *ob)
    {
        Zombie::fakeKill(ob);
    }

    namespace Zombie
    {
        namespace Constants
        {
            static const int32_t restTics = 200;
        }

        void shootChance(objtype *ob, int &chance)
        {
            int32_t move,target;

            move = ob->speed*tics;

            chance = 0;

            target = abs(ob->x - player_mp->x) - move;
            //std::cerr << "x " << target << std::endl;
            if (target <= MINACTORDIST)
            {
                target = abs(ob->y - player_mp->y) - move;
                //std::cerr << "y " << target << std::endl;
                if (target <= MINACTORDIST)
                {
                    chance = 300;
                }
            }
        }

        void bite(objtype *ob)
        {
            int32_t    dx,dy;

            PlaySoundLocActor(ZOMBIEATTACKSND, ob);

            dx = player_mp->x - ob->x;
            if (dx<0)
                dx = -dx;
            dx -= TILEGLOBAL;
            if (dx <= MINACTORDIST)
            {
                dy = player_mp->y - ob->y;
                if (dy<0)
                    dy = -dy;
                dy -= TILEGLOBAL;
                if (dy <= MINACTORDIST)
                {
                    if (US_RndT()<180)
                    {
                        TakeDamage (US_RndT()>>4,ob);
                        return;
                    }
                }
            }
        }

        class State
        {
            objtype *ob;

            typedef struct
            {
                bool wasAlive;
                int32_t duration;
            } Data;

            Data &data(void) const
            {
                assert(sizeof(Data) <= sizeof(ob->zombiedata));
                return *((Data *)(&ob->zombiedata[0]));
            }

        public:
            explicit State(objtype *ob_) : ob(ob_)
            {
            }

            bool &wasAlive(void) const
            {
                return data().wasAlive;
            }

            int32_t &duration(void) const
            {
                return data().duration;
            }
        };

        bool visible(objtype *ob)
        {
            int c;

            for (LWMP_REPEAT(c))
            {
                if (ob->visflags_mp & FL_VISABLE)
                {
                    LWMP_ENDREPEAT(c);
                    return true;
                }
            }

            return false;
        }

        bool tooClose(objtype *ob, int dist)
        {
            int c;

            for (LWMP_REPEAT(c))
            {
                const int dx = ABS(ob->tilex - player_mp->tilex);
                const int dy = ABS(ob->tiley - player_mp->tiley);
                if (dx <= dist && dy <= dist)
                {
                    LWMP_ENDREPEAT(c);
                    return true;
                }
            }

            return false;
        }

        void rest(objtype *ob)
        {
            if (!State(ob).wasAlive())
            {
                return;
            }

            int32_t &duration = State(ob).duration();

            duration = std::min(duration + (int32_t)tics, Constants::restTics);

            if (actorat[ob->tilex][ob->tiley] == NULL &&
                duration >= Constants::restTics && !visible(ob) &&
                !tooClose(ob, 2))
            {
                duration = 0;

                actorat[ob->tilex][ob->tiley] = ob;
                ob->flags |= FL_SHOOTABLE;
                ob->flags &= ~FL_NEVERMARK;

                ob->hitpoints = ob->starthitpoints;

                switch (ob->obclass)
                {
                    case guardobj:
                        NewState(ob, &s_grdchase1);
                        break;

                    case officerobj:
                        NewState(ob, &s_ofcchase1);
                        break;

                    case mutantobj:
                        NewState(ob, &s_mutchase1);
                        break;

                    case ssobj:
                        NewState(ob, &s_sschase1);
                        break;

                    case dogobj:
                        NewState(ob, &s_dogchase1);
                        break;
                }

                scaleHp(ob->hitpoints);
                scaleSpeed(ob->speed);
                setShade(ob->shade);
            }
        }

        void scaleHp(short &hp)
        {
            if (enabled())
            {
                if (hp != 1)
                {
                    hp = std::min(hp * 4, 200);
                }
                else
                {
                    hp = 50; // dog
                }
            }
        }

        void scaleSpeed(int32_t &speed)
        {
            if (enabled())
            {
                speed = speed * 5 / 8;
            }
        }

        void setShade(int &shade)
        {
            if (enabled())
            {
                shade = COLORREMAP_SHADE_ZOMBIE + 1;
            }
        }

        namespace Enemy
        {
            namespace SightSound
            {
                typedef std::map<classtype, soundnames> Map;

                static Map m;

                bool get(classtype obclass, soundnames &snd)
                {
                    if (m.empty())
                    {
                        m[guardobj] = HITWALLSND;
                        m[officerobj] = HITWALLSND;
                        m[ssobj] = HITWALLSND;
                    }
                    if (m.find(obclass) != m.end())
                    {
                        snd = m[obclass];
                        return true;
                    }
                    return false;
                }
            }

            bool sightSound(objtype *ob, soundnames &snd)
            {
                return SightSound::get(ob->obclass, snd);
            }

            namespace DieSound
            {
                typedef std::map<classtype, soundnames> Map;

                static Map m;

                bool get(classtype obclass, soundnames &snd)
                {
                    if (m.empty())
                    {
                        m[guardobj] = AHHHGSND;
                        m[officerobj] = AHHHGSND;
                        m[ssobj] = AHHHGSND;
                    }
                    if (m.find(obclass) != m.end())
                    {
                        snd = m[obclass];
                        return true;
                    }
                    return false;
                }
            }

            bool dieSound(objtype *ob, soundnames &snd)
            {
                return DieSound::get(ob->obclass, snd);
            }
        }

        bool enemySightSound(objtype *ob, soundnames &snd)
        {
            return enabled() && Enemy::sightSound(ob, snd);
        }

        bool enemyDieSound(objtype *ob, soundnames &snd)
        {
            return enabled() && Enemy::dieSound(ob, snd);
        }

        namespace ShapeRemap
        {
            typedef std::map<int, int> Map;

            static Map m;

            int get(int shapenum)
            {
                if (m.empty())
                {
                    m[SPR_GRD_SHOOT2] = SPR_GRD_PAIN_1;
                    m[SPR_GRD_SHOOT3] = SPR_GRD_PAIN_2;
                    m[SPR_SS_SHOOT2] = SPR_SS_PAIN_1;
                    m[SPR_SS_SHOOT3] = SPR_SS_PAIN_2;
                    m[SPR_OFC_SHOOT2] = SPR_OFC_PAIN_1;
                    m[SPR_OFC_SHOOT3] = SPR_OFC_PAIN_2;
                    m[SPR_MUT_SHOOT2] = SPR_MUT_PAIN_1;
                    m[SPR_MUT_SHOOT3] = SPR_MUT_PAIN_2;
                    m[SPR_MUT_SHOOT4] = SPR_MUT_PAIN_1;
                }

                if (m.find(shapenum) != m.end())
                {
                    return m[shapenum];
                }

                return 0;
            }
        }

        void remapShapeNum(int &shapenum)
        {
            if (enabled())
            {
                const short newShapeNum = ShapeRemap::get(shapenum);
                if (newShapeNum != 0)
                {
                    shapenum = newShapeNum;
                }
            }
        }

        bool &wasAlive(objtype *ob)
        {
            return State(ob).wasAlive();
        }

        void fakeKill(objtype *ob)
        {
            wasAlive(ob) = false;

            switch (ob->obclass)
            {
                case guardobj:
                    NewState (ob,&s_grddie1);
                    ::Enemy::dropAmmo(ob);
                    break;

                case officerobj:
                    NewState (ob,&s_ofcdie1);
                    ::Enemy::dropAmmo(ob);
                    break;

                case mutantobj:
                    NewState (ob,&s_mutdie1);
                    ::Enemy::dropAmmo(ob);
                    break;

                case ssobj:
                    NewState (ob,&s_ssdie1);
                    ::Enemy::dropAmmo(ob);
                    break;

                case dogobj:
                    NewState (ob,&s_dogdie1);
                    break;
            }

            ob->flags &= ~FL_SHOOTABLE;
            ob->flags |= FL_NEVERMARK;

            if (actorat[ob->tilex][ob->tiley] == ob)
            {
                actorat[ob->tilex][ob->tiley] = NULL;
            }
        }
    }

    namespace Harvester
    {
        class State
        {
            objtype *ob;

            typedef struct
            {
                bool captured;
            } Data;

            Data &data(void) const
            {
                assert(sizeof(Data) <= sizeof(ob->zombieharvesterdata));
                return *((Data *)(&ob->zombieharvesterdata[0]));
            }

        public:
            explicit State(objtype *ob_) : ob(ob_)
            {
            }

            bool &captured(void) const
            {
                return data().captured;
            }
        };

        namespace Pillar
        {
            extern statetype s_skull1;
            extern statetype s_skull2;

            statetype s_idle      = {false,SPR_HPILLAR_IDLE,0,NULL,NULL,&s_idle};
            statetype s_skull1    = {false,SPR_HPILLAR_SKULL1,10,NULL,NULL,&s_skull2};
            statetype s_skull2    = {false,SPR_HPILLAR_SKULL2,10,NULL,NULL,&s_skull1};

            void spawn(int tilex, int tiley)
            {
                SpawnNewObj (tilex,tiley,&s_idle);
                newobj->obclass = inertobj;
                newobj->active = ac_yes;
            }

            void enterSkull(objtype *ob)
            {
                NewState(ob, &s_skull1);
            }

            void clearSkull(objtype *ob)
            {
                NewState(ob, &s_idle);
            }

            bool checkPickup(objtype *ob, int &playerNum)
            {
                for (int i = 0; i < LWMP_NUM_PLAYERS; i++)
                {
                    objtype *playerObj = player[i];
                    const bool on = 
                        abs(playerObj->x - ob->x) <= (TILEGLOBAL / 2) &&
                        abs(playerObj->y - ob->y) <= (TILEGLOBAL / 2);
                    if (on)
                    {
                        playerNum = i;
                        return true;
                    }
                }

                return false;
            }
        }

        namespace Soul
        {
            extern statetype s_anim1;
            extern statetype s_anim2;
            extern statetype s_anim3;
            extern statetype s_anim4;
            extern statetype s_anim5;

            statetype s_anim1     = {false,SPR_HSOUL1,10,NULL,NULL,&s_anim2};
            statetype s_anim2     = {false,SPR_HSOUL2,10,NULL,NULL,&s_anim3};
            statetype s_anim3     = {false,SPR_HSOUL3,10,NULL,NULL,&s_anim4};
            statetype s_anim4     = {false,SPR_HSOUL4,10,NULL,NULL,&s_anim5};
            statetype s_anim5     = {false,SPR_HSOUL5,10,NULL,NULL,NULL};

            void spawn(objtype *ob)
            {
                SpawnNewObj (ob->tilex,ob->tiley,&s_anim1);
                newobj->obclass = inertobj;
                newobj->active = ac_yes;
                newobj->x = ob->x;
                newobj->y = ob->y;
            }
        }

        static const double limitSecs = 8.0;
        static const double soulSecs = 6.0;

        namespace GameState
        {
            typedef struct
            {
                bool spawned;
                double secs;
                int objNums[NUMAREAS];
                int objCount;
            } Data;

            Data &getData(void)
            {
                assert(sizeof(Data) <= sizeof(gamestate.harvestermodedata));
                Data *data = (Data *)(&gamestate.harvestermodedata[0]);
                return *data;
            }

            void reset(void)
            {
                Data &data = getData();
                memset(&data, 0, sizeof(data));
            }
        }

        void reset(void)
        {
            GameState::reset();
        }

        int timeLeft(void)
        {
            GameState::Data &gsData = GameState::getData();
            return limitSecs - gsData.secs;
        }

        void drawTimeLeft(void)
        {
            GameState::Data &gsData = GameState::getData();
            if (gsData.spawned)
            {
                LatchNumber (1,-10,3,timeLeft());
            }
        }

        void spawnPillar(int tilex, int tiley)
        {
            GameState::Data &gsData = GameState::getData();
            if (gsData.objCount < NUMAREAS)
            {
                Pillar::spawn(tilex, tiley);
                gsData.objNums[gsData.objCount++] = newobj - objlist;
            }
        }

        bool spawnSkulls(void)
        {
            GameState::Data &gsData = GameState::getData();
            for (int i = 0; i < gsData.objCount; i++)
            {
                objtype *ob = objlist + gsData.objNums[i];
                Pillar::enterSkull(ob);
            }

            if (gsData.objCount > 0)
            {
                if (!gsData.spawned)
                {
                    gsData.spawned = true;
                    gsData.secs = 0.0;
                }
                else
                {
                    gsData.secs = std::max(gsData.secs - soulSecs, 0.0);
                }

                return true;
            }

            return false;
        }

        void clearSkulls(void)
        {
            GameState::Data &gsData = GameState::getData();
            for (int i = 0; i < gsData.objCount; i++)
            {
                objtype *ob = objlist + gsData.objNums[i];
                Pillar::clearSkull(ob);
            }
            SD_PlaySound (HSKULLCLEARSND);
        }

        bool enemyValid(objtype *ob)
        {
            switch (ob->obclass)
            {
            case guardobj:
            case officerobj:
            case mutantobj:
            case ssobj:
            case dogobj:
            case bjmutantobj:
            case bjmutantdogobj:
                return true;
            default:
                return false;
            }
        }

        bool checkPickup(int &playerNum)
        {
            GameState::Data &gsData = GameState::getData();

            for (int i = 0; i < gsData.objCount; i++)
            {
                objtype *ob = objlist + gsData.objNums[i];
                if (Pillar::checkPickup(ob, playerNum))
                {
                    return true;
                }
            }

            return false;
        }
        
        void poll(void)
        {
            GameState::Data &gsData = GameState::getData();
            if (gsData.spawned)
            {
                gsData.secs += TICS2SEC(tics);

                int playerNum = -1;
                if (checkPickup(playerNum))
                {
                    gsData.spawned = false;
                    clearSkulls();
                    clearCaptured();

                    int c;
                    for (LWMP_REPEAT_ONCE(c, playerNum))
                    {
                        StartBonusFlash();
                    }
                }

                if (gsData.secs >= limitSecs)
                {
                    gsData.spawned = false;
                    clearSkulls();
                    spawnZombies();
                }
            }
        }

        void enemyKilled(objtype *ob)
        {
            if (enabled() && enemyValid(ob) && spawnSkulls())
            {
                Soul::spawn(ob);
                SD_PlaySound (HSKULLSPAWNSND);
                State(ob).captured() = true;
            }
        }

        void spawnZombies(void)
        {
            objtype *check;

            for (check = objlist; check; check = check->next)
            {
                State checkS = State(check);
                if (checkS.captured())
                {
                    checkS.captured() = false;
                    Zombie::wasAlive(check) = true;
                }
            }
        }

        void clearCaptured(void)
        {
            objtype *check;

            for (check = objlist; check; check = check->next)
            {
                State checkS = State(check);
                checkS.captured() = false;
            }
        }
    }
}


/*
============================================================================

                            INFINITE AMMO MODE

============================================================================
*/

namespace InfiniteAmmoMode
{
    bool enabled(void)
    {
        return VampireMode::enabled();
    }
}


/*
============================================================================

                            ALL WEAPONS MODE

============================================================================
*/

namespace AllWeaponsMode
{
    bool enabled(void)
    {
        return InfiniteAmmoMode::enabled() || BjMutantMode::enabled();
    }
}


/*
============================================================================

                            INFINITE LIVES MODE

============================================================================
*/

namespace InfiniteLivesMode
{
    bool enabled(void)
    {
        return LWMP_NUM_PLAYERS > 1 && !InstagibMode::enabled();
    }
}


/*
============================================================================

                            DEATHMATCH MODE

============================================================================
*/

namespace DeathmatchMode
{
    // z component is spawn dir
    std::vector<lwlib::Point3i> spawn_pos;

    namespace ItemSpawner
    {
        extern statetype s_idle1;
        extern statetype s_idle2;
        extern statetype s_respawn1;
        extern statetype s_respawn2;
        extern statetype s_respawn3;
        extern statetype s_respawn4;
        extern statetype s_respawn5;

        static void spawnItem(objtype *ob);
        static void countDown(objtype *ob);

        statetype s_idle1    = {false,SPR_DEMO,1,NULL,NULL,&s_idle2};
        statetype s_idle2    = {false,SPR_DEMO,0,(statefunc)countDown,NULL,&s_idle2};
        statetype s_respawn1 = {false,SPR_RESPAWN_1,15,NULL,(statefunc)spawnItem,&s_respawn2};
        statetype s_respawn2 = {false,SPR_RESPAWN_2,15,NULL,NULL,&s_respawn3};
        statetype s_respawn3 = {false,SPR_RESPAWN_3,15,NULL,NULL,&s_respawn4};
        statetype s_respawn4 = {false,SPR_RESPAWN_4,15,NULL,NULL,&s_respawn5};
        statetype s_respawn5 = {false,SPR_RESPAWN_5,15,NULL,NULL,NULL};

        std::vector<int32_t> cooldown_by_area;

        void spawn(statobj_t *statptr)
        {
            const word areanumber = *(mapsegs[0] +
                    (statptr->tiley<<mapshift)+statptr->tilex) - AREATILE;
            if(areanumber >= NUMAREAS)
                return;

            SpawnNewObj (statptr->tilex,statptr->tiley,&s_idle1);
            newobj->obclass = inertobj;
            newobj->active = ac_yes;
            newobj->x = (newobj->tilex<<TILESHIFT)+(TILEGLOBAL/2);
            newobj->y = (newobj->tiley<<TILESHIFT)+(TILEGLOBAL/2);

            DmItemSpawnerInfo_t &info = newobj->dmItemSpawnerInfo;
            info.itemtype = (wl_stat_t)statptr->itemnumber;
            info.ticsleft = cooldown_by_area[areanumber];
        }

        static void countDown(objtype *ob)
        {
            DmItemSpawnerInfo_t &info = ob->dmItemSpawnerInfo;
            info.ticsleft -= tics;
            if(info.ticsleft < 0)
            {
                NewState(ob, &s_respawn1);
            }
        }

        static void spawnItem(objtype *ob)
        {
            const DmItemSpawnerInfo_t &info = ob->dmItemSpawnerInfo;
            PlaceItemType(info.itemtype, ob->tilex, ob->tiley);
        }

        static void resetCooldownConfig()
        {
            cooldown_by_area.clear();
            for(std::size_t i = 0; i < NUMAREAS; ++i)
            {
                cooldown_by_area.push_back(i * 70 * 2);
            }

            std::ifstream fs("configs/item_respawn_cooldown.cfg");
            if(fs)
            {
                std::size_t i = 0;
                int32_t ticsleft;
                while(i < NUMAREAS && fs >> ticsleft)
                {
                    cooldown_by_area[i++] = ticsleft;
                }
            }
        }
    }

    bool enabled(void)
    {
        return gamestate.mode == GameMode::deathmatch ||
            gamestate.mode == GameMode::timelimitdm;
    }

    void itemGrabbed(statobj_t *statptr)
    {
        if(enabled())
            ItemSpawner::spawn(statptr);
    }

    void reset()
    {
        int x,y;
        word *start;
        word tile;

        spawn_pos.clear();

        start = mapsegs[1];
        for (y=0;y<mapheight;y++)
        {
            for (x=0;x<mapwidth;x++)
            {
                tile = *start++;
                if (!tile)
                    continue;

                switch (tile)
                {
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                        spawn_pos.push_back(lwlib::vec3i(x,y,NORTH + tile - 19));
                        break;
                }
            }
        }

        ItemSpawner::resetCooldownConfig();
    }

    bool nextSpawn(int &tilex, int &tiley, int &dir)
    {
        // figure out which players are spawned and active
        int c;
        std::vector<lwlib::Point2i> player_pos;
        for (LWMP_REPEAT(c))
        {
            if(player_mp->active == ac_yes)
            {
                player_pos.push_back(lwlib::vec2i(player_mp->tilex, player_mp->tiley));
            }
        }

        // rank the available spawn positions by distance from active players
        typedef std::multimap<int,lwlib::Point3i> M;
        std::size_t i,j;
        M spawn_pos_by_mindist;
        for(i = 0; i < spawn_pos.size(); ++i)
        {
            int mindist = vec_length_sq(lwlib::vec2i(MAPSIZE, MAPSIZE));
            for(j = 0; j < player_pos.size(); ++j)
            {
                int dist = vec_length_sq(player_pos[j] - vec2(spawn_pos[i]));
                if(dist < mindist)
                {
                    mindist = dist;
                }
            }
            spawn_pos_by_mindist.insert(std::make_pair(mindist, spawn_pos[i]));
        }

        if(spawn_pos_by_mindist.empty())
            return false;

        // randomly select from the most distant spawn positions
        typedef M::const_iterator It;
        typedef std::pair<It,It> Range;
        const int mindist = spawn_pos_by_mindist.rbegin()->first;
        const M::size_type numequal = spawn_pos_by_mindist.count(mindist);
        const Range range = spawn_pos_by_mindist.equal_range(mindist);
        assert(numequal > 0 && range.second != range.first);

        It it = range.first;
        std::advance(it, US_RndT() % numequal);

        vec3_get(it->second, tilex, tiley, dir);
        return true;
    }
}


/*
============================================================================

                        TIMELIMIT DEATHMATCH MODE

============================================================================
*/

namespace TimelimitDeathmatchMode
{
    bool enabled(void)
    {
        return gamestate.mode == GameMode::timelimitdm;
    }
}


/*
============================================================================

                                 ENEMY

============================================================================
*/

namespace Enemy
{
    bool ammoDropPermitted(void)
    {
        return !RampageMode::enabled();
    }

    void dropAmmo(objtype *ob)
    {
        if (ammoDropPermitted())
        {
            PlaceItemType (bo_clip2, ob->tilex, ob->tiley);
        }
    }

    void die(objtype *ob)
    {
        if (RampageMode::enabled())
        {
            RampageMode::Enemy::die(ob);
        }
        else if (ZombieMode::enabled())
        {
            ZombieMode::rest(ob);
        }
        else if (ZombieHarvesterMode::enabled())
        {
            ZombieHarvesterMode::die(ob);
        }
    }

    namespace SightSound
    {
        typedef std::map<classtype, soundnames> Map;

        static Map m;

        soundnames get(classtype obclass)
        {
            if (m.empty())
            {
                m[guardobj] = HALTSND;
                m[officerobj] = SPIONSND;
                m[ssobj] = SCHUTZADSND;
                m[dogobj] = DOGBARKSND;
            }
            return m[obclass];
        }
    }

    soundnames sightSound(objtype *ob)
    {
        soundnames snd = SightSound::get(ob->obclass);
        ZombieMode::enemySightSound(ob, snd);
        ZombieHarvesterMode::enemySightSound(ob, snd);
        return snd;
    }

    namespace DieSound
    {
        typedef std::map<classtype, soundnames> Map;

        static Map m;

        soundnames get(classtype obclass)
        {
            if (m.empty())
            {
                m[officerobj] = NEINSOVASSND;
                m[ssobj] = LEBENSND;
                m[dogobj] = DOGDEATHSND;
            }
            return m[obclass];
        }
    }

    soundnames dieSound(objtype *ob)
    {
        soundnames snd = DieSound::get(ob->obclass);
        ZombieMode::enemyDieSound(ob, snd);
        ZombieHarvesterMode::enemyDieSound(ob, snd);
        return snd;
    }
}


/*
============================================================================

                                 OBJECT

============================================================================
*/

namespace Object
{
    const lwlib::Point2f getPos(objtype *ob)
    {
        return lwlib::vec2f(fixedpt_toconst(ob->x), 
            fixedpt_toconst(ob->y));
    }

    void setPos(objtype *ob, const lwlib::Point2f pos)
    {
        ob->x = fixedpt_rconst(pos.v[0]);
        ob->y = fixedpt_rconst(pos.v[1]);

        ob->tilex = (short)(ob->x >> TILESHIFT);
        ob->tiley = (short)(ob->y >> TILESHIFT);
    }
}


/*
===============
=
= LaunchRocket
=
===============
*/

void LaunchRocket (objtype *ob)
{
    int     iangle_mp;

    iangle_mp = ob->angle;
    SD_PlaySound (ATKROCKETLAUNCHERSND);

    GetNewActor ();
    newobj->state = &s_rocket;
    newobj->ticcount = 1;

    newobj->tilex = ob->tilex;
    newobj->tiley = ob->tiley;
    newobj->x = ob->x;
    newobj->y = ob->y;
    newobj->obclass = rocketobj;

    newobj->dir = nodir;
    newobj->angle = iangle_mp;
    newobj->speed = 0x2000l;
    newobj->flags = FL_NEVERMARK;
    newobj->active = ac_yes;
    newobj->shooterobjid = (ob - &objlist[0])+1;
}


/*
===============
=
= LaunchFlame
=
===============
*/

void LaunchFlame (objtype *ob)
{
    int     iangle_mp;

    iangle_mp = ob->angle;
    SD_PlaySound (ATKFLAMETHROWERSND);

    GetNewActor ();
    newobj->state = &s_flame1;
    newobj->ticcount = 1;

    newobj->tilex = ob->tilex;
    newobj->tiley = ob->tiley;
    newobj->x = ob->x;
    newobj->y = ob->y;
    newobj->obclass = flameobj;

    newobj->dir = nodir;
    newobj->angle = iangle_mp;
    newobj->speed = 0x2000l;
    newobj->flags = FL_NEVERMARK;
    newobj->active = ac_yes;
    newobj->shooterobjid = (ob - &objlist[0])+1;
}


//
// vines
//

extern statetype s_vinesstand;
extern statetype s_vinesdie1;
extern statetype s_vinesdie2;
extern statetype s_vinesdie3;
extern statetype s_vinesdie4;
extern statetype s_vinesdead;
extern statetype s_vinesgrow1;
extern statetype s_vinesgrow2;

statetype s_vinesstand = {false,SPR_VINES_STAND,0,NULL,NULL,&s_vinesstand};
statetype s_vinesdie1 = {false,SPR_VINES_DIE1,10,NULL,(statefunc)A_DeathScream,&s_vinesdie2};
statetype s_vinesdie2 = {false,SPR_VINES_DIE2,10,NULL,NULL,&s_vinesdie3};
statetype s_vinesdie3 = {false,SPR_VINES_DIE3,10,NULL,NULL,&s_vinesdie4};
statetype s_vinesdie4 = {false,SPR_VINES_DIE4,10,NULL,(statefunc)A_VinesSplashDamage,&s_vinesdead};
statetype s_vinesdead = {false,SPR_VINES_DEAD,540,NULL,(statefunc)A_VinesRespawn,&s_vinesgrow1};
statetype s_vinesgrow1 = {false,SPR_VINES_GROW1,10,NULL,NULL,&s_vinesgrow2};
statetype s_vinesgrow2 = {false,SPR_VINES_GROW2,10,NULL,NULL,&s_vinesstand};


/*
===============
=
= SpawnVines
=
===============
*/

void SpawnVines (int tilex, int tiley)
{
    SpawnNewObj (tilex,tiley,&s_vinesstand);
    newobj->obclass = vinesobj;
    newobj->hitpoints = starthitpoints[gamestate.difficulty][en_vines];
    newobj->flags |= FL_DAMAGEABLE | FL_PROJHITS;
    newobj->actorDist = MINACTORDISTBAR;
}


/*
===============
=
= A_VinesSplashDamage
=
===============
*/

void A_VinesSplashDamage (objtype *ob)
{
    SplashDamage(ob, 30, FP(2), true);

    ob->temp1++;
    if (ob->temp1 < 8)
    {
        NewState(ob, &s_vinesdie2);
        return;
    }
    ob->temp1 = 0;
}


/*
===============
=
= A_VinesRespawn
=
===============
*/

void A_VinesRespawn (objtype *ob)
{
    int         c;
    int32_t     deltax,deltay;
    bool        too_close = false;

    ob->flags |= FL_DAMAGEABLE;
    ob->hitpoints = starthitpoints[gamestate.difficulty][en_vines];
    ob->flags &= ~FL_NONMARK;
}


