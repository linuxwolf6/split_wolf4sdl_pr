// WL_AUTOPLAY.H
// Author: LinuxWolf - Team RayCAST

#ifndef WL_AUTOPLAY_H
#define WL_AUTOPLAY_H

typedef struct Autoplay_Pos_s
{
	int x, y;
	int dir;
} Autoplay_Pos_t;

void Autoplay_Init(int x, int y, int dir);

void Autoplay_Update(void);

Autoplay_Pos_t Autoplay_GetPos(void);

bool Autoplay_UpdateRequired(void);

void Autoplay_ToggleEnabled(void);

void Autoplay_GotKey(void);

#endif
