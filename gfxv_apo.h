//////////////////////////////////////
//
// Graphics .H file for Apogee v1.4
// IGRAB-ed on Sun May 03 01:19:32 1992
//
//////////////////////////////////////

typedef enum {
    // Lump Start
    H_BJPIC=3,
    H_CASTLEPIC,                 // 4
    H_KEYBOARDPIC,               // 5
    H_JOYPIC,                    // 6
    H_HEALPIC,                   // 7
    H_TREASUREPIC,               // 8
    H_GUNPIC,                    // 9
    H_KEYPIC,                    // 10
    H_BLAZEPIC,                  // 11
    H_WEAPON1234PIC,             // 12
    H_WOLFLOGOPIC,               // 13
    H_VISAPIC,                   // 14
    H_MCPIC,                     // 15
    H_IDLOGOPIC,                 // 16
    H_TOPWINDOWPIC,              // 17
    H_LEFTWINDOWPIC,             // 18
    H_RIGHTWINDOWPIC,            // 19
    H_BOTTOMINFOPIC,             // 20
#if !defined(APOGEE_1_0) && !defined(APOGEE_1_1) && !defined(APOGEE_1_2)
    H_SPEARADPIC,                // 21
#endif
    // Lump Start
    C_OPTIONSPIC,                // 22
    C_CURSOR1PIC,                // 23
    C_CURSOR2PIC,                // 24
    C_NOTSELECTEDPIC,            // 25
    C_SELECTEDPIC,               // 26
    C_FXTITLEPIC,                // 27
    C_DIGITITLEPIC,              // 28
    C_MUSICTITLEPIC,             // 29
    C_MOUSELBACKPIC,             // 30
    C_BABYMODEPIC,               // 31
    C_EASYPIC,                   // 32
    C_NORMALPIC,                 // 33
    C_HARDPIC,                   // 34
    C_LOADSAVEDISKPIC,           // 35
    C_DISKLOADING1PIC,           // 36
    C_DISKLOADING2PIC,           // 37
    C_CONTROLPIC,                // 38
    C_CUSTOMIZEPIC,              // 39
    C_LOADGAMEPIC,               // 40
    C_SAVEGAMEPIC,               // 41
    C_EPISODE1PIC,               // 42
    C_EPISODE2PIC,               // 43
    C_EPISODE3PIC,               // 44
    C_EPISODE4PIC,               // 45
    C_EPISODE5PIC,               // 46
    C_EPISODE6PIC,               // 47
    C_CODEPIC,                   // 48
#ifndef APOGEE_1_0
    C_TIMECODEPIC,               // 49
    C_LEVELPIC,                  // 50
    C_NAMEPIC,                   // 51
    C_SCOREPIC,                  // 52
#if !defined(APOGEE_1_1) && !defined(APOGEE_1_2)
    C_JOY1PIC,                   // 53
    C_JOY2PIC,                   // 54
#endif
#else
    C_TIMECODEPIC=C_CODEPIC,     // 47
#endif
    // Lump Start
    L_GUYPIC,                    // 55
    L_COLONPIC,                  // 56
    L_NUM0PIC,                   // 57
    L_NUM1PIC,                   // 58
    L_NUM2PIC,                   // 59
    L_NUM3PIC,                   // 60
    L_NUM4PIC,                   // 61
    L_NUM5PIC,                   // 62
    L_NUM6PIC,                   // 63
    L_NUM7PIC,                   // 64
    L_NUM8PIC,                   // 65
    L_NUM9PIC,                   // 66
    L_PERCENTPIC,                // 67
    L_APIC,                      // 68
    L_BPIC,                      // 69
    L_CPIC,                      // 70
    L_DPIC,                      // 71
    L_EPIC,                      // 72
    L_FPIC,                      // 73
    L_GPIC,                      // 74
    L_HPIC,                      // 75
    L_IPIC,                      // 76
    L_JPIC,                      // 77
    L_KPIC,                      // 78
    L_LPIC,                      // 79
    L_MPIC,                      // 80
    L_NPIC,                      // 81
    L_OPIC,                      // 82
    L_PPIC,                      // 83
    L_QPIC,                      // 84
    L_RPIC,                      // 85
    L_SPIC,                      // 86
    L_TPIC,                      // 87
    L_UPIC,                      // 88
    L_VPIC,                      // 89
    L_WPIC,                      // 90
    L_XPIC,                      // 91
    L_YPIC,                      // 92
    L_ZPIC,                      // 93
    L_EXPOINTPIC,                // 94
#ifndef APOGEE_1_0
    L_APOSTROPHEPIC,             // 95
#endif
    L_GUY2PIC,                   // 96
    L_BJWINSPIC,                 // 97
    STATUSBARPIC,                // 98
    TITLEPIC,                    // 99
    PG13PIC,                     // 100
    CREDITSPIC,                  // 101
    HIGHSCORESPIC,               // 102
    // Lump Start
    KNIFEPIC,                    // 103
    GUNPIC,                      // 104
    MACHINEGUNPIC,               // 105
    GATLINGGUNPIC,               // 106
    NOKEYPIC,                    // 107
    GOLDKEYPIC,                  // 108
    SILVERKEYPIC,                // 109
    N_BLANKPIC,                  // 110
    N_0PIC,                      // 111
    N_1PIC,                      // 112
    N_2PIC,                      // 113
    N_3PIC,                      // 114
    N_4PIC,                      // 115
    N_5PIC,                      // 116
    N_6PIC,                      // 117
    N_7PIC,                      // 118
    N_8PIC,                      // 119
    N_9PIC,                      // 120
    FACE1APIC,                   // 121
    FACE1BPIC,                   // 122
    FACE1CPIC,                   // 123
    FACE2APIC,                   // 124
    FACE2BPIC,                   // 125
    FACE2CPIC,                   // 126
    FACE3APIC,                   // 127
    FACE3BPIC,                   // 128
    FACE3CPIC,                   // 129
    FACE4APIC,                   // 130
    FACE4BPIC,                   // 131
    FACE4CPIC,                   // 132
    FACE5APIC,                   // 133
    FACE5BPIC,                   // 134
    FACE5CPIC,                   // 135
    FACE6APIC,                   // 136
    FACE6BPIC,                   // 137
    FACE6CPIC,                   // 138
    FACE7APIC,                   // 139
    FACE7BPIC,                   // 140
    FACE7CPIC,                   // 141
    FACE8APIC,                   // 142
    GOTGATLINGPIC,               // 143
    MUTANTBJPIC,                 // 144
    PAUSEDPIC,                   // 145
    GETPSYCHEDPIC,               // 146

    TILE8,                       // 147

    ORDERSCREEN,                 // 148
    ERRORSCREEN,                 // 149
    T_HELPART,                   // 150
#ifdef APOGEE_1_0
    T_ENDART1,                   // 143
#endif
    T_DEMO0,                     // 151
    T_DEMO1,                     // 152
    T_DEMO2,                     // 153
    T_DEMO3,                     // 154
#ifndef APOGEE_1_0
    T_ENDART1,                   // 155
    T_ENDART2,                   // 156
    T_ENDART3,                   // 157
    T_ENDART4,                   // 158
    T_ENDART5,                   // 159
    T_ENDART6,                   // 160
#endif

    ENUMEND,                     // 161
    L_GUYPIC_KNIFE=ENUMEND,      // 161
    L_GUYPIC_KNIFE_HURT1,        // 162
    L_GUYPIC_KNIFE_HURT2,        // 163
    L_GUYPIC_KNIFE_HURT3,        // 164
    L_GUYPIC_KNIFE_HURT4,        // 165
    L_GUYPIC_KNIFE_HURT5,        // 166
    L_GUYPIC_KNIFE_HURT6,        // 167
    L_GUYPIC_KNIFE_HURT7,        // 168
    L_GUYPIC_PISTOL,             // 169
    L_GUYPIC_PISTOL_HURT1,       // 170
    L_GUYPIC_PISTOL_HURT2,       // 171
    L_GUYPIC_PISTOL_HURT3,       // 172
    L_GUYPIC_PISTOL_HURT4,       // 173
    L_GUYPIC_PISTOL_HURT5,       // 174
    L_GUYPIC_PISTOL_HURT6,       // 175
    L_GUYPIC_PISTOL_HURT7,       // 176
    L_GUYPIC_MP40,               // 177
    L_GUYPIC_MP40_HURT1,         // 178
    L_GUYPIC_MP40_HURT2,         // 179
    L_GUYPIC_MP40_HURT3,         // 180
    L_GUYPIC_MP40_HURT4,         // 181
    L_GUYPIC_MP40_HURT5,         // 182
    L_GUYPIC_MP40_HURT6,         // 183
    L_GUYPIC_MP40_HURT7,         // 184
    L_GUYPIC_CHAIN,              // 185
    L_GUYPIC_CHAIN_HURT1,        // 186
    L_GUYPIC_CHAIN_HURT2,        // 187
    L_GUYPIC_CHAIN_HURT3,        // 188
    L_GUYPIC_CHAIN_HURT4,        // 189
    L_GUYPIC_CHAIN_HURT5,        // 190
    L_GUYPIC_CHAIN_HURT6,        // 191
    L_GUYPIC_CHAIN_HURT7,        // 192
    L_GUYPIC_RPG,                // ?
    L_GUYPIC_RPG_HURT1,          // ?
    L_GUYPIC_RPG_HURT2,          // ?
    L_GUYPIC_RPG_HURT3,          // ?
    L_GUYPIC_RPG_HURT4,          // ?
    L_GUYPIC_RPG_HURT5,          // ?
    L_GUYPIC_RPG_HURT6,          // ?
    L_GUYPIC_RPG_HURT7,          // ?
    L_GUYPIC_FLAME,              // ?
    L_GUYPIC_FLAME_HURT1,        // ?
    L_GUYPIC_FLAME_HURT2,        // ?
    L_GUYPIC_FLAME_HURT3,        // ?
    L_GUYPIC_FLAME_HURT4,        // ?
    L_GUYPIC_FLAME_HURT5,        // ?
    L_GUYPIC_FLAME_HURT6,        // ?
    L_GUYPIC_FLAME_HURT7,        // ?
    L_GUY2PIC_KNIFE,             // 193
    L_GUY2PIC_KNIFE_HURT1,       // 194
    L_GUY2PIC_KNIFE_HURT2,       // 195
    L_GUY2PIC_KNIFE_HURT3,       // 196
    L_GUY2PIC_KNIFE_HURT4,       // 197
    L_GUY2PIC_KNIFE_HURT5,       // 198
    L_GUY2PIC_KNIFE_HURT6,       // 199
    L_GUY2PIC_KNIFE_HURT7,       // 200
    L_GUY2PIC_PISTOL,            // 201
    L_GUY2PIC_PISTOL_HURT1,      // 202
    L_GUY2PIC_PISTOL_HURT2,      // 203
    L_GUY2PIC_PISTOL_HURT3,      // 204
    L_GUY2PIC_PISTOL_HURT4,      // 205
    L_GUY2PIC_PISTOL_HURT5,      // 206
    L_GUY2PIC_PISTOL_HURT6,      // 207
    L_GUY2PIC_PISTOL_HURT7,      // 208
    L_GUY2PIC_MP40,              // 209
    L_GUY2PIC_MP40_HURT1,        // 210
    L_GUY2PIC_MP40_HURT2,        // 211
    L_GUY2PIC_MP40_HURT3,        // 212
    L_GUY2PIC_MP40_HURT4,        // 213
    L_GUY2PIC_MP40_HURT5,        // 214
    L_GUY2PIC_MP40_HURT6,        // 215
    L_GUY2PIC_MP40_HURT7,        // 216
    L_GUY2PIC_CHAIN,             // 217
    L_GUY2PIC_CHAIN_HURT1,       // 218
    L_GUY2PIC_CHAIN_HURT2,       // 219
    L_GUY2PIC_CHAIN_HURT3,       // 220
    L_GUY2PIC_CHAIN_HURT4,       // 221
    L_GUY2PIC_CHAIN_HURT5,       // 222
    L_GUY2PIC_CHAIN_HURT6,       // 223
    L_GUY2PIC_CHAIN_HURT7,       // 224
    L_GUY2PIC_RPG,               // ?
    L_GUY2PIC_RPG_HURT1,         // ?
    L_GUY2PIC_RPG_HURT2,         // ?
    L_GUY2PIC_RPG_HURT3,         // ?
    L_GUY2PIC_RPG_HURT4,         // ?
    L_GUY2PIC_RPG_HURT5,         // ?
    L_GUY2PIC_RPG_HURT6,         // ?
    L_GUY2PIC_RPG_HURT7,         // ?
    L_GUY2PIC_FLAME,             // ?
    L_GUY2PIC_FLAME_HURT1,       // ?
    L_GUY2PIC_FLAME_HURT2,       // ?
    L_GUY2PIC_FLAME_HURT3,       // ?
    L_GUY2PIC_FLAME_HURT4,       // ?
    L_GUY2PIC_FLAME_HURT5,       // ?
    L_GUY2PIC_FLAME_HURT6,       // ?
    L_GUY2PIC_FLAME_HURT7,       // ?
    L_BJWINSPIC_KNIFE,           // 225
    L_BJWINSPIC_KNIFE_HURT1,     // 226
    L_BJWINSPIC_KNIFE_HURT2,     // 227
    L_BJWINSPIC_KNIFE_HURT3,     // 228
    L_BJWINSPIC_KNIFE_HURT4,     // 229
    L_BJWINSPIC_KNIFE_HURT5,     // 230
    L_BJWINSPIC_KNIFE_HURT6,     // 231
    L_BJWINSPIC_PISTOL,          // 232
    L_BJWINSPIC_PISTOL_HURT1,    // 233
    L_BJWINSPIC_PISTOL_HURT2,    // 234
    L_BJWINSPIC_PISTOL_HURT3,    // 235
    L_BJWINSPIC_PISTOL_HURT4,    // 236
    L_BJWINSPIC_PISTOL_HURT5,    // 237
    L_BJWINSPIC_PISTOL_HURT6,    // 238
    L_BJWINSPIC_MP40,            // 239
    L_BJWINSPIC_MP40_HURT1,      // 240
    L_BJWINSPIC_MP40_HURT2,      // 241
    L_BJWINSPIC_MP40_HURT3,      // 242
    L_BJWINSPIC_MP40_HURT4,      // 243
    L_BJWINSPIC_MP40_HURT5,      // 244
    L_BJWINSPIC_MP40_HURT6,      // 245
    L_BJWINSPIC_CHAIN,           // 246
    L_BJWINSPIC_CHAIN_HURT1,     // 247
    L_BJWINSPIC_CHAIN_HURT2,     // 248
    L_BJWINSPIC_CHAIN_HURT3,     // 249
    L_BJWINSPIC_CHAIN_HURT4,     // 250
    L_BJWINSPIC_CHAIN_HURT5,     // 251
    L_BJWINSPIC_CHAIN_HURT6,     // 252
    TANKGUNPIC,                  // 253
    TANKWHEELPIC,                // 254
    N_INFL,                      // 255
    N_INFR,                      // 256
    N_COLONPIC,                  // ?
    MG42GUNPIC,                  // 257
    DEATHMATCHMODEPIC,           // ?
    TIMELIMITDMPIC,              // ?
    CLASSICMODEPIC,              // 258
    INSTAGIBMODEPIC,             // 259
    DEFUSEMODEPIC,               // 260
    VAMPIREMODEPIC,              // 261
    CTFMODEPIC,                  // 262
    HARVESTERMODEPIC,            // 263
    BJMUTANTMODEPIC,             // 264
    RAMPAGEMODEPIC,              // 265
    ZOMBIEMODEPIC,               // 266
    ZOMBIEHARVESTERMODEPIC,      // 267
    DM01PIC,                     // ?
    DM02PIC,                     // ?
    DM03PIC,                     // ?
    DM04PIC,                     // ?
    DM05PIC,                     // ?
    DM06PIC,                     // ?
    DM07PIC,                     // ?
    DM08PIC,                     // ?
    DM09PIC,                     // ?
    FLAGPIC,                     // 269
    FISTSPIC,                    // 270
    SWTITLEPIC,                  // 271
    SWTITLE2PIC,                 // 272
    ROCKETLANCHERPIC,            // 273
    FLAMETHROWERPIC,             // 274
    L_GUYPIC_DEAD,               // 275
    L_GUY2PIC_DEAD,              // 276
    STATUSBARPIC_DM,             // 277
    STATUSBARPIC_TIMEDM,         // ?
    ENUMEND_EXTRA,               // 278
} graphicnums;

//
// Data LUMPs
//
#define README_LUMP_START       H_BJPIC
#define README_LUMP_END         H_BOTTOMINFOPIC

#define CONTROLS_LUMP_START     C_OPTIONSPIC
#define CONTROLS_LUMP_END       (L_GUYPIC - 1)

#define LEVELEND_LUMP_START     L_GUYPIC
#define LEVELEND_LUMP_END       L_BJWINSPIC

#define LATCHPICS_LUMP_START    KNIFEPIC
#define LATCHPICS_LUMP_END      GETPSYCHEDPIC


//
// Amount of each data item
//
#define NUMCHUNKS    ENUMEND
#define NUMFONT      2
#define NUMFONTM     0
#define NUMPICS      (GETPSYCHEDPIC - NUMFONT)
#define NUMPICM      0
#define NUMSPRITES   0
#define NUMTILE8     72
#define NUMTILE8M    0
#define NUMTILE16    0
#define NUMTILE16M   0
#define NUMTILE32    0
#define NUMTILE32M   0
#define NUMEXTERNS   13
//
// File offsets for data items
//
#define STRUCTPIC    0

#define STARTFONT    1
#define STARTFONTM   3
#define STARTPICS    3
#define STARTPICM    TILE8
#define STARTSPRITES TILE8
#define STARTTILE8   TILE8
#define STARTTILE8M  ORDERSCREEN
#define STARTTILE16  ORDERSCREEN
#define STARTTILE16M ORDERSCREEN
#define STARTTILE32  ORDERSCREEN
#define STARTTILE32M ORDERSCREEN
#define STARTEXTERNS ORDERSCREEN

//
// Thank you for using IGRAB!
//
